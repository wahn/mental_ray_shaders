all: clobber

clobber:
	-find . -name "*\~" -exec rm -f {} \;
	-find . -name "*.blend?" -exec rm -f {} \;
	-find . -name "*.log" -exec rm -f {} \;
	-rm mi_scenes/massive/*.tt
	-rm mi_scenes/massive/mental_ray.exr
	cd shaders/wmrs; make clobber
	cd shaders/massive; make clobber
	-git status

clean:
	-find . -name "*\~" -exec rm -f {} \;
	-find . -name "*.blend?" -exec rm -f {} \;
	-find . -name "*.log" -exec rm -f {} \;
	cd shaders/wmrs; make clean
	cd shaders/massive; make clean
	-git status

shaders: wmrs.so mill_massive.so

wmrs.so:
	cd shaders/wmrs; make all

mill_massive.so:
	cd shaders/massive; make all
