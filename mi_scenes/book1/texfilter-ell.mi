#----------------------------------------------------------------------------
# Copyright 1986-2000 by mental images GmbH & Co.KG, Fasanenstr. 81, D-10623
# Berlin, Germany. All rights reserved.
#----------------------------------------------------------------------------
#
# Chapter  4.2.6  : Elliptic Filtered Textures: Even Better
#                   Texture Anti-Aliasing
# Figure   4.16   : Texture mapping with elliptical filtering
#
#----------------------------------------------------------------------------

verbose on
link "base.so"
$include <base.mi>

options "opt"
	samples 	-2 -2
	object space
end options

camera "cam"
	frame		1
	output		"rgb" "out.rgb"
	focal		1
	aperture	2
	aspect		1
	resolution	800 800
end camera

#begin
filter color texture "pic" "tex_checker.rgb"

declare phenomenon
	color "flat_checker" (
		color texture	"texture"
	)
	shader "coord" "mib_texture_vector" (
		"select"	0,
		"selspace"	1
	)
	shader "remap" "mib_texture_remap" (
		"input"		= "coord",
		"repeat"	40 40
	)
	shader "tex" "mib_texture_filter_lookup" (
		"tex"		= interface "texture",
		"coord"		= "remap",
		"remap"		"remap",
		"eccmax"	20,
		"maxminor"	6,
		"space"		0
	)
	root = "tex"
end declare
#end

material "mtl"
	opaque
	"flat_checker" (
	    "texture"	"pic"
	)
end material

object "plane"
	visible trace shadow
	tag 1
	group
		-50  0  -50
		-50  0   50
		 50  0  -50
		 50  0   50

		 0   0   0
		 0   1   0
		 1   0   0
		 1   1   0

		v 0 t 4   v 1 t 5   v 2 t 6   v 3 t 7

		p "mtl" 0  1  3  2
	end group
end object

instance "plane_inst" "plane"
end instance

instance "cam_inst" "cam"
	transform	-0.9501 -0.1946  0.2439  0
			 0.0000  0.7817  0.6237  0
			-0.3120  0.5925 -0.7427  0
			 0.0000  0.7682 -2.5739  1
end instance

instgroup "rootgrp"
	"cam_inst" "plane_inst"
end instgroup

render "rootgrp" "cam_inst" "opt"

#------------------------------------------------------------------------------

incremental camera "cam"
	output		"rgb" "out.1.rgb"
end camera

incremental options "opt"
	samples 	-2 0
	object space
end options

render "rootgrp" "cam_inst" "opt"

#------------------------------------------------------------------------------

incremental camera "cam"
	output		"rgb" "out.2.rgb"
end camera

incremental options "opt"
	samples 	0 2
	object space
end options

render "rootgrp" "cam_inst" "opt"
