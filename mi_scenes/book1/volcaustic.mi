#----------------------------------------------------------------------------
# Copyright 1986-2000 by mental images GmbH & Co.KG, Fasanenstr. 81, D-10623
# Berlin, Germany. All rights reserved.
#----------------------------------------------------------------------------
#
# Chapter  7.7.1  : Volume Caustics
# Figure   7.13   : Volume caustic
#
#----------------------------------------------------------------------------

#background 0 0 0 255
verbose on
#begin
link "physics.so"
$include <physics.mi>

camera "cam"
        output		"rgb" "out.rgb"
        focal		270.0
        aperture	30.0
        aspect		1.0 
        resolution	800 800  
end camera

options "opt"
	caustic		   on
	caustic		   0
	photonvol accuracy 500 0.15
	shadow		   on 
        samples		   -1 2
        contrast	   0.05 0.05 0.05 0.1
	dither		   on
        trace depth	   2 3 4
	photon trace depth 0 3 3
	object space
end options

light "pointlight"
        "physical_light" (
	    "color"	1000 1000 1000
        )
        origin		-1.5 2.999 -50.0
        energy		1000 1000 1000
	caustic photons	100000
end light

instance "light-i" "pointlight" end instance

material "covermat"				#--------- light cover
        "transmat" ()
        photon "transmat_photon" ()
	volume "parti_volume" (
	    "scatter"			0.3 0.3 0.3, 
	    "extinction"		0.3,
	    "min_step_len"		0.03,   # for ray marching
	    "max_step_len"		0.2,    # for ray marching
	    "no_globil_where_direct"	true,	# optimization
	    "lights"			["light-i"]
	)
	photonvol "parti_volume_photon" (
	    "scatter"			0.3 0.3 0.3, 
	    "extinction"		0.3,
	    "min_level"			3	# only store after 3 refracts
	)
end material

object "cover" visible trace tag 0 caustic 1 
 	group
		-1.4	2.0	-49.6
		-1.4	2.0	-50.4
		-0.5	2.0	-50.4
		-0.5	2.0	-49.6
		v 0 v 1 v 2 v 3
		p "covermat"	0 1 2 3
	end group
end object

material "volsurf"				#--------- bounding box
        "transmat" ()
        shadow "transmat" ()
	volume "parti_volume" (
	    "scatter"			0.3 0.3 0.3, 
	    "extinction"		0.3,
	    "min_step_len"		0.03,
	    "max_step_len"		0.2,
	    "no_globil_where_direct"	true,
	    "lights"			["light-i"]
	)
end material

object "bbox" visible trace tag 1
	group
		-3  3 -51.5	-3  3 -48.5	-3 -3 -48.5	-3 -3 -51.5
		 3  3 -51.5	 3  3 -48.5	 3 -3 -48.5	 3 -3 -51.5
		v 0 v 1 v 2 v 3 v 4 v 5 v 6 v 7
		p "volsurf"	0 1 2 3		# left wall
		p		4 5 6 7		# right wall
		p		3 7 4 0		# back wall
		p		2 6 5 1		# front wall
		p		5 4 0 1		# ceiling
		p		6 7 3 2		# floor
	end group
end object

material "glass"				#--------- sphere, radius 1
	"dgs_material" (
	    "ior"	1.5,
	    "transp"	0.9,
	    "specular"	1.0 1.0 1.0
	)
	photon "dgs_material_photon" (
	    "ior"	1.5,
	    "transp"	1.0,
	    "specular"	1.0 1.0 1.0
	)
end material

object "sphere" visible shadow trace caustic 3
	# ...
#end
	basis "ratio_bez2" rational bezier 2
	group
		 0  0 -1	 1  0 -1	 1  1 -1	0  1 -1
		-1  1 -1	-1  0 -1	-1 -1 -1	0 -1 -1
		 1 -1 -1	 1  0  0	 1  1  0	0  1  0
		-1  1  0	-1  0  0	-1 -1  0	0 -1  0
		 1 -1  0	 1  0  1	 1  1  1	0  1  1
		-1  1  1	-1  0  1	-1 -1  1	0 -1  1
		 1 -1  1	 0  0  1

		v 0	v 1	v 2	v 3	v 4	v 5	v 6	v 7
		v 8	v 9	v 10	v 11	v 12	v 13	v 14	v 15
		v 16	v 17	v 18	v 19	v 20	v 21	v 22	v 23
		v 24	v 25

		surface "surf" "glass"
			"ratio_bez2" 0.0 1.0	0.0 0.25 0.5 0.75 1.0
			"ratio_bez2" 0.0 1.0	0.0 0.5	1.0

			0 0 0 w 2 0 0 0 0 w 2 0 0
			1 2 3 w 2 4 5 6 7 w 2 8 1
			9 w 2 10 w 2 11 w 4 12 w 2 13 w 2 14 w 2 15 w 4
				16 w 2 9 w 2
			17 18 19 w 2 20 21 22 23 w 2 24 17
			25 25 25 w 2 25 25 25 25 w 2 25 25

		derivative
		approximate surface parametric 14.0 14.0 "surf"
	end group
#begin
end object

instance "cam-i"    "cam"    end instance	#--------- instancing/rendering
instance "cover-i"  "cover"  end instance
instance "bbox-i"   "bbox"   end instance
instance "sphere-i" "sphere"
	transform 1 0 0 0   0 1 0 0   0 0 1 0   0 0 50 1
end instance

instgroup "all"
	"cam-i" "light-i" "cover-i" "bbox-i" "sphere-i"
end instgroup

render "all" "cam-i" "opt"
#end

#----------------------------------------------------------------

incremental options "opt"
	caustic		   off
end options

incremental camera "cam"
        output		"rgb" "out.1.rgb"
end camera

render "all" "cam-i" "opt"
