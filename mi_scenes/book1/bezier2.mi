#----------------------------------------------------------------------------
# Copyright 1986-2000 by mental images GmbH & Co.KG, Fasanenstr. 81, D-10623
# Berlin, Germany. All rights reserved.
#----------------------------------------------------------------------------
#
# Chapter  12.3.2 : Surfaces
# Figure   12.10  : Parameter range and parameter vector
# Description     : Subset of a parameter range (V range 0 2)
#
#----------------------------------------------------------------------------

verbose on
link "base.so"
$include <base.mi>

light "light1"
	"mib_light_point" (
	    "color"	1 1 1,
	    "factor"	0.75
	)
	origin		0 0 0
end light

instance "light1_inst" "light1"
	transform	 1  0  0  0
			 0  1  0  0
			 0  0  1  0
			-2 -3 -2  1
end instance

options "opt"
	samples 	-1 2
	contrast	0.1  0.1  0.1
	object space
end options

camera "cam"
	frame		1
	output		"rgb" "out.rgb"
	focal		50
	aperture	44
	aspect		1
	resolution	800 800
end camera

material "mtl"
	opaque
	"mib_illum_phong" (
	    "ambient"	0.2  0.2  0.2,
	    "diffuse"	0.7  0.7  0.7,
	    "ambience"	0.3  0.3  0.3,
	    "specular"	0.6  0.6  0.6,
	    "exponent"	50,
	    "mode"	1,
	    "lights"	["light1_inst"]
	)
end material

object "bez_surf"
	visible shadow trace tag 1
	basis "bez3" bezier 3
	group
                 4.0  0.0  4.0    4.0  0.6  3.2    4.0  1.0  2.2    4.0  1.2  1.4
                 4.0  1.4  0.6    4.0  1.4 -0.6    4.0  1.2 -1.4    4.0  1.0 -2.2
                 4.0  0.6 -3.2    4.0  0.0 -4.0    3.2  0.6  4.0    3.2  1.2  3.2
                 3.2  1.6  2.2    3.2  1.8  1.4    3.2  2.0  0.6    3.2  2.0 -0.6
                 3.2  1.8 -1.4    3.2  1.6 -2.2    3.2  1.2 -3.2    3.2  0.6 -4.0
                 2.2  1.0  4.0    2.2  1.6  3.2    2.2  2.0  2.2    2.2  2.2  1.4
                 2.2  2.4  0.6    2.2  2.4 -0.6    2.2  2.2 -1.4    2.2  2.0 -2.2
                 2.2  1.6 -3.2    2.2  1.0 -4.0    1.4  1.2  4.0    1.4  1.8  3.2
                 1.4  2.2  2.2    1.4  2.4  1.4    1.4  2.6  0.6    1.4  2.6 -0.6
                 1.4  2.4 -1.4    1.4  2.2 -2.2    1.4  1.8 -3.2    1.4  1.2 -4.0
                 0.6  1.4  4.0    0.6  2.0  3.2    0.6  2.4  2.2    0.6  2.6  1.4
                 0.6  2.8  0.6    0.6  2.8 -0.6    0.6  2.6 -1.4    0.6  2.4 -2.2
                 0.6  2.0 -3.2    0.6  1.4 -4.0   -0.6  1.4  4.0   -0.6  2.0  3.2
                -0.6  2.4  2.2   -0.6  2.6  1.4   -0.6  2.8  0.6   -0.6  2.8 -0.6
                -0.6  2.6 -1.4   -0.6  2.4 -2.2   -0.6  2.0 -3.2   -0.6  1.4 -4.0
                -1.4  1.2  4.0   -1.4  1.8  3.2   -1.4  2.2  2.2   -1.4  2.4  1.4
                -1.4  2.6  0.6   -1.4  2.6 -0.6   -1.4  2.4 -1.4   -1.4  2.2 -2.2
                -1.4  1.8 -3.2   -1.4  1.2 -4.0   -2.2  1.0  4.0   -2.2  1.6  3.2
                -2.2  2.0  2.2   -2.2  2.2  1.4   -2.2  2.4  0.6   -2.2  2.4 -0.6
                -2.2  2.2 -1.4   -2.2  2.0 -2.2   -2.2  1.6 -3.2   -2.2  1.0 -4.0
                -3.2  0.6  4.0   -3.2  1.2  3.2   -3.2  1.6  2.2   -3.2  1.8  1.4
                -3.2  2.0  0.6   -3.2  2.0 -0.6   -3.2  1.8 -1.4   -3.2  1.6 -2.2
                -3.2  1.2 -3.2   -3.2  0.6 -4.0   -4.0  0.0  4.0   -4.0  0.6  3.2
                -4.0  1.0  2.2   -4.0  1.2  1.4   -4.0  1.4  0.6   -4.0  1.4 -0.6
                -4.0  1.2 -1.4   -4.0  1.0 -2.2   -4.0  0.6 -3.2   -4.0  0.0 -4.0

		v 0	v 1	v 2	v 3	v 4	v 5	v 6	v 7
		v 8	v 9	v 10	v 11	v 12	v 13	v 14	v 15
		v 16	v 17	v 18	v 19	v 20	v 21	v 22	v 23
		v 24	v 25	v 26	v 27	v 28	v 29	v 30	v 31
		v 32	v 33	v 34	v 35	v 36	v 37	v 38	v 39
		v 40	v 41	v 42	v 43	v 44	v 45	v 46	v 47
		v 48	v 49	v 50	v 51	v 52	v 53	v 54	v 55
		v 56	v 57	v 58	v 59	v 60	v 61	v 62	v 63
		v 64	v 65	v 66	v 67	v 68	v 69	v 70	v 71
		v 72	v 73	v 74	v 75	v 76	v 77	v 78	v 79
		v 80	v 81	v 82	v 83	v 84	v 85	v 86	v 87
		v 88	v 89	v 90	v 91	v 92	v 93	v 94	v 95
		v 96	v 97	v 98	v 99

#begin
		surface "surf" "mtl"
			"bez3" 0 3	0.0  1.0  2.0  3.0 
			"bez3" 0 2	0.0  1.0  2.0 

			69 68 67 66 65 64
			63 62 61 60 59 58 57 56 55 54 53 52
			51 50 49 48 47 46 45 44 43 42 41 40
			39 38 37 36 35 34 33 32 31 30 29 28
			27 26 25 24 23 22 21 20 19 18 17 16
			15 14 13 12 11 10 9 8 7 6 5 4 3 2 1 0
#end

		approximate surface parametric 3.333333 3.333333 "surf"
	end group
end object

instance "bez_surf_inst" "bez_surf"
	transform	6  0  0  0
			0  6  0  0
			0  0  6  0
			0 -1  0  1
end instance

instance "cam_inst" "cam"
	transform	0.7719	0.3042 -0.5582 0.0
			0.0000	0.8781	0.4785 0.0
			0.6357 -0.3693	0.6778 0.0
			0.0000	0.0000 -2.5000 1.0
end instance

instgroup "rootgrp"
	"cam_inst" "light1_inst"  "bez_surf_inst"
end instgroup

render "rootgrp" "cam_inst" "opt"
