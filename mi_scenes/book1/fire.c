#include <stdio.h>
#include <math.h>
#include "/usr/local/mi/rayinc/shader.h"

#define max(x,y) ((x)>(y)?(x):(y))

struct fire { miColor filter; };

int fire_version(void) {return(1);}

miBoolean fire(
	miColor		*result,
	miState		*state,
	struct fire	*paras)
{
	miScalar	col, t, ft, x, fx, r, g, b, a, rad;
	miVector	vtmp, start, end, mid, gnoise;
	miScalar	*l2g, *g2l;
	miColor		*filter;

	if (!state->inv_normal)		/* only on back side */
		return(miTRUE);
	mi_query(miQ_TRANS_OBJECT_TO_WORLD, state, 0, &l2g);
	mi_query(miQ_TRANS_WORLD_TO_OBJECT, state, 0, &g2l);
	vtmp.x = 0; vtmp.y = -1; vtmp.z = 0;
	mi_point_transform(&start, &state->org,   g2l);
	mi_point_transform(&end,   &state->point, g2l);

	mid.x = (end.x + start.x) * 0.5;
	mid.y = (end.y + start.y) * 0.5;
	mid.z = (end.z + start.z) * 0.5;
	t = (mid.y + 1) * 0.5;
	vtmp.x = mid.x * (t * 28.5);
	vtmp.y = mid.y * (t * 28.5);
	vtmp.z = mid.z * (t * 28.5);
	mi_noise_3d_grad(&vtmp, &gnoise);
	mid.x += 0.2 * gnoise.x;
	mid.y += 0.2 * gnoise.y;
	mid.z += 0.2 * gnoise.z;

	ft = sqrt(mid.x*mid.x + mid.z*mid.z);   
	x  = t * 4.65;
	fx = 2 * (sqrt(x) - x*x * 0.1) * 0.2; /* magic fire formulae */
	if (ft <= fx) {
#if 0
		filter = mi_eval_color(&paras->filter);
#endif
		rad = ft / fx;
		col = 1 - rad;
#if 0
		r   = filter->r * 1.5*col * sqrt(col) * 1.2;
		g   = filter->g * 1.5*col * sqrt(col) * 0.5 + t * col * 0.4;
		b   = filter->b * 1.5*col * (col*0.5 + (1-t) * rad*rad * 0.4);
#else
		r   = 1.5*col * sqrt(col) * 1.2;
		g   = 1.5*col * sqrt(col) * 0.5 + t * col * 0.4;
		b   = 1.5*col * (col*0.5 + (1-t) * rad*rad * 0.4);
#endif
		a   = col < 0 ? 0 : col;
		a   = max(r, g);
		a   = max(a, b);
		t   = a > 1 ? 0 : 1-a;
		result->r = result->r * t + r;
		result->g = result->g * t + g;
		result->b = result->b * t + b;
		result->a = result->a * t + a;
	}
	return(miTRUE);
}
