#----------------------------------------------------------------------------
# Copyright 1986-2000 by mental images GmbH & Co.KG, Fasanenstr. 81, D-10623
# Berlin, Germany. All rights reserved.
#----------------------------------------------------------------------------
#
# Chapter 4.7  : Environment mapping 
# Figure  4.37 : Global and local environment map
#
#----------------------------------------------------------------------------

verbose on
link "base.so"
$include <base.mi>

color texture "pic0" "tex_tiles.rgb"
color texture "pic1" "tex_checker.rgb"

light "light1"
	"mib_light_point" (
	    "color"	1 1 1,
	    "factor"	0.75
	)
	origin		10 10 10
end light

instance "light1_inst" "light1"
end instance

options "opt"
	samples 	0 2
	contrast	0.1  0.1  0.1
	object space
end options

declare phenomenon
	color "env_sh" (
		color texture	"ceil_env",
		color texture	"floor_env",
		color texture	"wall_env"
	)
	shader "state-point" "mib_texture_vector" (
		"select"	-1,
		"selspace"	2
	)
	shader "state-dir" "mib_texture_vector" (
		"select"	-4,
		"selspace"	2
	)
	shader "tex" "mib_lookup_cube6" (
		"point"		= "state-point",
		"dir"		= "state-dir",
		"size"		100 100 100,
		"tex_mx"	= interface "wall_env",
		"tex_px"	= interface "wall_env",
		"tex_my"	= interface "floor_env",
		"tex_py"	= interface "ceil_env",
		"tex_mz"	= interface "wall_env",
		"tex_pz"	= interface "wall_env"
	)
	root = "tex"
end declare

#begin
camera "cam"
	frame		1
	output		"rgb" "out.rgb"
	focal		1
	aperture	1
	aspect		1
	resolution	800 800
	environment "env_sh" (
	    "ceil_env"	"pic0",
	    "floor_env"	"pic0",
	    "wall_env"	"pic1"
	)
end camera
#end

declare phenomenon
	color "mtl_sh" (
		array light	"lights"
	)
	shader "mtl" "mib_illum_phong" (
		"ambient"	.3 .3 .3,
		"diffuse"	.7 .7 .7,
		"ambience"	.1 .1 .1,
		"specular"	1 1 1,
		"exponent"	50,
		"mode"		1,
		"lights"	= interface "lights"
	)
	shader "env" "mib_reflect" (
		"input"		= "mtl",
		"reflect"	.3 .3 .3 .3,
		"notrace"	true
	)
	root = "env"
end declare

material "mtl"
	opaque
	"mtl_sh" (
	    "lights"	["light1_inst"],
	)
	environment "env_sh" (
	    "ceil_env"	"pic0",
	    "floor_env"	"pic0",
	    "wall_env"	"pic1"
	)
end material

$include "sphere.mi"

instance "sphere1_inst" "sphere"
	material	"mtl"
end instance

instance "cam_inst" "cam"
	transform	1  0  0  0
			0  1  0  0
			0  0  1  0
			0  0 -12 1
end instance

instgroup "rootgrp"
	"cam_inst" "light1_inst"  "sphere1_inst"
end instgroup

render "rootgrp" "cam_inst" "opt"
