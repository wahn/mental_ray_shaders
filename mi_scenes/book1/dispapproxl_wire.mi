#----------------------------------------------------------------------------
# Copyright 1986-2000 by mental images GmbH & Co.KG, Fasanenstr. 81, D-10623
# Berlin, Germany. All rights reserved.
#----------------------------------------------------------------------------
#
# Chapter  4.5.1  : Displacement Approximation
# Figure   4.28   : Edge length displacement map approximation
#
#----------------------------------------------------------------------------

verbose on
link "contour.so"
link "base.so"
$include <contour.mi>
$include <base.mi>

options "opt"
	samples 	-1 2
	contrast	0.1  0.1  0.1
	contour store	"contour_store_function" ()
	contour contrast "contour_contrast_function_levels" (
		"zdelta"		10.0,
		"ndelta"		90.0,
		"max_level"		1,
		"diff_label"		on,
		"diff_index"		on,
	)
	object space
end options

camera "cam"
	frame		1
	output		"contour,rgba" "contour_ps" (
		"paper_size"		4,
		"paper_scale"		1.0,
		"paper_transform_b"	0.0,
		"paper_transform_d"	1.0,
		"title"			off,
		"file_name"		"wire.ps"
	)
	focal		3
	aperture	1
	aspect		1
	resolution	800 800
end camera

instance "cam_inst" "cam"
	transform 1 0 0 0  0 1 0 0  0 0 1 0  0 0 -14.5 1
end instance

light "light1"
	"mib_light_point" (
	    "color"	1 1 1
	)
	origin		50 150 400
end light

instance "light1_inst" "light1" end instance

color texture "pic1" "tex_wave.rgb"

declare phenomenon
	color "displace_sh" (
		color texture	"texture"
	)
	shader "coord" "mib_texture_vector" (
		"select"	0,
		"selspace"	1
	)
	shader "remap" "mib_texture_remap" (
		"input"		= "coord",
		"repeat"	3 3
	)
	shader "tex" "mib_texture_lookup" (
		"tex"		= interface "texture",
		"coord"		= "remap"
	)
	root = "tex"
end declare

material "mtl"
	opaque
	"mib_illum_phong" (
	    "ambient"	0.3  0.3  0.3,
	    "diffuse"	0.7  0.7  0.7,
	    "ambience"	0.3  0.3  0.3,
	    "specular"	0.2  0.2  0.2,
	    "exponent"	50,
	    "mode"	1,
	    "lights"	["light1_inst"]
	)
	displace "displace_sh" (
	    "texture"	"pic1"
	)
	contour "contour_shader_simple" (
	    "color"     0 0 0 1,
	    "width"	0.05
	)
end material

object "plane"
	visible trace shadow tag 1
	basis "bez" bezier 3
	basis "lin" bezier 1
	group
		-2 -2  2	-2 -2  2	2 -2  2		2 -2  2
		-2  0 -4	-2  0 -4	2  0 -4		2  0 -4
		-2  2 -10	-2  2 -10	2  2 -10	2  2 -10
		-2  4 -16	-2  4 -16	2  4 -16	2  4 -16

		 0 .99  0	 0  0  0	.99 .99 0	.99  0  0

		v 0 v 1 v 2 v 3 v 4 v 5 v 6 v 7 v 8 v 9 v 10
		v 11 v 12 v 13 v 14 v 15 v 16 v 17 v 18 v 19

		surface "surf" "mtl"
			"bez" 0 1	0. 1.
			"bez" 0 1	0. 1.
			0 1 2 3   4 5 6 7   8 9 10 11   12 13 14 15

		texture "lin"		0. 1.
			"lin"		0. 1.
			16 17 18 19

#begin
		approximate surface  parametric 4 4 "surf"
		approximate displace length 0.412 1 5 "surf"
#end
	end group
end object

instance "plane_inst" "plane" end instance

instgroup "rootgrp"
	"cam_inst" "light1_inst" "plane_inst"
end instgroup

render "rootgrp" "cam_inst" "opt"
