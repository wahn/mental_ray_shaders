#----------------------------------------------------------------------------
# Copyright 1986-2000 by mental images GmbH & Co.KG, Fasanenstr. 81, D-10623
# Berlin, Germany. All rights reserved.
#----------------------------------------------------------------------------
#
# Chapter  3.6    : Orthographic Camera
# Figure   3.5    : Pinhole and orthographic camera
# Description     : Ortographic image of a cube
#
#----------------------------------------------------------------------------

verbose on
link "base.so"
$include <base.mi>

light "light1"
	"mib_light_point" (
	    "color"	1 1 1,
	    "factor"	0.75,
	)
	origin		0 0 0
end light

instance "light1_inst" "light1"
	transform	 1  0  0  0
			 0  1  0  0
			 0  0  1  0
			-2 -3 -2  1
end instance

options "opt"
	samples 	-1 2
	contrast	0.1  0.1  0.1
	object space
end options

#begin
camera "cam"
	frame		1
	output		"rgb" "out.rgb"
	focal		infinity
	aperture	2
	aspect		1
	resolution	800 800
end camera
#end

material "mtl"
	opaque
	"mib_illum_phong" (
	    "ambient"	0.5  0.5  0.5,
	    "diffuse"	0.7  0.7  0.7,
	    "ambience"	0.3  0.3  0.3,
	    "specular"	1.0  1.0  1.0,
	    "exponent"	50,
	    "mode"	1,
	    "lights"	["light1_inst"]
	)
end material

object "cube1"
	visible trace shadow
	tag 1
	group
		-0.5 -0.5 -0.5
		-0.5 -0.5  0.5
		-0.5  0.5 -0.5
		-0.5  0.5  0.5
		 0.5 -0.5 -0.5
		 0.5 -0.5  0.5
		 0.5  0.5 -0.5
		 0.5  0.5  0.5

		v 0   v 1   v 2   v 3
		v 4   v 5   v 6   v 7

		p "mtl" 0  1  3  2
		p	1  5  7  3
		p	5  4  6  7
		p	4  0  2  6
		p	4  5  1  0
		p	2  3  7  6
	end group
end object

instance "cube1_inst" "cube1"
	transform	1  0  0  0
			0  1  0  0
			0  0  1  0
			0  0  0  1
end instance

instance "cam_inst" "cam"
	transform	0.7719	0.3042 -0.5582 0.0
			0.0000	0.8781	0.4785 0.0
			0.6357 -0.3693	0.6778 0.0
			0.0000	0.0000 -2.5000 1.0
end instance

instgroup "rootgrp"
	"cam_inst" "light1_inst"  "cube1_inst"
end instgroup

render "rootgrp" "cam_inst" "opt"
