#----------------------------------------------------------------------------
# Copyright 1986-2000 by mental images GmbH & Co.KG, Fasanenstr. 81, D-10623
# Berlin, Germany. All rights reserved.
#----------------------------------------------------------------------------
#
# Chapter  12.3.8 : Approximations
# Figure   12.16  : Ungraded triangulation
# Description     : Ungraded delaunay length approximation of a bezier
#                   surface (delaunay length 0.5)
#
#----------------------------------------------------------------------------

verbose on
link "contour.so"
$include <contour.mi>
link "base.so"
$include <base.mi>

light "light1"
	"mib_light_point" (
	    "color"	1 1 1,
	    "factor"	0.75
	)
	origin		0 0 0
end light

instance "light1_inst" "light1"
	transform	 1  0  0  0
			 0  1  0  0
			 0  0  1  0
			-2 -3 -2  1
end instance

options "opt"
	samples 	-1 2
	contrast	0.1  0.1  0.1
	contour store	"contour_store_function" ()
	contour contrast "contour_contrast_function_levels" (
		"zdelta"		10.0,
		"ndelta"		90.0,
		"max_level"		1,
		"diff_label"		on,
		"diff_index"		on,
	)
	object space
end options

camera "cam"
	frame		1
	output		"contour,rgba" "contour_ps" (
		"paper_size"		4,
		"paper_scale"		1.0,
		"paper_transform_b"	0.0,
		"paper_transform_d"	1.0,
		"title"			off,
		"file_name"		"wire.ps"
	)
	focal		50
	aperture	44
	aspect		1
	resolution	800 800
end camera

material "mtl"
	opaque
	"mib_illum_phong" (
	    "ambient"	0.2  0.2  0.2,
	    "diffuse"	0.7  0.7  0.7,
	    "ambience"	0.3  0.3  0.3,
	    "specular"	0.6  0.6  0.6,
	    "exponent"	50,
	    "mode"	1,
	    "lights"	["light1_inst"]
	)
	contour "contour_shader_simple" (
	    "color"     0 0 0 1,
	    "width"     0.01
	)
end material

object "bez_surf"
	visible shadow trace tag 1
	basis "bez1" bezier 1
	basis "bez3" bezier 3
	group
		-3.0 -3.0  0	 3.0 -3.0  0	-3.0  3.0  0	 3.0  3.0  0
		 1.5  0.0  0     2.5  0.0  0     3.0  0.5  0     3.0  1.5  0
		 3.0  2.5  0     2.5  3.0  0     1.5  3.0  0     0.5  3.0  0
		 0.0  2.5  0     0.0  1.5  0     0.0  0.5  0     0.5  0.0  0

		v 0	v 1	v 2	v 3	v 4	v 5	v 6	v 7
		v 8	v 9	v 10	v 11	v 12	v 13	v 14	v 15

		curve "boundary"
			"bez3"		0.0  0.25 0.5  0.75  1.0
			4 5 6 7 8 9 10 11 12 13 14 15 4

		surface "surf" "mtl"
			"bez1" 0 3	0.0  3.0
			"bez1" 0 3	0.0  3.0
			0 1 2 3
			trim "boundary"  0 1

#begin
		approximate surface delaunay length 0.5 "surf"
		approximate curve parametric 20.0 "boundary"
#end
	end group
end object

instance "bez_surf_inst" "bez_surf"
end instance

instance "cam_inst" "cam"
	transform	1  0  0  0
			0  1  0  0
			0  0  1  0
			0  0 -7  1
end instance

instgroup "rootgrp"
	"cam_inst" "light1_inst"  "bez_surf_inst"
end instgroup

render "rootgrp" "cam_inst" "opt"
