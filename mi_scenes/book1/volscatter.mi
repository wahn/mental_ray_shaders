#----------------------------------------------------------------------------
# Copyright 1986-2000 by mental images GmbH & Co.KG, Fasanenstr. 81, D-10623
# Berlin, Germany. All rights reserved.
#----------------------------------------------------------------------------
#
# Chapter  7.7.2  : Multiple Volume Scatteringi: Global Illumination
#                   in Volumes
# Figure   7.15   : Multiple volume scattering from a light beam
#
#----------------------------------------------------------------------------

#background 0 0 0 255
#begin
min version "2.1"
verbose on
link "physics.so"
$include <physics.mi>

options "opt"
	samples		-2 1
	dither		on
	globillum	on
	photonvol accuracy 500 0.5
	object space
end options

camera "cam"
        output		"rgb" "out.rgb"
        focal		270.0
        aperture	30.0
        aspect		1.0 
        resolution	800 800  
end camera

light "light" visible 
        "physical_light" (
	    "color"	1000 1000 1000
        )
        origin		0.0 2.0 -50.0
	direction	0.0 -1.0 0.0
	spread		0.9
        energy		10000 10000 10000
	globillum photons 100000
end light

instance "light-i" "light" end instance

#---------------------------------- light cover (to enter volume)

material "covermat"
        "transmat" ()
        photon "transmat_photon" ()
	volume "parti_volume" (
	    "scatter"	   0.3 0.3 0.3, 
	    "extinction"   0.4,
	    "min_step_len" 0.01,	# for ray marching
	    "max_step_len" 0.1,		# for ray marching
	    "lights"	   ["light-i"]
	)
	photonvol "parti_volume_photon" (
	    "scatter"	   0.3 0.3 0.3, 
	    "extinction"   0.4
	)
end material

object "cover" visible trace tag 0
 	group
		-0.1	1.999	-49.99
		-0.1	1.999	-50.01
		 0.1	1.999	-50.01
		 0.1	1.999	-49.99
		v 0 v 1 v 2 v 3
		p "covermat"	0 1 2 3
	end group
end object

#---------------------------------- box

material "volsurf"
        "transmat" ()
        photon "transmat_photon" ()
	volume "parti_volume" (
	    "scatter"	   0.3 0.3 0.3, 
	    "extinction"   0.3,
	    "min_step_len" 0.01,
	    "max_step_len" 0.1,
	    "lights"	   ["light-i"]
	)
end material

object "box" visible trace tag 1
	group
		-3	-3	-51
		-3	 3	-51
		 3	 3	-51
		 3	-3	-51
		-3	-3	-49
		-3	 3	-49
		 3	 3	-49
		 3	-3	-49
		v 0 v 1 v 2 v 3 v 4 v 5 v 6 v 7
		p "volsurf"	0 1 2 3		# back wall
		p		7 6 5 4		# front wall
		p		0 1 4 5		# left wall
		p		2 3 7 6		# right wall
		p		1 2 6 5		# ceiling
		p		0 3 7 4		# floor
	end group
end object

#---------------------------------- instances and rendering

instance "cam-i"   "cam"   end instance
instance "cover-i" "cover" end instance
instance "box-i"   "box"   end instance

instgroup "all"
	"cam-i" "light-i" "cover-i" "box-i"
end instgroup

render "all" "cam-i" "opt"
#end

#---------------------------------- same without scattering

incremental options "opt"
	globillum	off
end options

incremental camera "cam"
        output "rgb" "out.1.rgb"
end camera

render "all" "cam-i" "opt"
