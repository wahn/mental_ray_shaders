#----------------------------------------------------------------------------
# Copyright 1986-2000 by mental images GmbH & Co.KG, Fasanenstr. 81, D-10623
# Berlin, Germany. All rights reserved.
#----------------------------------------------------------------------------
#
# Chapter  7.2    : Diffuse, Glossy, and Specular Reflection and Transmission
# Figure   7.1    : Diffuse, glossy, and specular reflection.
#
#----------------------------------------------------------------------------

verbose on
link "base.so"
link "physics.so"
$include <base.mi>
$include <physics.mi>

options "opt"
	samples		-1 2
	object space
end options

camera "cam"
	output		"rgb" "out.rgb"
	focal		50
	aperture	42
	aspect		1
	resolution	800 800
end camera

light "light1"
	"physical_light" (
	    "color"	100000 100000 100000
	)
	visible
        origin		0.0 5.0 -50.0
	sphere		1.0
end light

instance "light1-i" "light1" end instance

#begin
material "mat" opaque				# diffuse reflection
        "dgs_material" (
	    "diffuse"	0.8 0.8 0.8,
	    "lights"	["light1-i"]
        )
end material

#end
object "obj" visible trace shadow		# ground plane
	group
		 100	-3	0
		 100	-3	-2000
		-100	-3	-2000
		-100	-3	0
		v 0 v 1 v 2 v 3
		p "mat" 0 1 2 3
	end group
end object

material "gray" opaque
        "mib_illum_lambert" (
	    "ambience"	1.0 1.0 1.0,
	    "ambient"	0.8 0.8 0.8
        )
end material

object "bkg" visible				# background
	group
		-100	-100	-100
		 100	-100	-100
		 100	 100	-100
		-100	 100	-100
		v 0 v 1 v 2 v 3
		p "gray" 0 1 2 3
	end group
end object

instance "cam-i" "cam" end instance
instance "obj-i" "obj" end instance
instance "bkg-i" "bkg" end instance

instgroup "all"
	"cam-i" "light1-i" "obj-i" "bkg-i"
end instgroup

render "all" "cam-i" "opt"

#------------------------------------------------------------------------------

incremental camera "cam"
	output		"rgb" "out.1.rgb"
end camera

incremental
#begin
material "mat" opaque				# glossy reflection
        "dgs_material" (
	    "glossy"	0.8 0.8 0.8,
	    "shiny"	30.0,
	    "lights"	["light1-i"]
        )
end material

#end
render "all" "cam-i" "opt"

#------------------------------------------------------------------------------

incremental camera "cam"
	output		"rgb" "out.2.rgb"
end camera

incremental
#begin
material "mat" opaque				# specular reflection
        "dgs_material" (
	    "specular"	0.8 0.8 0.8,
	    "lights"	["light1-i"]
        )
end material
#end

render "all" "cam-i" "opt"
