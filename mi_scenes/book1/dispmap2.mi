#----------------------------------------------------------------------------
# Copyright 1986-2000 by mental images GmbH & Co.KG, Fasanenstr. 81, D-10623
# Berlin, Germany. All rights reserved.
#----------------------------------------------------------------------------
#
# Chapter  4.5    : Displacement Mapping
# Figure   4.24   : Approximation of displacement maps
# Description     : Cube with displacement mapping ("factor" -0.2)
#
#----------------------------------------------------------------------------

verbose on
link "base.so"
$include <base.mi>

light "light1"
	"mib_light_point" (
	    "color"	1 1 1,
	    "factor"	0.75
	)
	origin		0 0 0
end light

instance "light1_inst" "light1"
	transform	 1  0  0  0
			 0  1  0  0
			 0  0  1  0
			-2 -3 -2  1
end instance

options "opt"
	samples 	-1 2
	contrast	0.1  0.1  0.1
	object space
end options

camera "cam"
	frame		1
	output		"rgb" "out.rgb"
	focal		50
	aperture	44
	aspect		1
	resolution	800 800
end camera

color texture "pic1" "tex_sphere.rgb"

declare phenomenon
	color "mtl_sh" (
		array light	"lights"
	)
	shader "mtl" "mib_illum_phong" (
		"ambient"	.3 .3 .3,
		"diffuse"	.7 .7 .7,
		"ambience"	.1 .1 .1,
		"specular"	1 1 1,
		"exponent"	50,
		"mode"		1,
		"lights"	= interface "lights"
	)
	root = "mtl"
end declare

declare phenomenon
	scalar "displace_sh" (
		color texture	"texture"
	)
	shader "coord" "mib_texture_vector" (
		"select"	0,
		"selspace"	1
	)
	shader "remap" "mib_texture_remap" (
		"input"		= "coord"
	)
	shader "tex" "mib_texture_lookup" (
		"tex"		= interface "texture",
		"coord"		= "remap"
	)
#begin
	shader "displace" "mib_color_intensity" (
		"input"		= "tex",
		"factor"	-.2
	)
#end
	root = "displace"
end declare

material "mtl"
	opaque
	"mtl_sh" (
	    "lights"	["light1_inst"]
	)
	displace "displace_sh" (
	    "texture"	"pic1"
	)
end material

object "cube1"
	visible trace shadow
	tag 1
	group
		-0.5 -0.5 -0.5
		-0.5 -0.5  0.5
		-0.5  0.5 -0.5
		-0.5  0.5  0.5
		 0.5 -0.5 -0.5
		 0.5 -0.5  0.5
		 0.5  0.5 -0.5
		 0.5  0.5  0.5

		 0.0      0.0      0.0
		 0.99999  0.0      0.0
		 0.0      0.99999  0.0
		 0.99999  0.99999  0.0

		v 0 t 8   v 1 t 9   v 3 t 11  v 2 t 10
		v 1 t 8   v 5 t 9   v 7 t 11  v 3 t 10
		v 5 t 8   v 4 t 9   v 6 t 11  v 7 t 10
		v 4 t 8   v 0 t 9   v 2 t 11  v 6 t 10
		v 5 t 8   v 1 t 9   v 0 t 11  v 4 t 10
		v 3 t 8   v 7 t 9   v 6 t 11  v 2 t 10

		p "mtl"  0   1   2   3
		p	 4   5   6   7
		p        8   9  10  11
		p       12  13  14  15
		p       16  17  18  19
		p       20  21  22  23

		approximate angle 5 2 10
	end group
end object

instance "cube1_inst" "cube1"
end instance

instance "cam_inst" "cam"
	transform	0.7719	0.3042 -0.5582 0.0
			0.0000	0.8781	0.4785 0.0
			0.6357 -0.3693	0.6778 0.0
			0.0000	0.0000 -2.5000 1.0
end instance

instgroup "rootgrp"
	"cam_inst" "light1_inst"  "cube1_inst"
end instgroup

render "rootgrp" "cam_inst" "opt"
