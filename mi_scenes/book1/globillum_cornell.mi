#----------------------------------------------------------------------------
# Copyright 1986-2000 by mental images GmbH & Co.KG, Fasanenstr. 81, D-10623
# Berlin, Germany. All rights reserved.
#----------------------------------------------------------------------------
#
# Chapter  7.6.1  : Diffuse Global Illumination (Radiosity)
# Figure   7.10   : Empty cornell box with white walls and diffuse
#                   global illumination
#
#----------------------------------------------------------------------------

min version "2.1"

#begin
verbose on
link "base.so"
link "physics.so"
$include <base.mi>
$include <physics.mi>

options "opt"
	globillum	on			# <<<
	globillum accuracy 4000 2.0		# <<<
	samples		-2 1
end options

camera "cam"
	output		"rgb" "out.rgb"
	focal		50
	aperture	34
	aspect		1
	resolution	800 800
end camera

light "light1"
	"physical_light" (
	    "color"	600 600 600
	)
	origin		0.0 4.999 -20.0
	rectangle	1 0 0  0 0 1  6 6
	visible					# <<<
	energy		600 600 600		# <<<
	globillum photons 200000                # <<<
end light

instance "light1-i" "light1" end instance

material "paper" opaque				# diffuse white material
	"dgs_material" (
	    "diffuse"	0.8 0.8 0.8,		# <<<
	    "lights"	["light1-i"]
	)
	photon "dgs_material_photon" ()		# <<<
end material

object "box" visible shadow trace tag 1
	group
		-5	-5	-25
		-5	 5	-25
		 5	 5	-25
		 5	-5	-25
		-5	-5	-15
		-5	 5	-15
		 5	 5	-15
		 5	-5	-15
		v 0 v 1 v 2 v 3 v 4 v 5 v 6 v 7
		p "paper" 3 2 1 0		# back wall
		p	  0 1 4 5		# left wall
		p	  2 3 7 6		# right wall
		p	  4 7 3 0		# floor
		p	  1 2 6 5		# ceiling
	end group
end object

instance "cam-i" "cam" end instance
instance "box-i" "box" end instance
instgroup "rootgrp" "cam-i" "light1-i" "box-i" end instgroup
render "rootgrp" "cam-i" "opt"
#end

#------------------------------------------------------------------------------

incremental options "opt"
	globillum	off
end options

incremental camera "cam"
	output		"rgb" "out.1.rgb"
end camera

render "rootgrp" "cam-i" "opt"
