#----------------------------------------------------------------------------
# Copyright 1986-2000 by mental images GmbH & Co.KG, Fasanenstr. 81, D-10623
# Berlin, Germany. All rights reserved.
#----------------------------------------------------------------------------
#
# Chapter  9.4    : Contours on Reflections and Refractions
# Figure   9.3    : Contours from reflections
# Description     : Teapot with color and contours around the reflections
#       
#----------------------------------------------------------------------------
                            

verbose on
link "base.so"
link "contour.so"
$include <base.mi>
$include <contour.mi>

#begin
options "opt"
        samples		 0 3
	contour store	 "contour_store_function" ()
  	contour contrast "contour_contrast_function_levels" ( 
			    "zdelta"	 0.1, 
			    "ndelta"	 60.0, 
			    "max_level"	 2
			 )
	object space
end options
#end

camera "cam"
  	output		"rgb" "out.rgb"
	output		"contour,rgba" "contour_composite" ()
  	output		"rgb" "out.1.rgb"
        focal		120.0
        aperture	42.0 
        aspect		1.66
        resolution	800 480
end camera

light "light"
	"mib_light_point" (
	    "color"	1 1 1
        )
        origin		-5 5 -18
end light

instance "light-i" "light" end instance

shader "illum" "mib_illum_phong" (
	    "ambient"	0.5 0.5 0.5,
	    "ambience"	1.0 1.0 1.0,
	    "diffuse"	0.8 0.8 0.8,
	    "specular"	1.0 1.0 1.0,
	    "exponent"	50.0,
	    "lights"	["light-i"]
        )

material "white" opaque
        "mib_reflect" (
	    "input"	= "illum",
	    "reflect"	0.7 0.7 0.7 0.5
        )
        contour "contour_shader_simple" (
	    "color"	0.0 0.0 0.0 1.0,
	    "width"	0.5
        )
end material

$include "teapot.mi"	# 300 lines are too many to list here

instance "teapot-i" "teapot" 
        transform
                 0.97  0  0.24  0 
                 0     1  0     0
                -0.24  0  0.97  0
                 -5    4 20     1
        material "white"
end instance

instance "cam-i" "cam" 
	transform 
		1   0    0    0
		0  0.97 0.24  0
		0 -0.24 0.97  0
		0  -2    0    1 
end instance

instgroup "all"
        "cam-i" "light-i" "teapot-i" 
end instgroup

render "all" "cam-i" "opt"
