#----------------------------------------------------------------------------
# Copyright 1986-2000 by mental images GmbH & Co.KG, Fasanenstr. 81, D-10623
# Berlin, Germany. All rights reserved.
#----------------------------------------------------------------------------
#
# Chapter  4.2.1.2 : Repeated XY Projection
# Figure   4.7     : Repeating texture
# 
#----------------------------------------------------------------------------

verbose on
link "base.so"
$include <base.mi>

light "light1"
	"mib_light_point" (
	    "color"	1 1 1,
	    "factor"	0.75
	)
	origin		10 10 10
end light

instance "light1_inst" "light1"
end instance

options "opt"
	samples 	0 2
	contrast	0.1  0.1  0.1
	object space
end options

camera "cam"
	frame		1
	output		"rgb" "out.rgb"
	focal		1
	aperture	1
	aspect		1
	resolution	800 800
end camera

color texture "pic1" "tex_image.rgb"

declare phenomenon
	color "textured_mtl_sh" (
		color texture	"texture",
		array light	"lights"
	)
	shader "coord" "mib_texture_vector" (
		"select"	-1,
		"project"	2,
		"selspace"	1
	)
#begin
	shader "remap" "mib_texture_remap" (
		"input"		= "coord",
		"repeat"	6 6 0,
		"alt_x"		on,
		"alt_y"		on,
		"transform"	.1  0  0  0
				 0 .1  0  0
				 0  0  0  0
				.5 .5  0  1
	)
#end
	shader "tex" "mib_texture_lookup" (
		"tex"		= interface "texture",
		"coord"		= "remap"
	)
	shader "mtl" "mib_illum_phong" (
		"ambient"	= "tex",
		"diffuse"	= "tex",
		"ambience"	.1 .1 .1,
		"specular"	1 1 1,
		"exponent"	50,
		"mode"		1,
		"lights"	= interface "lights"
	)
	root = "mtl"
end declare

material "mtl"
	opaque
	"textured_mtl_sh" (
	    "texture"	"pic1",
	    "lights"	["light1_inst"],
	)
end material

$include "sphere.mi"

instance "sphere1_inst" "sphere"
	material	"mtl"
end instance

instance "cam_inst" "cam"
	transform	1  0  0  0
			0  1  0  0
			0  0  1  0
			0  0 -12 1
end instance

instgroup "rootgrp"
	"cam_inst" "light1_inst"  "sphere1_inst"
end instgroup

render "rootgrp" "cam_inst" "opt"
