# mental ray scene file from "Writing mental ray shaders"
# http://www.writingshaders.com/scene_catalog.html

verbose on


link "global_lambert.so"
$include "global_lambert.mi"
link "texture_uv.so"
$include "texture_uv.mi"
link "point_light_shadow.so"
$include "point_light_shadow.mi"
link "color_ramp.so"
$include "color_ramp.mi"
link "depth_of_field.so"
$include "depth_of_field.mi"

options "opt"
    object space
    samples -1 2
    contrast .1 .1 .1 1
    filter gauss
    scanline off
    finalgather on
    finalgather file "lens_scenes.fg"
    finalgather rebuild freeze
end options
 

shader "sky"
    "color_ramp" (
        "colors" [ 0 0 0 0, 
                   0 0 0 .47, 
                  .95 .95 1 .48, 
                  .1 .1 .6 1 ] )

color texture "octatile" "octatile.tif"

light "right_light"
    "point_light_shadow" (
        "light_color" .3 .3 .3
    )
    origin 6 6 6
end light

instance "right_light_i"
    "right_light"
end instance

light "left_light"
    "point_light_shadow" (
        "light_color" .3 .3 .3
    )
    origin -6 6 6
end light

instance "left_light_i"
    "left_light" 
end instance

light "back_light"
    "point_light_shadow" (
        "light_color" .3 .3 .3
    )
    origin 0 10 -40
end light

instance "back_light_i"
    "back_light" 
end instance

light "front_light"
    "point_light_shadow" (
        "light_color" .3 .3 .3
    )
    origin -.5 .6 10
end light

instance "front_light_i"
    "front_light" 
end instance

shader "octatile_pattern"
    "texture_uv" (
	"tex" "octatile", 
	"u_scale" 100,
	"v_scale" 100
    )

material "ground" opaque
    "global_lambert" (
         "diffuse" = "octatile_pattern",
         "lights" ["left_light_i", "right_light_i", "back_light_i"]
    )
end material

shader "single_octatile"
    "texture_uv" (
	"tex" "octatile" )

material "tile" opaque
    "global_lambert" (
        "diffuse" = "single_octatile",
        "lights" ["left_light_i", "right_light_i", "front_light_i"]
    )
end material

camera "camera"
    output "rgba" "tif" "lens_7.tif"
    focal 1.5
    aperture 1
    aspect 1
    resolution 300 300
    environment = "sky"
    lens
        "depth_of_field" (
            "focus_plane_distance" 9,
            "number_of_samples" 100,
            "blur_radius" .2 )
end camera

instance "camera-instance" "camera"
    transform 
        1 0 0 0 
        0 0.957826 0.287348 0 
        0 -0.287348 0.957826 0 
        0 0 -5.22015 1
end instance

instance "cube-1"
    geometry "bw_cube" ("name" "cube")
    material "ground"
    transform 
        -0.010 0.0 -0.0 0.0 
        0.0 100.0 0.0 0.0 
        0.0 0.0 -0.010 0.0 
        0.0 -9.50 0.0 1.0 
    tag 1
    globillum on
    globillum 3
end instance

instance "cube-2"
    geometry "bw_cube" ("name" "cube")
    material "tile"
    transform 
        5.0 0.0 0.0 0.0 
        0.0 0.833333 0.0 0.0 
        0.0 0.0 5.0 0.0 
        2.50 -0.580 -12.50 1.0 
    tag 2
    globillum on
    globillum 3
end instance

instance "cube-3"
    geometry "bw_cube" ("name" "cube")
    material "tile"
    transform 
        5.0 0.0 0.0 0.0 
        0.0 0.833333 0.0 0.0 
        0.0 0.0 5.0 0.0 
        0.944444 -0.580 -9.277778 1.0 
    tag 3
    globillum on
    globillum 3
end instance

instance "cube-4"
    geometry "bw_cube" ("name" "cube")
    material "tile"
    transform 
        5.0 0.0 0.0 0.0 
        0.0 0.833333 0.0 0.0 
        0.0 0.0 5.0 0.0 
        -0.611111 -0.580 -6.055556 1.0 
    tag 4
    globillum on
    globillum 3
end instance

instance "cube-5"
    geometry "bw_cube" ("name" "cube")
    material "tile"
    transform 
        5.0 0.0 0.0 0.0 
        0.0 0.833333 0.0 0.0 
        0.0 0.0 5.0 0.0 
        -2.166667 -0.580 -2.833333 1.0 
    tag 5
    globillum on
    globillum 3
end instance

instance "cube-6"
    geometry "bw_cube" ("name" "cube")
    material "tile"
    transform 
        5.0 0.0 0.0 0.0 
        0.0 0.833333 0.0 0.0 
        0.0 0.0 5.0 0.0 
        -3.722222 -0.580 0.388889 1.0 
    tag 6
    globillum on
    globillum 3
end instance

instance "cube-7"
    geometry "bw_cube" ("name" "cube")
    material "tile"
    transform 
        5.0 0.0 0.0 0.0 
        0.0 0.833333 0.0 0.0 
        0.0 0.0 5.0 0.0 
        -5.277778 -0.580 3.611111 1.0 
    tag 7
    globillum on
    globillum 3
end instance

instance "cube-8"
    geometry "bw_cube" ("name" "cube")
    material "tile"
    transform 
        5.0 0.0 0.0 0.0 
        0.0 0.833333 0.0 0.0 
        0.0 0.0 5.0 0.0 
        -6.833333 -0.580 6.833333 1.0 
    tag 8
    globillum on
    globillum 3
end instance

instance "cube-9"
    geometry "bw_cube" ("name" "cube")
    material "tile"
    transform 
        5.0 0.0 0.0 0.0 
        0.0 0.833333 0.0 0.0 
        0.0 0.0 5.0 0.0 
        -8.388889 -0.580 10.055556 1.0 
    tag 9
    globillum on
    globillum 3
end instance

instance "cube-10"
    geometry "bw_cube" ("name" "cube")
    material "tile"
    transform 
        5.0 0.0 0.0 0.0 
        0.0 0.833333 0.0 0.0 
        0.0 0.0 5.0 0.0 
        -9.944444 -0.580 13.277778 1.0 
    tag 10
    globillum on
    globillum 3
end instance

instance "cube-11"
    geometry "bw_cube" ("name" "cube")
    material "tile"
    transform 
        5.0 0.0 0.0 0.0 
        0.0 0.833333 0.0 0.0 
        0.0 0.0 5.0 0.0 
        -11.50 -0.580 16.50 1.0 
    tag 11
    globillum on
    globillum 3
end instance

instgroup "root"
    "camera-instance" "left_light_i" "right_light_i" "back_light_i" "front_light_i"    "cube-1" "cube-2" "cube-3" "cube-4" "cube-5" "cube-6" "cube-7" "cube-8" 
    "cube-9" "cube-10" "cube-11" 
end instgroup

render "root" "camera-instance" "opt"
 
