# mental ray scene file from "Writing mental ray shaders"
# http://www.writingshaders.com/scene_catalog.html

verbose on

link "lambert.so"
$include "lambert.mi"
link "texture_uv.so"
$include "texture_uv.mi"
link "ground_fog.so"
$include "ground_fog.mi"
link "point_light_shadow.so"
$include "point_light_shadow.mi"

options "opt"
    object space
    contrast .1 .1 .1 1
    samples 0 2
    volume on
end options

camera "cam"
    output "rgba" "tif"
        "atmosphere_4.tif"
    focal 1.5
    aperture 1
    aspect 1
    resolution 300 300
    volume
        "ground_fog" (
            "fog_color" .95 1 .9,
            "fade_start" -.8,
            "fade_end" -.1,
            "march_increment" .05
        )
end camera

instance "camera-instance" "cam"
    transform 
        1.0 0.0 0.0 0.0 
        0.0 1.0 0.0 0.0 
        0.0 0.0 1.0 0.0 
        0.0 0.0 -4.0 1.0 
end instance

 

light "light1"
    "point_light_shadow" (
         "light_color" .9 .9 .9
    )
    origin 6 4 5
end light

instance "light1-i" "light1" end instance

light "light2"
    "point_light_shadow" ()
    origin -6 4 2
end light

instance "light2-i" "light2" end instance

color texture "diamond" "diamond.tif"

shader "grid"
    "texture_uv" (
        "tex" "diamond",
        "u_scale" 20,
	"v_scale" 20
    )

shader "column_grid"
    "texture_uv" (
        "tex" "diamond",
        "u_scale" 1,
	"v_scale" 4
    )

material "gray"
    "lambert" (
	"lights" ["light1-i", "light2-i"],
        "diffuse" = "grid"
    )
end material

material "column"
    "lambert" (
	"lights" ["light1-i", "light2-i"],
        "diffuse" = "column_grid"
    )
end material


instance "cube-1"
    geometry "bw_cube" ("name" "cube")
    material "gray"
    transform 
        -0.010 0.0 0.0 0.0 
        0.0 -0.010 0.0 0.0 
        0.0 0.0 -0.010 0.0 
        0.0 0.0 0.0 1.0 
    tag 1
    globillum on
    globillum 3
end instance

instance "cube-2"
    geometry "bw_cube" ("name" "cube")
    material "gray"
    transform 
        0.10 0.0 0.0 0.0 
        0.0 100.0 0.0 0.0 
        0.0 0.0 0.10 0.0 
        0.0 100.0 0.0 1.0 
    tag 2
    globillum on
    globillum 3
end instance

instance "cube-3"
    geometry "bw_cube" ("name" "cube")
    material "gray"
    transform 
        0.10 0.0 0.0 0.0 
        0.0 -0.000004 -0.10 0.0 
        0.0 100.0 -0.0 0.0 
        0.0 100.0 -0.0 1.0 
    tag 3
    globillum on
    globillum 3
end instance

instance "cube-4"
    geometry "bw_cube" ("name" "cube")
    material "column"
    transform 
        4.714045 0.0 4.714045 0.0 
        0.0 0.666667 0.0 0.0 
        -4.714045 0.0 4.714045 0.0 
        14.142136 0.166667 -9.428090 1.0 
    tag 4
    globillum on
    globillum 3
end instance

instance "cube-5"
    geometry "bw_cube" ("name" "cube")
    material "column"
    transform 
        4.714045 0.0 4.714045 0.0 
        0.0 0.666667 0.0 0.0 
        -4.714045 0.0 4.714045 0.0 
        10.423278 0.166667 -8.432903 1.0 
    tag 5
    globillum on
    globillum 3
end instance

instance "cube-6"
    geometry "bw_cube" ("name" "cube")
    material "column"
    transform 
        4.714045 0.0 4.714045 0.0 
        0.0 0.666667 0.0 0.0 
        -4.714045 0.0 4.714045 0.0 
        6.704420 0.166667 -7.437716 1.0 
    tag 6
    globillum on
    globillum 3
end instance

instance "cube-7"
    geometry "bw_cube" ("name" "cube")
    material "column"
    transform 
        4.714045 0.0 4.714045 0.0 
        0.0 0.666667 0.0 0.0 
        -4.714045 0.0 4.714045 0.0 
        2.985562 0.166667 -6.442528 1.0 
    tag 7
    globillum on
    globillum 3
end instance

instance "cube-8"
    geometry "bw_cube" ("name" "cube")
    material "column"
    transform 
        4.714045 0.0 4.714045 0.0 
        0.0 0.666667 0.0 0.0 
        -4.714045 0.0 4.714045 0.0 
        -0.733296 0.166667 -5.447341 1.0 
    tag 8
    globillum on
    globillum 3
end instance

instance "cube-9"
    geometry "bw_cube" ("name" "cube")
    material "column"
    transform 
        4.714045 0.0 4.714045 0.0 
        0.0 0.666667 0.0 0.0 
        -4.714045 0.0 4.714045 0.0 
        -4.452154 0.166667 -4.452154 1.0 
    tag 9
    globillum on
    globillum 3
end instance

instance "cube-10"
    geometry "bw_cube" ("name" "cube")
    material "column"
    transform 
        4.714045 0.0 4.714045 0.0 
        0.0 0.666667 0.0 0.0 
        -4.714045 0.0 4.714045 0.0 
        -8.171012 0.166667 -3.456967 1.0 
    tag 10
    globillum on
    globillum 3
end instance

instance "cube-11"
    geometry "bw_cube" ("name" "cube")
    material "column"
    transform 
        4.714045 0.0 4.714045 0.0 
        0.0 0.666667 0.0 0.0 
        -4.714045 0.0 4.714045 0.0 
        -11.889870 0.166667 -2.461779 1.0 
    tag 11
    globillum on
    globillum 3
end instance

instance "cube-12"
    geometry "bw_cube" ("name" "cube")
    material "column"
    transform 
        4.714045 0.0 4.714045 0.0 
        0.0 0.666667 0.0 0.0 
        -4.714045 0.0 4.714045 0.0 
        -15.608727 0.166667 -1.466592 1.0 
    tag 12
    globillum on
    globillum 3
end instance

instance "cube-13"
    geometry "bw_cube" ("name" "cube")
    material "column"
    transform 
        4.714045 0.0 4.714045 0.0 
        0.0 0.666667 0.0 0.0 
        -4.714045 0.0 4.714045 0.0 
        -19.327585 0.166667 -0.471405 1.0 
    tag 13
    globillum on
    globillum 3
end instance

instgroup "root"
    "camera-instance"
    "light1-i" "light2-i"    "cube-1" "cube-2" "cube-3" "cube-4" "cube-5" "cube-6" "cube-7" "cube-8" 
    "cube-9" "cube-10" "cube-11" "cube-12" "cube-13" 
end instgroup

render "root" "camera-instance" "opt"
 
