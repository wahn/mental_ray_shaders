# mental ray scene file from "Writing mental ray shaders"
# http://www.writingshaders.com/scene_catalog.html

verbose on

link "lambert.so"
$include "lambert.mi"
link "texture_uv.so"
$include "texture_uv.mi"
link "point_light_shadow.so"
$include "point_light_shadow.mi"
link "median_filter.so"
$include "median_filter.mi"

options "opt"
    object space
    samples 0 2
    contrast .1 .1 .1 1
    trace depth 2 2 4
    frame buffer 0 "+rgba"
end options
 

color texture "octatile" "octatile.tif"

light "right_light"
    "point_light_shadow" ()
    origin 6 6 6
end light

instance "right_light_i"
    "right_light"
end instance

light "left_light"
    "point_light_shadow" ()
    origin -6 6 6
end light

instance "left_light_i"
    "left_light" 
end instance

shader "octatile_pattern"
    "texture_uv" (
	"tex" "octatile",
	"u_scale" 100,
	"v_scale" 100
    )

material "ground" opaque
    "lambert" (
         "diffuse" = "octatile_pattern",
         "lights" ["left_light_i", "right_light_i"]
    )
end material

shader "single_octatile"
    "texture_uv" (
	"tex" "octatile" )

material "cube" opaque
    "lambert" (
        "diffuse" = "single_octatile",
        "lights" ["left_light_i", "right_light_i"]
    )
end material
    
camera "cam"
   output "rgba" 
      "median_filter" ( 
         "radius" 4 )
   output "rgba" "tif" 
      "output_4.tif"
   output "fb0" "tif"  
      "output_4_median.tif"
   focal 1.5
   aperture 1
   aspect 1.333
   resolution 400 300
end camera

instance "camera-instance" "cam"
    transform 
        1 0 0 0 
        0 0.957826 0.287348 0 
        0 -0.287348 0.957826 0 
        0 0 -5.22015 1 
end instance

instance "cube-1"
    geometry "bw_cube" ("name" "cube")
    material "ground"
    transform 
        -0.010 0.0 -0.0 0.0 
        0.0 100.0 0.0 0.0 
        0.0 0.0 -0.010 0.0 
        0.0 -9.50 0.0 1.0 
    tag 1
    globillum on
    globillum 3
end instance

instance "cube-2"
    geometry "bw_cube" ("name" "cube")
    material "cube"
    transform 
        5.0 0.0 0.0 0.0 
        0.0 1.250 0.0 0.0 
        0.0 0.0 5.0 0.0 
        2.50 -0.6250 -12.50 1.0 
    tag 2
    globillum on
    globillum 3
end instance

instance "cube-3"
    geometry "bw_cube" ("name" "cube")
    material "cube"
    transform 
        5.0 0.0 0.0 0.0 
        0.0 1.250 0.0 0.0 
        0.0 0.0 5.0 0.0 
        1.055556 -0.6250 -9.277778 1.0 
    tag 3
    globillum on
    globillum 3
end instance

instance "cube-4"
    geometry "bw_cube" ("name" "cube")
    material "cube"
    transform 
        5.0 0.0 0.0 0.0 
        0.0 1.250 0.0 0.0 
        0.0 0.0 5.0 0.0 
        -0.388889 -0.6250 -6.055556 1.0 
    tag 4
    globillum on
    globillum 3
end instance

instance "cube-5"
    geometry "bw_cube" ("name" "cube")
    material "cube"
    transform 
        5.0 0.0 0.0 0.0 
        0.0 1.250 0.0 0.0 
        0.0 0.0 5.0 0.0 
        -1.833333 -0.6250 -2.833333 1.0 
    tag 5
    globillum on
    globillum 3
end instance

instance "cube-6"
    geometry "bw_cube" ("name" "cube")
    material "cube"
    transform 
        5.0 0.0 0.0 0.0 
        0.0 1.250 0.0 0.0 
        0.0 0.0 5.0 0.0 
        -3.277778 -0.6250 0.388889 1.0 
    tag 6
    globillum on
    globillum 3
end instance

instance "cube-7"
    geometry "bw_cube" ("name" "cube")
    material "cube"
    transform 
        5.0 0.0 0.0 0.0 
        0.0 1.250 0.0 0.0 
        0.0 0.0 5.0 0.0 
        -4.722222 -0.6250 3.611111 1.0 
    tag 7
    globillum on
    globillum 3
end instance

instance "cube-8"
    geometry "bw_cube" ("name" "cube")
    material "cube"
    transform 
        5.0 0.0 0.0 0.0 
        0.0 1.250 0.0 0.0 
        0.0 0.0 5.0 0.0 
        -6.166667 -0.6250 6.833333 1.0 
    tag 8
    globillum on
    globillum 3
end instance

instance "cube-9"
    geometry "bw_cube" ("name" "cube")
    material "cube"
    transform 
        5.0 0.0 0.0 0.0 
        0.0 1.250 0.0 0.0 
        0.0 0.0 5.0 0.0 
        -7.611111 -0.6250 10.055556 1.0 
    tag 9
    globillum on
    globillum 3
end instance

instance "cube-10"
    geometry "bw_cube" ("name" "cube")
    material "cube"
    transform 
        5.0 0.0 0.0 0.0 
        0.0 1.250 0.0 0.0 
        0.0 0.0 5.0 0.0 
        -9.055556 -0.6250 13.277778 1.0 
    tag 10
    globillum on
    globillum 3
end instance

instance "cube-11"
    geometry "bw_cube" ("name" "cube")
    material "cube"
    transform 
        5.0 0.0 0.0 0.0 
        0.0 1.250 0.0 0.0 
        0.0 0.0 5.0 0.0 
        -10.50 -0.6250 16.50 1.0 
    tag 11
    globillum on
    globillum 3
end instance

instgroup "root"
    "camera-instance" "left_light_i" "right_light_i"    "cube-1" "cube-2" "cube-3" "cube-4" "cube-5" "cube-6" "cube-7" "cube-8" 
    "cube-9" "cube-10" "cube-11" 
end instgroup

render "root" "camera-instance" "opt"
 
