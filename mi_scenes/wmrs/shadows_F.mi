# mental ray scene file from "Writing mental ray shaders"
# http://www.writingshaders.com/scene_catalog.html

verbose on

link "lambert.so"
$include "lambert.mi"
link "transparent_shadow.so"
$include "transparent_shadow.mi"
link "point_light_shadow.so"
$include "point_light_shadow.mi"
link "texture_uv.so"
$include "texture_uv.mi"
link "transparent.so"
$include "transparent.mi"

options "opt"
    object space
    globillum off
    face both
    trace depth 2 2 2
    samples 2 2
    contrast .1 .1 .1
end options
 

light "white_light"
    "point_light_shadow" ("light_color" 1.1 1.1 1.1)
    origin 0 7 -9
end light

instance "light_instance" 
    "white_light" 
end instance

material "gray" opaque
    "lambert" (
        "diffuse" 1 1 1,
        "lights" ["light_instance"]
    )
end material


color texture "fourgrid" "octatile.tif"

#color texture "colorchart" "color_chart.tif"

color texture "colorchart" "colorstrip.tif"

filter 8 color texture "diamond" "gray_diamond.map"

#color texture "diamond" "diamond.tif"

shader "diamondgrid"
    "texture_uv" (
       "tex" "diamond",
       "u_scale" 400,
       "v_scale" 400
    )

material "diamondtile" opaque
    "lambert" (
        "diffuse" = "diamondgrid",
        "lights" ["light_instance"]
    )
end material


shader "grid"
    "texture_uv" (
	"tex" "colorchart",
	"u_scale" 1,
	"v_scale" 1
    )



camera "cam"
    output "rgba" "tif"
        "shadows_F.tif"
    focal 65
    aperture 33.3
    aspect 1.66667
    resolution 500 300
end camera

instance "camera-instance" "cam"
    transform 
        1.0 0.0 0.0 0.0 
        0.0 0.945151 0.326633 0.0 
        0.0 -0.326633 0.945151 0.0 
        0.0 0.330803 -7.080294 1.0 
end instance

instance "cube-1"
    geometry "bw_cube" ("name" "cube")
    material "diamondtile"
    transform 
        0.010 0.0 0.0 0.0 
        0.0 100.0 0.0 0.0 
        0.0 0.0 0.010 0.0 
        0.0 55.0 0.0 1.0 
    tag 1
    globillum on
    globillum 3
end instance

material "shadow_transparent_0"
    "transparent_shadow" (
        "color" = "grid",
        "transparency" 0.00 0.00 0.00,
        "breakpoint" .25,
        "center" .25,
        "extent" .25
    )
    shadow
        "transparent_shadow" ()
end material

instance "square-1"
    geometry "bw_square" ()
    material "shadow_transparent_0"
    transform 
        9.708738 0.0 0.0 0.0 
        0.0 0.861085 0.0 0.0 
        0.0 0.0 1.0 0.0 
        10.970874 -0.086109 0.0 1.0 
    tag 2
    globillum on
    globillum 3
end instance

material "shadow_transparent_5"
    "transparent_shadow" (
        "color" = "grid",
        "transparency" 0.05 0.05 0.05,
        "breakpoint" .25,
        "center" .25,
        "extent" .25
    )
    shadow
        "transparent_shadow" ()
end material

instance "square-2"
    geometry "bw_square" ()
    material "shadow_transparent_5"
    transform 
        9.708738 0.0 0.0 0.0 
        0.0 0.861085 0.0 0.0 
        0.0 0.0 1.0 0.0 
        9.873786 -0.086109 0.0 1.0 
    tag 3
    globillum on
    globillum 3
end instance

material "shadow_transparent_10"
    "transparent_shadow" (
        "color" = "grid",
        "transparency" 0.10 0.10 0.10,
        "breakpoint" .25,
        "center" .25,
        "extent" .25
    )
    shadow
        "transparent_shadow" ()
end material

instance "square-3"
    geometry "bw_square" ()
    material "shadow_transparent_10"
    transform 
        9.708738 0.0 0.0 0.0 
        0.0 0.861085 0.0 0.0 
        0.0 0.0 1.0 0.0 
        8.776699 -0.086109 0.0 1.0 
    tag 4
    globillum on
    globillum 3
end instance

material "shadow_transparent_15"
    "transparent_shadow" (
        "color" = "grid",
        "transparency" 0.15 0.15 0.15,
        "breakpoint" .25,
        "center" .25,
        "extent" .25
    )
    shadow
        "transparent_shadow" ()
end material

instance "square-4"
    geometry "bw_square" ()
    material "shadow_transparent_15"
    transform 
        9.708738 0.0 0.0 0.0 
        0.0 0.861085 0.0 0.0 
        0.0 0.0 1.0 0.0 
        7.679612 -0.086109 0.0 1.0 
    tag 5
    globillum on
    globillum 3
end instance

material "shadow_transparent_20"
    "transparent_shadow" (
        "color" = "grid",
        "transparency" 0.20 0.20 0.20,
        "breakpoint" .25,
        "center" .25,
        "extent" .25
    )
    shadow
        "transparent_shadow" ()
end material

instance "square-5"
    geometry "bw_square" ()
    material "shadow_transparent_20"
    transform 
        9.708738 0.0 0.0 0.0 
        0.0 0.861085 0.0 0.0 
        0.0 0.0 1.0 0.0 
        6.582524 -0.086109 0.0 1.0 
    tag 6
    globillum on
    globillum 3
end instance

material "shadow_transparent_25"
    "transparent_shadow" (
        "color" = "grid",
        "transparency" 0.25 0.25 0.25,
        "breakpoint" .25,
        "center" .25,
        "extent" .25
    )
    shadow
        "transparent_shadow" ()
end material

instance "square-6"
    geometry "bw_square" ()
    material "shadow_transparent_25"
    transform 
        9.708738 0.0 0.0 0.0 
        0.0 0.861085 0.0 0.0 
        0.0 0.0 1.0 0.0 
        5.485437 -0.086109 0.0 1.0 
    tag 7
    globillum on
    globillum 3
end instance

material "shadow_transparent_30"
    "transparent_shadow" (
        "color" = "grid",
        "transparency" 0.30 0.30 0.30,
        "breakpoint" .25,
        "center" .25,
        "extent" .25
    )
    shadow
        "transparent_shadow" ()
end material

instance "square-7"
    geometry "bw_square" ()
    material "shadow_transparent_30"
    transform 
        9.708738 0.0 0.0 0.0 
        0.0 0.861085 0.0 0.0 
        0.0 0.0 1.0 0.0 
        4.388350 -0.086109 0.0 1.0 
    tag 8
    globillum on
    globillum 3
end instance

material "shadow_transparent_35"
    "transparent_shadow" (
        "color" = "grid",
        "transparency" 0.35 0.35 0.35,
        "breakpoint" .25,
        "center" .25,
        "extent" .25
    )
    shadow
        "transparent_shadow" ()
end material

instance "square-8"
    geometry "bw_square" ()
    material "shadow_transparent_35"
    transform 
        9.708738 0.0 0.0 0.0 
        0.0 0.861085 0.0 0.0 
        0.0 0.0 1.0 0.0 
        3.291262 -0.086109 0.0 1.0 
    tag 9
    globillum on
    globillum 3
end instance

material "shadow_transparent_40"
    "transparent_shadow" (
        "color" = "grid",
        "transparency" 0.40 0.40 0.40,
        "breakpoint" .25,
        "center" .25,
        "extent" .25
    )
    shadow
        "transparent_shadow" ()
end material

instance "square-9"
    geometry "bw_square" ()
    material "shadow_transparent_40"
    transform 
        9.708738 0.0 0.0 0.0 
        0.0 0.861085 0.0 0.0 
        0.0 0.0 1.0 0.0 
        2.194175 -0.086109 0.0 1.0 
    tag 10
    globillum on
    globillum 3
end instance

material "shadow_transparent_45"
    "transparent_shadow" (
        "color" = "grid",
        "transparency" 0.45 0.45 0.45,
        "breakpoint" .25,
        "center" .25,
        "extent" .25
    )
    shadow
        "transparent_shadow" ()
end material

instance "square-10"
    geometry "bw_square" ()
    material "shadow_transparent_45"
    transform 
        9.708738 0.0 0.0 0.0 
        0.0 0.861085 0.0 0.0 
        0.0 0.0 1.0 0.0 
        1.097087 -0.086109 0.0 1.0 
    tag 11
    globillum on
    globillum 3
end instance

material "shadow_transparent_50"
    "transparent_shadow" (
        "color" = "grid",
        "transparency" 0.50 0.50 0.50,
        "breakpoint" .25,
        "center" .25,
        "extent" .25
    )
    shadow
        "transparent_shadow" ()
end material

instance "square-11"
    geometry "bw_square" ()
    material "shadow_transparent_50"
    transform 
        9.708738 0.0 0.0 0.0 
        0.0 0.861085 0.0 0.0 
        0.0 0.0 1.0 0.0 
        0.0 -0.086109 0.0 1.0 
    tag 12
    globillum on
    globillum 3
end instance

material "shadow_transparent_55"
    "transparent_shadow" (
        "color" = "grid",
        "transparency" 0.55 0.55 0.55,
        "breakpoint" .25,
        "center" .25,
        "extent" .25
    )
    shadow
        "transparent_shadow" ()
end material

instance "square-12"
    geometry "bw_square" ()
    material "shadow_transparent_55"
    transform 
        9.708738 0.0 0.0 0.0 
        0.0 0.861085 0.0 0.0 
        0.0 0.0 1.0 0.0 
        -1.097087 -0.086109 0.0 1.0 
    tag 13
    globillum on
    globillum 3
end instance

material "shadow_transparent_60"
    "transparent_shadow" (
        "color" = "grid",
        "transparency" 0.60 0.60 0.60,
        "breakpoint" .25,
        "center" .25,
        "extent" .25
    )
    shadow
        "transparent_shadow" ()
end material

instance "square-13"
    geometry "bw_square" ()
    material "shadow_transparent_60"
    transform 
        9.708738 0.0 0.0 0.0 
        0.0 0.861085 0.0 0.0 
        0.0 0.0 1.0 0.0 
        -2.194175 -0.086109 0.0 1.0 
    tag 14
    globillum on
    globillum 3
end instance

material "shadow_transparent_65"
    "transparent_shadow" (
        "color" = "grid",
        "transparency" 0.65 0.65 0.65,
        "breakpoint" .25,
        "center" .25,
        "extent" .25
    )
    shadow
        "transparent_shadow" ()
end material

instance "square-14"
    geometry "bw_square" ()
    material "shadow_transparent_65"
    transform 
        9.708738 0.0 0.0 0.0 
        0.0 0.861085 0.0 0.0 
        0.0 0.0 1.0 0.0 
        -3.291262 -0.086109 0.0 1.0 
    tag 15
    globillum on
    globillum 3
end instance

material "shadow_transparent_70"
    "transparent_shadow" (
        "color" = "grid",
        "transparency" 0.70 0.70 0.70,
        "breakpoint" .25,
        "center" .25,
        "extent" .25
    )
    shadow
        "transparent_shadow" ()
end material

instance "square-15"
    geometry "bw_square" ()
    material "shadow_transparent_70"
    transform 
        9.708738 0.0 0.0 0.0 
        0.0 0.861085 0.0 0.0 
        0.0 0.0 1.0 0.0 
        -4.388350 -0.086109 0.0 1.0 
    tag 16
    globillum on
    globillum 3
end instance

material "shadow_transparent_75"
    "transparent_shadow" (
        "color" = "grid",
        "transparency" 0.75 0.75 0.75,
        "breakpoint" .25,
        "center" .25,
        "extent" .25
    )
    shadow
        "transparent_shadow" ()
end material

instance "square-16"
    geometry "bw_square" ()
    material "shadow_transparent_75"
    transform 
        9.708738 0.0 0.0 0.0 
        0.0 0.861085 0.0 0.0 
        0.0 0.0 1.0 0.0 
        -5.485437 -0.086109 0.0 1.0 
    tag 17
    globillum on
    globillum 3
end instance

material "shadow_transparent_80"
    "transparent_shadow" (
        "color" = "grid",
        "transparency" 0.80 0.80 0.80,
        "breakpoint" .25,
        "center" .25,
        "extent" .25
    )
    shadow
        "transparent_shadow" ()
end material

instance "square-17"
    geometry "bw_square" ()
    material "shadow_transparent_80"
    transform 
        9.708738 0.0 0.0 0.0 
        0.0 0.861085 0.0 0.0 
        0.0 0.0 1.0 0.0 
        -6.582524 -0.086109 0.0 1.0 
    tag 18
    globillum on
    globillum 3
end instance

material "shadow_transparent_85"
    "transparent_shadow" (
        "color" = "grid",
        "transparency" 0.85 0.85 0.85,
        "breakpoint" .25,
        "center" .25,
        "extent" .25
    )
    shadow
        "transparent_shadow" ()
end material

instance "square-18"
    geometry "bw_square" ()
    material "shadow_transparent_85"
    transform 
        9.708738 0.0 0.0 0.0 
        0.0 0.861085 0.0 0.0 
        0.0 0.0 1.0 0.0 
        -7.679612 -0.086109 0.0 1.0 
    tag 19
    globillum on
    globillum 3
end instance

material "shadow_transparent_90"
    "transparent_shadow" (
        "color" = "grid",
        "transparency" 0.90 0.90 0.90,
        "breakpoint" .25,
        "center" .25,
        "extent" .25
    )
    shadow
        "transparent_shadow" ()
end material

instance "square-19"
    geometry "bw_square" ()
    material "shadow_transparent_90"
    transform 
        9.708738 0.0 0.0 0.0 
        0.0 0.861085 0.0 0.0 
        0.0 0.0 1.0 0.0 
        -8.776699 -0.086109 0.0 1.0 
    tag 20
    globillum on
    globillum 3
end instance

material "shadow_transparent_95"
    "transparent_shadow" (
        "color" = "grid",
        "transparency" 0.95 0.95 0.95,
        "breakpoint" .25,
        "center" .25,
        "extent" .25
    )
    shadow
        "transparent_shadow" ()
end material

instance "square-20"
    geometry "bw_square" ()
    material "shadow_transparent_95"
    transform 
        9.708738 0.0 0.0 0.0 
        0.0 0.861085 0.0 0.0 
        0.0 0.0 1.0 0.0 
        -9.873786 -0.086109 0.0 1.0 
    tag 21
    globillum on
    globillum 3
end instance

material "shadow_transparent_100"
    "transparent_shadow" (
        "color" = "grid",
        "transparency" 1.00 1.00 1.00,
        "breakpoint" .25,
        "center" .25,
        "extent" .25
    )
    shadow
        "transparent_shadow" ()
end material

instance "square-21"
    geometry "bw_square" ()
    material "shadow_transparent_100"
    transform 
        9.708738 0.0 0.0 0.0 
        0.0 0.861085 0.0 0.0 
        0.0 0.0 1.0 0.0 
        -10.970874 -0.086109 0.0 1.0 
    tag 22
    globillum on
    globillum 3
end instance

instgroup "root"
    "camera-instance"
    "light_instance"    "cube-1" "square-1" "square-2" "square-3" "square-4" "square-5" "square-6" "square-7" 
    "square-8" "square-9" "square-10" "square-11" "square-12" "square-13" "square-14" "square-15" 
    "square-16" "square-17" "square-18" "square-19" "square-20" "square-21" 
end instgroup

render "root" "camera-instance" "opt"
 
