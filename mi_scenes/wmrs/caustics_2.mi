# mental ray scene file from "Writing mental ray shaders"
# http://www.writingshaders.com/scene_catalog.html

verbose on

link "store_diffuse_photon.so"
$include "store_diffuse_photon.mi"
link "global_lambert.so"
$include "global_lambert.mi"
link "spotlight.so"
$include "spotlight.mi"
link "specular_refraction.so"
$include "specular_refraction.mi"
link "transmit_specular_photon.so"
$include "transmit_specular_photon.mi"
link "displace_ripple.so"
$include "displace_ripple.mi"

options "opt"
    object space
    contrast .1 .1 .1 1
    samples -1 2
    shadow on
    displace on
    max displace .2
    caustic on
    caustic accuracy 500 .1
end options
 

light "light"
    "spotlight" ()
    origin 1 5 0
    direction -.14 -1 -.1
    spread .912
    energy 800 800 800
    globillum photons 50000
    caustic photons 100000
end light

instance "light_inst" "light"
end instance

material "global_diffuse"
    "global_lambert" (
        "diffuse" .8 .9 1,
        "lights" ["light_inst"]
    )
    photon
        "store_diffuse_photon" (
            "diffuse_color" .8 .9 1
         )    
end material    


material "refract" 
    "specular_refraction" (
        "index_of_refraction" 1.5 )
    displace
        "displace_ripple" (
            "center" .2 .5 0,
            "frequency" 20,
            "amplitude" .01 )
        "displace_ripple" (
            "center" .8 .8 0,
            "frequency" 20,
            "amplitude" .01 )
        "displace_ripple" (
            "center" .8 .2 0,
            "frequency" 20,
            "amplitude" .01 )
    photon 
        "transmit_specular_photon" (
            "index_of_refraction" 1.5 )
end material



camera "cam"
    output "rgba" "tif"
        "caustics_2.tif"
    focal 1.5
    aperture 1
    aspect 1.5
    resolution 300 200
end camera

instance "camera-instance" "cam"
    transform 
        1.0 0.0 0.0 0.0 
        0.0 0.908490 0.417906 0.0 
        0.0 -0.417906 0.908490 0.0 
        0.0 0.272547 -5.378263 1.0 
end instance

instance "cube-1"
    geometry "bw_cube" ("name" "cube")
    material "global_diffuse"
    transform 
        0.010 0.0 0.0 0.0 
        0.0 10.0 0.0 0.0 
        0.0 0.0 0.010 0.0 
        0.0 8.0 0.0 1.0 
    tag 1
    globillum on
    globillum 3
end instance

instance "cone-1"
    geometry "bw_cone" ("name" "bw-cone")
    material "global_diffuse"
    transform 
        0.223607 -0.447214 -0.866025 0.0 
        -0.894427 -0.447214 0.0 0.0 
        -0.387298 0.774597 -0.50 0.0 
        0.085525 -0.789083 -0.592820 1.0 
    tag 2
    globillum on
    globillum 3
end instance

instance "ball-1"
    geometry "bw_ball" ("name" "bw-ball")
    material "global_diffuse"
    transform 
        1.818182 0.0 0.0 0.0 
        0.0 -0.0 -1.818182 0.0 
        0.0 1.818182 -0.0 0.0 
        -1.636364 0.545455 -0.0 1.0 
    tag 3
    globillum on
    globillum 3
end instance

instance "column-1"
    geometry "bw_cylinder" ("name" "bw-column")
    material "global_diffuse"
    transform 
        1.812616 -0.845237 0.0 0.0 
        0.0 -0.0 -2.0 0.0 
        0.845237 1.812616 -0.0 0.0 
        -1.111022 -1.909353 -0.50 1.0 
    tag 4
    globillum on
    globillum 3
end instance

instance "square-1"
    geometry "bw_square" ()
    material "global_diffuse"
    transform 
        0.010 0.0 0.0 0.0 
        0.0 -0.0 0.010 0.0 
        0.0 -0.010 -0.0 0.0 
        0.0 -0.0 0.0050 1.0 
    tag 5
    globillum on
    globillum 3
end instance

instance "square-2"
    geometry "bw_square" ()
    material "refract"
    transform 
        0.20 0.0 0.0 0.0 
        0.0 -0.0 1.0 0.0 
        0.0 -0.232558 -0.0 0.0 
        0.0 -0.116279 -0.60 1.0 
    tag 6
    globillum on
    globillum 3
end instance

instgroup "root"
    "camera-instance"
    "light_inst"    "cube-1" "cone-1" "ball-1" "column-1" "square-1" "square-2" 
end instgroup

render "root" "camera-instance" "opt"
 
