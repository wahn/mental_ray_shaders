# mental ray scene file from "Writing mental ray shaders"
# http://www.writingshaders.com/scene_catalog.html

verbose on

link "lambert.so"
$include "lambert.mi"
link "one_color.so"
$include "one_color.mi"
link "glossy_reflection_sample_varying.so"
$include "glossy_reflection_sample_varying.mi"
link "texture_uv.so"
$include "texture_uv.mi"
link "point_light.so"
$include "point_light.mi"

options "opt"
    object space
    samples 0 2
    contrast .1 .1 .1
    trace depth 5
end options
 
color texture "octatile" "octatile.tif"

light "white_light"
    "point_light" (
        "light_color"  1.2 1.2 1.2
    )
    origin 0 10 0
end light

instance "light_instance" 
    "white_light" 
end instance

material "reflect" opaque
    "glossy_reflection_sample_varying" (
        "shiny" 3,
        "samples" [200, 1]
    )
end material

shader "octatile_pattern"
    "texture_uv" (
	"tex" "octatile", 
	"u_scale" 100,
	"v_scale" 100
    )

material "tile" opaque
    "lambert" (
         "diffuse" = "octatile_pattern",
         "lights" ["light_instance"]
    )
end material

camera "cam"
    output "rgba" "tif"
        "reflection_9.tif"
    focal 50
    aperture 33.3
    aspect 1.5
    resolution 300 200
    environment
        "one_color" (
            "color" 1 1 .9
        )
end camera

instance "camera-instance" "cam"
    transform 
        1.0 0.0 0.0 0.0 
        0.0 0.957826 0.287348 0.0 
        0.0 -0.287348 0.957826 0.0 
        0.0 0.0 -5.220153 1.0 
end instance

instance "cube-1"
    geometry "bw_cube" ("name" "cube")
    material "tile"
    transform 
        -0.010 0.0 -0.0 0.0 
        0.0 10.0 0.0 0.0 
        0.0 0.0 -0.010 0.0 
        0.0 8.0 0.0 1.0 
    tag 1
    globillum on
    globillum 3
end instance

instance "ball-1"
    geometry "bw_ball" ("name" "bw-pball")
    material "reflect"
    transform 
        2.0 0.0 0.0 0.0 
        0.0 2.0 0.0 0.0 
        0.0 0.0 2.0 0.0 
        1.40 0.10 -2.0 1.0 
    tag 2
    globillum on
    globillum 3
end instance

instance "ball-2"
    geometry "bw_ball" ("name" "bw-pball")
    material "reflect"
    transform 
        2.0 0.0 0.0 0.0 
        0.0 2.0 0.0 0.0 
        0.0 0.0 2.0 0.0 
        -1.40 0.10 -2.0 1.0 
    tag 3
    globillum on
    globillum 3
end instance

instance "ball-3"
    geometry "bw_ball" ("name" "bw-pball")
    material "reflect"
    transform 
        2.0 0.0 0.0 0.0 
        0.0 2.0 0.0 0.0 
        0.0 0.0 2.0 0.0 
        0.0 -1.10 0.0 1.0 
    tag 4
    globillum on
    globillum 3
end instance

instgroup "root"
    "camera-instance"
    "light_instance"    "cube-1" "ball-1" "ball-2" "ball-3" 
end instgroup

render "root" "camera-instance" "opt"
 
