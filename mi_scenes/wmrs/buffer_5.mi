# mental ray scene file from "Writing mental ray shaders"
# http://www.writingshaders.com/scene_catalog.html

verbose on

link "one_color.so"
$include "one_color.mi"
link "phong.so"
$include "phong.mi"
link "spotlight.so"
$include "spotlight.mi"
link "average_radiance.so"
$include "average_radiance.mi"
link "add_colors.so"
$include "add_colors.mi"
link "named_framebuffer_put.so"
$include "named_framebuffer_put.mi"
link "store_diffuse_photon.so"
$include "store_diffuse_photon.mi"
link "op_mul_cc.so"
$include "op_mul_cc.mi"

options "opt"
    object space
    contrast .1 .1 .1 1
    samples 0 2
    finalgather on
    finalgather accuracy 50 2 .5
end options
 

light "light"
    "spotlight" ( "light_color"  .6 .6 .6 )
    origin 4 -.5 7
    direction  -4 .5 -7
    spread .9
end light

instance "light_inst" "light" end instance

declare phenomenon
    "global_phong_components" (
        color "diffuse" default .5 .5 .5,
	color "specular" default .3 .3 .3,
	scalar "exponent" default 30,
	array light "lights" )

    shader "diffuse" 
        "phong" (
            "diffuse" = interface "diffuse",
            "lights" = interface "lights",
            "specular" 0 0 0 )

    shader "specular" 
        "phong" (
            "specular" = interface "specular",
            "exponent" = interface "exponent",
            "lights" = interface "lights",
            "diffuse" 0 0 0 )

    shader "indirect" 
        "average_radiance" ()

    shader "indirect_diffuse"
        "op_mul_cc" (
            "A" = "indirect",
            "B" = interface "diffuse" )

    shader "write_diffuse" 
        "named_framebuffer_put" (
            "color" = "diffuse",
            "name" "diffuse" )

    shader "write_specular" 
        "named_framebuffer_put" (
            "color" = "specular",
            "name" "specular" )

    shader "write_indirect" 
        "named_framebuffer_put" (
            "color" = "indirect_diffuse",
            "name" "indirect" )

    shader "add_direct" 
        "add_colors" (
            "x" = "write_diffuse",
            "y" = "write_specular" )

    shader "add_indirect" 
        "add_colors" (
            "x" = "add_direct",
            "y" = "write_indirect" )

    root = "add_indirect"
end declare


declare phenomenon
    material "global_phong" (
        color "diffuse" default 1 1 1,
        array light "lights" )

    material "phong_buffers" 
        "global_phong_components" (
            "diffuse" = interface "diffuse",
            "specular"  1 1 1,
            "lights" = interface "lights" )
        photon
            "store_diffuse_photon" ( 
                "diffuse_color" = interface "diffuse" )
    end material

    root material "phong_buffers"
end declare


# This material Phenomenon is specific to this scene as an shortcut -- all
# the materials will be using the same specular value, light instance.
# This Phenomenon also uses the same color for the diffuse value of
# phong and the photon shader.

light "light"
    "spotlight" ( 
        "light_color" .6 .6 .6 )
    origin 4 -.5 7
    direction  -4 .5 -7
    spread .9
end light

instance "light_inst" "light" end instance

declare phenomenon
    material "global_phong_with_light" (
        color "diffuse" default 1 1 1 )

    material "phong_buffers" 
        "global_phong_components" (
            "diffuse" = interface "diffuse",
            "specular"  1 1 1,
            "lights" [ "::light_inst" ] )
        photon
            "store_diffuse_photon" ( 
                "diffuse_color" = interface "diffuse" )
    end material

    root material "phong_buffers"
end declare

shader "red"      
    "global_phong_with_light" ( 
        "diffuse" 1 .4 .4 )
shader "yellow"
    "global_phong_with_light" ( 
        "diffuse" .8 .7 .4 )
shader "blue"
    "global_phong_with_light" ( 
        "diffuse" .4 .4 1 )
shader "darkgray"
    "global_phong_with_light" ( 
        "diffuse" .1 .1 .1 )
shader "white"
    "global_phong_with_light" ( 
        "diffuse" 1 1 1 )

camera "cam"
    framebuffer "primary"
        datatype "rgba"
	filtering on
	primary on
	filename "buffer_5.tif"
    framebuffer "diffuse"
        datatype "rgba"
	filtering on
	user on
	filename "buffer_5_diffuse.tif"
    framebuffer "specular"
        datatype "rgba"
	filtering on
	user on
	filename "buffer_5_specular.tif"
    framebuffer "indirect"
        datatype "rgba"
	filtering on
	user on
	filename "buffer_5_indirect.tif"
    focal 90
    aperture 33.3
    aspect 1.4
    resolution 420 300
    environment
        "one_color" (
            "color" .1 .1 .2 )
end camera

instance "camera-instance" "cam"
    transform 
        1 0 0 0 
        0 1 0 0 
        0 0 1 0 
        0 0 -8 1 
end instance

$include "coniral.mi"

instance "coniral-inst-1" "coniral"
    material "yellow"
    transform 
        0.666667 0.0 0.0 0.0 
        0.0 0.769231 0.0 0.0 
        0.0 0.0 0.666667 0.0 
        0.266667 0.076923 -0.333333 1.0 
    tag 1
    globillum on
    globillum 3
end instance

$include "spheral.mi"

instance "spheral-inst-1" "spheral"
    material "blue"
    transform 
        1.538462 0.0 0.0 0.0 
        0.0 1.538462 0.0 0.0 
        0.0 0.0 1.538462 0.0 
        -1.153846 0.076923 0.307692 1.0 
    tag 2
    globillum on
    globillum 3
end instance

$include "cylinderal.mi"

instance "cylinderal-inst-1" "cylinderal"
    material "red"
    transform 
        1.538462 0.0 0.0 0.0 
        0.0 1.538462 0.0 0.0 
        0.0 0.0 1.538462 0.0 
        -0.384615 -0.923077 1.230769 1.0 
    tag 3
    globillum on
    globillum 3
end instance

 
instance "square-1"
    geometry "bw_square" (
        "name" "bw-square" )
    material "darkgray"
    transform 
        0.1 0 0 0 
        0 0.1 0 0 
        0 0 0.1 0 
        0 0 0.2 1 
    globillum 3
end instance
instance "square-1"
    geometry "bw_square" ()
    material "white"
    transform 
        -0.007548 0.050 0.086273 0.0 
        0.004358 0.086603 -0.049810 0.0 
        -0.099619 0.0 -0.008716 0.0 
        -0.013073 0.0 0.149429 1.0 
    tag 4
    globillum on
    globillum 3
end instance

instgroup "root"
    "camera-instance" "light_inst"    "coniral-inst-1" "spheral-inst-1" "cylinderal-inst-1" "square-1" 
end instgroup

render "root" "camera-instance" "opt"
 
