# mental ray scene file from "Writing mental ray shaders"
# http://www.writingshaders.com/scene_catalog.html

verbose on

link "shadow_default.so"
$include "shadow_default.mi"
link "lambert.so"
$include "lambert.mi"
link "point_light_shadow.so"
$include "point_light_shadow.mi"
link "texture_uv.so"
$include "texture_uv.mi"
link "transparent.so"
$include "transparent.mi"

options "opt"
    object space
    globillum off
    face both
    trace depth 2 2 2
    samples 2 2
    contrast .1 .1 .1
end options
 

light "white_light"
    "point_light_shadow" (
        "light_color" 1.1 1.1 1.1
    )
    origin 0 7 -9
end light

instance "light_instance" 
    "white_light" 
end instance

material "gray" opaque
    "lambert" (
        "diffuse" 1 1 1,
        "lights" ["light_instance"]
    )
end material

color texture "octatile" "octatile.tif"

shader "tile_shader"
    "texture_uv" (
       "tex" "octatile",
       "u_scale" 5,
       "v_scale" 2
    )

material "transparent_25"
    "transparent" (
        "color" = "tile_shader",
        "transparency" .25 .25 .25
    )
    shadow
        "shadow_default" ()
end material

material "transparent_50"
    "transparent" (
        "color" = "tile_shader",
        "transparency" .5 .5 .5
    )
    shadow
        "shadow_default" ()
end material

material "transparent_75"
    "transparent" (
        "color" = "tile_shader",
        "transparency" .75 .75 .75
    )
    shadow
        "shadow_default" ()
end material


color texture "diamond" "gray_diamond.map"

shader "diamondgrid"
    "texture_uv" (
       "tex" "diamond",
       "u_scale" 400,
       "v_scale" 400
    )

material "diamondtile" opaque
    "lambert" (
        "diffuse" = "diamondgrid",
        "lights" ["light_instance"]
    )
end material



camera "cam"
    output "rgba" "tif"
        "shadows_1.tif"
    focal 1.5
    aperture 1
    aspect 1.2
    resolution 300 250
end camera

instance "camera-instance" "cam"
    transform 
        1.0 0.0 0.0 0.0 
        0.0 0.920331 0.391141 0.0 
        0.0 -0.391141 0.920331 0.0 
        0.0 0.184066 -4.268035 1.0 
end instance

instance "cube-1"
    geometry "bw_cube" ("name" "cube")
    material "diamondtile"
    transform 
        0.010 0.0 0.0 0.0 
        0.0 100.0 0.0 0.0 
        0.0 0.0 0.010 0.0 
        0.0 55.0 0.0 1.0 
    tag 1
    globillum on
    globillum 3
end instance

instance "column-1"
    geometry "bw_cylinder" ("name" "bw-column")
    material "transparent_75"
    transform 
        1.0 0.0 0.0 0.0 
        0.0 1.0 0.0 0.0 
        0.0 0.0 1.0 0.0 
        0.650 -0.10 -0.30 1.0 
    tag 2
    globillum on
    globillum 3
end instance

instance "column-2"
    geometry "bw_cylinder" ("name" "bw-column")
    material "transparent_50"
    transform 
        1.0 0.0 0.0 0.0 
        0.0 1.0 0.0 0.0 
        0.0 0.0 1.0 0.0 
        -0.10 -0.10 0.70 1.0 
    tag 3
    globillum on
    globillum 3
end instance

instance "column-3"
    geometry "bw_cylinder" ("name" "bw-column")
    material "transparent_25"
    transform 
        1.0 0.0 0.0 0.0 
        0.0 1.0 0.0 0.0 
        0.0 0.0 1.0 0.0 
        -0.950 -0.10 1.70 1.0 
    tag 4
    globillum on
    globillum 3
end instance

instgroup "root"
    "camera-instance"
    "light_instance"    "cube-1" "column-1" "column-2" "column-3" 
end instgroup

render "root" "camera-instance" "opt"
 
