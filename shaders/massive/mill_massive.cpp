#include "shader.h"
#include <sstream>
#include <string>
#include <vector>
using namespace std;
extern "C" {
#include "miaux.h"
}

#define EPS 1.0e-8

struct mill_massive {
  // lambert
  miColor ambient;
  miColor diffuse;
  // phong
  miColor  specular;
  miScalar exponent;
  // reflect
  miColor refl_color;
  // ambient occlusion
  miUint ao_samples;
  miScalar ao_cutoff_distance;
  // agent IDs (can be bound in massive)
  miColor agentid1;
  miColor agentid2;
  miColor agentid3;
  // normal map
  miBoolean normal_map_enable;
  miColor normal_map;
  // light list (for light loop)
  miUint mode;
  int     i_light;
  int     n_light;
  miTag   light[1];
};

extern "C" DLLEXPORT
int
mill_massive_version(void) {
  return 1;
}

extern "C" DLLEXPORT
miBoolean
mill_massive_init(miState* state,
                  void* params,
                  miBoolean* instance_init_required) {
  // see named_framebuffer_put.cpp
  size_t count = 0;

  mi::shader::Access_fb framebuffers(state->camera->buffertag);
  framebuffers->get_buffercount(count);
  for (unsigned int i = 0; i < count; i++) {
    const char *name = NULL;
    if (framebuffers->get_buffername(i, name)) {
      size_t index;
      if (framebuffers->get_index(name, index)) {
        mi_info("  framebuffer '%s' has index %i", name, (int) index);
      }
      else  {
        mi_info("  framebuffer '%s' has no index", name);
      }
    }
  }

  return miTRUE;
}

extern "C" DLLEXPORT
miBoolean
mill_massive(miColor* result,
             miState* state,
             struct mill_massive* params) {
  char ambient_fb[] = "AMBIENT";
  char occlusion_fb[] = "OCCLUSION";
  char diffuse_fb[] = "DIFFUSE";
  char specular_fb[] = "SPECULAR";
  char reflect_fb[] = "REFLECT";
  // char depth_fb[] = "Z";
  char normals_fb[] = "NORMALS";
  char shadows_fb[] = "SHADOWS";
  char agentid1_fb[] = "AGENTID1";
  char agentid2_fb[] = "AGENTID2";
  char agentid3_fb[] = "AGENTID3";
#if 1
  char materials_fb[] = "MATERIALS";
  char instances_fb[] = "INSTANCES";
#endif
  miColor reflect = { 0.0f, 0.0f, 0.0f, 0.0f };
  miColor occlusion = { 0.0f, 0.0f, 0.0f, 0.0f };
  miColor refl_color = *mi_eval_color(&params->refl_color);
  // agentid[1|2|3]
  miColor* agentid1 = mi_eval_color(&params->agentid1);
  miColor* agentid2 = mi_eval_color(&params->agentid2);
  miColor* agentid3 = mi_eval_color(&params->agentid3);
  // see named_framebuffer_put.cpp
  mi::shader::Access_fb framebuffers(state->camera->buffertag);
  size_t buffer_index;
  // see ambient_occlusion_cutoff.c
  miUint ao_samples  = *mi_eval_integer(&params->ao_samples);
  miVector trace_direction;
  int object_hit = 0, sample_number = 0;
  double sample[2], hit_fraction = 0.0, ambient_exposure = 1.0;
  double falloff_start = 0.0, falloff_stop = 0.0;
  // see phong_framebuffer.cpp
  miColor light_color, diffuse_from_light, specular_from_light,
    diffuse_component, specular_component;
  int i, light_count, light_sample_count;
  miVector direction_toward_light;
  miScalar dot_nl;
  int mode;
  miTag* light;
  miColor* ambient = mi_eval_color(&params->ambient);
  miColor* diffuse = mi_eval_color(&params->diffuse);
  miColor* specular = mi_eval_color(&params->specular);
  miScalar exponent = *mi_eval_scalar(&params->exponent);
  // see shadowpass.cpp
  miColor without_shadow, with_shadow, shadow;
  // see normals_as_colors.c
  miVector normal;
  // see mlfNormalMap.cpp and mib_bump_basis.cpp
  miColor* normal_map;
  miVector u_basis = { 0.0f, 0.0f, 0.0f };
  miVector v_basis = { 0.0f, 0.0f, 0.0f };

  miaux_set_channels(result, 0.0); // init

  // state ////////////////////////////////////////////////////////////////////
  const miOptions *original_options = state->options;
  miOptions options_copy = *original_options;
  options_copy.shadow = miFALSE; // turn shadow off
  state->options = &options_copy;

  // labels ///////////////////////////////////////////////////////////////////

#if 1
  if (framebuffers->get_index(materials_fb, buffer_index)) {
    if (state->type == miRAY_EYE) {
      // miUint label = 0;
      // miBoolean got_label = miFALSE;
      // got_label = mi_query(miQ_INST_LABEL, state, state->instance,
      //         (void*) &label);
      // if (got_label) mi_info("  label %d", label);
      // mi_fb_put(state, buffer_index, &label);
      mi_fb_put(state, buffer_index, &state->material);
      // mi_fb_put(state, buffer_index, &state->instance);
      // mi_fb_put(state, buffer_index, &state->label);
    }
  }

  if (framebuffers->get_index(instances_fb, buffer_index)) {
    if (state->type == miRAY_EYE) {
      mi_fb_put(state, buffer_index, &state->instance);
    }
  }
#endif

  // depth ////////////////////////////////////////////////////////////////////

#if 0
  if (framebuffers->get_index(depth_fb, buffer_index)) {
    if (state->type == miRAY_EYE) {
      miVector p_to_cam;
      miScalar z_depth;
      mi_point_to_camera(state, &p_to_cam, &state->point);
      z_depth = -p_to_cam.z;
      mi_fb_put(state, buffer_index, &z_depth);
    }
  }
#endif

  // ambient occlusion ////////////////////////////////////////////////////////

  if (framebuffers->get_index(occlusion_fb, buffer_index)) {
    // do the ambient occlusion calculation only if framebuffer exists
    miScalar ao_cutoff_distance = *mi_eval_scalar(&params->ao_cutoff_distance);
    falloff_start = falloff_stop = ao_cutoff_distance;
    mi_ray_falloff(state, &falloff_start, &falloff_stop);
    while (mi_sample(sample, &sample_number, state, 2, &ao_samples)) {
      mi_reflection_dir_diffuse_x(&trace_direction, state, sample);
      if (mi_trace_probe(state, &trace_direction, &state->point)) {
        object_hit++;
      }
    }
    hit_fraction = ((double) object_hit / (double) ao_samples);
    ambient_exposure = 1.0 - hit_fraction;
    mi_ray_falloff(state, &falloff_start, &falloff_stop);
  }

  // framebuffers /////////////////////////////////////////////////////////////

  // ambient (means diffuse color without shading [diffuse, specular])
  if (framebuffers->get_index(ambient_fb, buffer_index)) {
    if (state->type == miRAY_EYE) {
      mi_fb_put(state, buffer_index, diffuse);
    }
  }
  // occlusion
  if (framebuffers->get_index(occlusion_fb, buffer_index)) {
    if (state->type == miRAY_EYE) {
      occlusion.r = occlusion.g = occlusion.b = hit_fraction;
      mi_fb_put(state, buffer_index, &occlusion);
    }
  }
  // normals
  if (*mi_eval_boolean(&params->normal_map_enable)) {
    // uv-coords (case TP_UV in basetexgen.cpp)
    struct uv { miVector u, v; };
    uv uv_coords = { { 0.0f, 0.0f, 0.0f }, { 0.0f, 0.0f, 0.0f } };
    miVector v0, v1, v2;
    miVector* qv[] = { &v0, &v1, &v2 };
    miVector t0, t1, t2;
    miVector* qt[] = { &t0, &t1, &t2 };
    int vert;
    if (!(!mi_query(miQ_PRI_NUM_VERTICES, state, 0, &vert) ||
          vert != 3 || !mi_query(miQ_PRI_VERTICES_POINTS, state, 0, qv) ||
          !mi_query(miQ_PRI_VERTICES_TEX, state, 0, qt, 0))) {
      // put state->normal in object space
      miVector norm_obj;
      mi_normal_to_object(state, &norm_obj, &state->normal);
      mi_vector_normalize(&norm_obj);
      miBoolean have_v = miTRUE;
      miVector refdir = { 0.0f, 0.0f, 0.0f };
      double fabs01x, fabs02x;
      if (fabs(t0.y - t1.y) < EPS) {
        have_v = miFALSE;
        if (t0.x > t1.x) mi_vector_sub(&uv_coords.u, &v0, &v1);
        else mi_vector_sub(&uv_coords.u, &v1, &v0);
        // calc a reference dir roughly in the positive v direction
        if (t0.y > t2.y) mi_vector_sub(&refdir, &v0, &v2);
        else mi_vector_sub(&refdir, &v2, &v0);
      } else if (fabs(t0.y - t2.y) < EPS) {
        have_v = miFALSE;
        if (t0.x > t2.x) mi_vector_sub(&uv_coords.u, &v0, &v2);
        else mi_vector_sub(&uv_coords.u, &v2, &v0);
        // calc a reference dir roughly in the positive v direction
        if (t0.y > t1.y) mi_vector_sub(&refdir, &v0, &v1);
        else mi_vector_sub(&refdir, &v1, &v0);
      } else if (fabs(t1.y - t2.y) < EPS) {
        have_v = miFALSE;
        if (t1.x > t2.x) mi_vector_sub(&uv_coords.u, &v1, &v2);
        else mi_vector_sub(&uv_coords.u, &v2, &v1);
        // calc a reference dir roughly in the positive v direction
        if (t1.y > t0.y) mi_vector_sub(&refdir, &v1, &v0);
        else mi_vector_sub(&refdir, &v0, &v1);
        // find a line of constant u on the triangle
      } else if ((fabs01x = fabs(t0.x - t1.x)) < EPS) {
        if (t0.y > t1.y) mi_vector_sub(&uv_coords.v, &v0, &v1);
        else mi_vector_sub(&uv_coords.v, &v1, &v0);
        // calc a reference dir roughly in the positive u direction
        if (t0.x > t2.x) mi_vector_sub(&refdir, &v0, &v2);
        else mi_vector_sub(&refdir, &v2, &v0);
      } else if ((fabs02x = fabs(t0.x - t2.x)) < EPS) {
        if (t0.y > t2.y) mi_vector_sub(&uv_coords.v, &v0, &v2);
        else mi_vector_sub(&uv_coords.v, &v2, &v0);
        // calc a reference dir roughly in the positive u direction
        if (t0.x > t1.x) mi_vector_sub(&refdir, &v0, &v1);
        else mi_vector_sub(&refdir, &v1, &v0);
      } else if (fabs(t1.x - t2.x) < EPS) {
        if (t1.y > t2.y) mi_vector_sub(&uv_coords.v, &v1, &v2);
        else mi_vector_sub(&uv_coords.v, &v2, &v1);
        // calc a reference dir roughly in the positive u direction
        if (t1.x > t0.x) mi_vector_sub(&refdir, &v1, &v0);
        else mi_vector_sub(&refdir, &v0, &v1);
      } else {
        miGeoVector tmp;
        double a, b;
        double t = ((t1.x - t0.x) * (t2.y - t0.y) -
                    (t1.y - t0.y) * (t2.x - t0.x));
        if (!(fabs(t) < EPS)) {
          t = 1.0 / t;
          tmp.x = t0.x;
          tmp.y = t0.y + 0.1;
          a = t *-((t2.x - t0.x) * (tmp.y - t0.y));
          b = t * ((t1.x - t0.x) * (tmp.y - t0.y));
          uv_coords.v.x = (miScalar) (a * (v1.x - v0.x) + b * (v2.x - v0.x));
          uv_coords.v.y = (miScalar) (a * (v1.y - v0.y) + b * (v2.y - v0.y));
          uv_coords.v.z = (miScalar)(a * (v1.z - v0.z) + b * (v2.z - v0.z));
          if (fabs02x > fabs01x) {
            if (t0.x > t2.x) mi_vector_sub(&refdir, &v0, &v2);
            else mi_vector_sub(&refdir, &v2, &v0);
          } else {
            if (t0.x > t1.x) mi_vector_sub(&refdir, &v0, &v1);
            else mi_vector_sub(&refdir, &v1, &v0);
          }
        }
      }
      if (have_v) {
        mi_vector_normalize(&uv_coords.v);
        mi_vector_prod(&uv_coords.u, &norm_obj, &uv_coords.v);
        // the uv_coords should be less than 90 degrees from refdir, if
        // it isn't, flip it
        if (mi_vector_dot(&uv_coords.u, &refdir) < 0) {
          mi_vector_neg(&uv_coords.u);
        }
      } else {
        mi_vector_normalize(&uv_coords.u);
        mi_vector_prod(&uv_coords.v, &uv_coords.u, &norm_obj);
        // the uv_coords should be less than 90 degrees from refdir, if
        // it isn't, flip it
        if (mi_vector_dot(&uv_coords.v, &refdir) < 0) {
          mi_vector_neg(&uv_coords.v);
        }
      }
      u_basis = uv_coords.u;
      v_basis = uv_coords.v;
    }
    // mlfNormalMap.cpp
    normal_map = mi_eval_color(&params->normal_map);
    miScalar u_shift, v_shift;
    u_shift = normal_map->r * 2.0f - 1.0f;
    v_shift = normal_map->g * 2.0f - 1.0f;
    mi_vector_normalize(&u_basis);
    mi_vector_normalize(&v_basis);
    mi_vector_mul(&u_basis, u_shift);
    mi_vector_mul(&v_basis, v_shift);
    miVector res_vec;
    mi_vector_add(&res_vec, &u_basis, &v_basis);
    // mi_vector_mul(&res_vec, scale);
    mi_vector_add(&state->normal, &state->normal, &res_vec);
    mi_vector_normalize(&state->normal);
  }
  if (framebuffers->get_index(normals_fb, buffer_index)) {
    if (state->type == miRAY_EYE) {
      mi_normal_to_world(state, &normal, &state->normal);
      mi_vector_normalize(&normal);
      mi_fb_put(state, buffer_index, &normal);
    }
  }
  // agentid1
  if (framebuffers->get_index(agentid1_fb, buffer_index)) {
    if (state->type == miRAY_EYE) {
      mi_fb_put(state, buffer_index, agentid1);
    }
  }
  // agentid2
  if (framebuffers->get_index(agentid2_fb, buffer_index)) {
    if (state->type == miRAY_EYE) {
      mi_fb_put(state, buffer_index, agentid2);
    }
  }
  // agentid3
  if (framebuffers->get_index(agentid3_fb, buffer_index)) {
    if (state->type == miRAY_EYE) {
      mi_fb_put(state, buffer_index, agentid3);
    }
  }
  // shadows
  miaux_set_channels(&without_shadow, 0.0);
  miaux_set_channels(&with_shadow, 0.0);
  miaux_set_channels(&shadow, 0.0);

  // lambert //////////////////////////////////////////////////////////////////

  // expect empty light list (see below)
  miaux_light_array(&light, &light_count, state,
                    &params->i_light, &params->n_light, params->light);
  // act like mode 2 of e.g. mib_illum_lambert (see baselambert.cpp)
  mode = *mi_eval_integer(&params->mode);
  switch (mode) {
  case 1:
    mi_inclusive_lightlist(&light_count, &light, state);
    break;
  case 2:
    mi_exclusive_lightlist(&light_count, &light, state);
    break;
  case 4:
    light_count = 0;
    light = NULL;
    break;
  default:
    break;
  }
  // create buffer names (based on light count)
  vector<string> diffuse_fbs;
  vector<string> specular_fbs;
  vector<string> shadows_fbs;
  miTag* light_shadows = light; // use different pointer for shadows !!!
  for (i = 0; i < light_count; i++, light_shadows++) {
    stringstream out;
    out << (i + 1);
    string diffuse_fbi = string(diffuse_fb) + out.str();
    string specular_fbi = string(specular_fb) + out.str();
    string shadows_fbi = string(shadows_fb) + out.str();
    diffuse_fbs.push_back(diffuse_fbi);
    specular_fbs.push_back(specular_fbi);
    shadows_fbs.push_back(shadows_fbi);
    // shadows
    miaux_set_channels(&without_shadow, 0.0);
    miaux_set_channels(&with_shadow, 0.0);
    miaux_set_channels(&shadow, 0.0);
    miaux_add_light_color(&without_shadow, state, *light_shadows, miFALSE);
    miaux_add_light_color(&with_shadow, state, *light_shadows, miTRUE);
    miaux_divide_colors(&shadow, &with_shadow, &without_shadow);
    if (framebuffers->get_index(shadows_fbs[i].c_str(), buffer_index)) {
      mi_fb_put(state, buffer_index, &shadow);
    }
  }
  // light loop
  miaux_set_channels(&without_shadow, 0.0);
  miaux_set_channels(&with_shadow, 0.0);
  miaux_set_channels(&shadow, 0.0);
  for (i = 0; i < light_count; i++, light++) {
    miaux_set_channels(&diffuse_component, 0.0);
    miaux_set_channels(&specular_component, 0.0);
    miaux_set_channels(&diffuse_from_light, 0.0);
    miaux_set_channels(&specular_from_light, 0.0);
    light_sample_count = 0;
    while (mi_sample_light(&light_color, &direction_toward_light, &dot_nl,
                           state, *light, &light_sample_count)) {
      miaux_add_diffuse_component(&diffuse_from_light, dot_nl,
                                  diffuse, &light_color);
      miaux_add_phong_specular_component(&specular_from_light, state, exponent,
                                         &direction_toward_light,
                                         specular, &light_color);
    }
    if (light_sample_count > 0) {
      miScalar scale_factor = 1.0 / light_sample_count;
      // diffuse_component
      miaux_add_scaled_color(&diffuse_component, &diffuse_from_light,
                             scale_factor);
      // specular_component
      miaux_add_scaled_color(&specular_component, &specular_from_light,
                             scale_factor);
      // add diffuse
      miaux_add_color(result, &diffuse_component);
      // add specular
      miaux_add_color(result, &specular_component);
    }
    // framebuffers
    if (framebuffers->get_index(diffuse_fbs[i].c_str(), buffer_index)) {
      if (state->type == miRAY_EYE) {
        mi_fb_put(state, buffer_index, &diffuse_from_light);
      }
    }
    if (framebuffers->get_index(specular_fbs[i].c_str(), buffer_index)) {
      if (state->type == miRAY_EYE) {
        mi_fb_put(state, buffer_index, &specular_from_light);
      }
    }
    // shadows
    miaux_add_light_color(&without_shadow, state, *light, miFALSE);
    miaux_add_light_color(&with_shadow, state, *light, miTRUE);
  }

  // reflections //////////////////////////////////////////////////////////////
  if (framebuffers->get_index(reflect_fb, buffer_index)) {
    if (refl_color.r > 0.0f && refl_color.g > 0.0f && refl_color.b > 0.0f) {
      miVector reflection_direction;
      mi_reflection_dir(&reflection_direction, state);
      if (!mi_trace_reflection(&reflect, state, &reflection_direction)) {
        mi_trace_environment(&reflect, state, &reflection_direction);
      }
      miColor mult;
      miaux_multiply_colors(&mult, &reflect, &refl_color);
      miaux_add_color(result, &mult);
    }
    mi_fb_put(state, buffer_index, &reflect);
  }

  // shadows
  miaux_divide_colors(&shadow, &with_shadow, &without_shadow);
  miaux_multiply_colors(result, result, &shadow);

  // state ////////////////////////////////////////////////////////////////////
  state->options = (miOptions*) original_options;

  // Zap suggests to multiply only the ambient term by ambient occlusion
  // see http://mentalraytips.blogspot.co.uk/2008/11/joy-of-little-ambience.html
  // but we DON'T follow him here ;)
  miaux_scale_color(result, ambient_exposure);

  // ambient (means: add ambient color to everything)
  miaux_add_color(result, ambient);

  // alpha channel ////////////////////////////////////////////////////////////
  result->a = 1.0;

  return miTRUE;
}
