/******************************************************************************
 * Copyright 1986-2011 by mental images GmbH, Fasanenstr. 81, D-10623 Berlin,
 * Germany. All rights reserved.
 ******************************************************************************
 * Created:	09.09.10
 * Module:	baseshader
 * Purpose:	base shaders for Phenomenon writers
 *
 * Exports:
 *      mib_map_get_scalar_version
 *      mib_map_get_scalar
 *      mib_map_get_integer_version
 *      mib_map_get_integer
 *      mib_map_get_vector_version
 *      mib_map_get_vector
 *      mib_map_get_color_version
 *      mib_map_get_color
 *      mib_map_get_transform_version
 *      mib_map_get_transform
 *      mib_map_get_scalar_array_version
 *      mib_map_get_scalar_array
 *      mib_map_get_integer_array_version
 *      mib_map_get_integer_array
 *
 * History:
 *
 * Description:
 *      Return the value of specific fields of the map object particle that has
 *      been intersected. There is a function for each field type (scalar,
 *      integer, vector and so on).
 *      A note for shader writers: at each call, the shaders access the map object,
 *      get a copy of its declaration, look for a field that has the given name to
 *      retrieve its id and only then use that id to access the data: doing all
 *      this at each shader call is for sure expensive. The field id could be
 *      cached between shader calls, in order not to perform all the mentioned
 *      steps every time.
 *****************************************************************************/


#include <stdio.h>

#include "shader.h"
#include "geoshader.h"


using namespace mi::shader;


/*
 * the parameter struct for all the "mib_map_get_*" shaders.
 */
struct mib_map_picker_paras {
    miTag	    field_name_tag;	/* tag of the string for the field name */
};


/*
 * returns in 'field_id' and 'field_is_global' the informations
 * about the field being queried, and runs all the needed checks.
 * Returns miTRUE in case it's successful, miFALSE otherwise
 */
template<typename Type>
static miBoolean get_field_id(
    Access_map	    &map,
    Map_field_type  field_type,
    miState	    *state,
    struct mib_map_picker_paras	*paras,
    Map_field_id    &field_id,
    bool	    &field_is_global)
{
    /* get a copy of the map declaration, that will later be queried   */
    /* to get the id of the field 'field_name'. Keep in mind that      */
    /* doing this at each shader call is quite expensive (getting a    */
    /* copy of the map declaration and querying the field id by name), */
    /* so some form of caching of this information between calls       */
    /* should be put in place.                                         */
    Map_status	    status;
    Map_declaration map_declaration(map, &status);

    if (!status.is_ok()) {
	/* something is wrong. */

	return(miFALSE);
    }

    /* evaluate the tag of the field name string. */
    miTag   field_name_tag = *mi_eval_tag(&paras->field_name_tag);

    /* retrieve the field name. */
    char    *field_name = (char *) mi_db_access(field_name_tag);

    /* get the field id from the declaration. */
    field_id = map_declaration->get_field_id(field_name, &status);

    mi_db_unpin(field_name_tag);

    if (!status.is_ok()) {
	/* the field is not there, or something is wrong. */

	return(miFALSE);
    }

    field_is_global = false;

    Map_field_type  f_type;
    miUint	    f_dimension;    /* unused. */
    bool	    f_is_global;

    /* retrieve the informations about the field, it's */
    /* needed to check the field type and whether the  */
    /* field is global or not.                         */
    status = map_declaration->get_field_info(
	field_id, f_type, f_dimension, f_is_global);

    if (!status.is_ok()) {
	/* something is wrong. */

	return(miFALSE);
    }

    if (field_type!=f_type) {
	/* the field type doesn't match. */

	return(miFALSE);
    }

    field_is_global = f_is_global;
    return(miTRUE);
}


/*
 * returns in 'out_value' the value of the map field. It's a template
 * function over the output value type, and it's called from all the
 * "mib_map_get_*" shaders that query non-array values:
 *
 * mib_map_get_scalar
 * mib_map_get_integer
 * mib_map_get_vector
 * mib_map_get_color
 *
 */
template<typename Type>
static miBoolean get_value(
    Type	    &out_value,
    Map_field_type  field_type,
    miState	    *state,
    struct mib_map_picker_paras	*paras)
{
    int	    particle_type;

    /* return here if it's not a map object. */
    if (!mi_query(miQ_PRI_PARTICLE_TYPE, state, 0, &particle_type))
	return(miFALSE);

    if (particle_type < 0)
	return(miFALSE);

    /* it's a map object for sure, because */
    /* mi_query returned miTRUE.           */
    miTag	instance_tag = state->instance;

    miInstance  *instance = (miInstance *) mi_db_access(instance_tag);
    miObject    *object = (miObject *) mi_db_access(instance->item);

    miTag   map_tag = object->geo.map;

    /* access the map by tag. */
    Access_map  map(map_tag);

    Map_field_id    field_id=0;
    bool	    field_is_global=false;

    if (!get_field_id<Type>(map, field_type, state, paras,
	field_id, field_is_global)) {
	/* something is wrong. */

	mi_db_unpin(instance->item);
	mi_db_unpin(instance_tag);
	return(miFALSE);
    }

    Map_status	status;

    if (field_is_global) {
	/* the field is global. */

	status = map->get(field_id, out_value);

	mi_db_unpin(instance->item);
	mi_db_unpin(instance_tag);

    if (!status.is_ok()) {
	    /* something is wrong. */

	    return(miFALSE);
	}

	return(miTRUE);
    }

    /* here we know that the field is not global - get the */
    /* particle index from the intersection and get the    */
    /* field value from the given particle.                */
    miUint    particle_index = 0;

    if (!mi_query(miQ_PRI_PARTICLE_INDEX, state, map_tag, &particle_index)) {
	mi_db_unpin(instance->item);
	mi_db_unpin(instance_tag);
	return(miFALSE);
    }

    /* use an iterator to retrieve the value of the field of */
    /* the particle whose index is 'particle_index'.         */
    Map_iterator_access	    it(map);
    status = it->set_to(particle_index);

    if (!status.is_ok()) {
	/* something is wrong. */

	mi_db_unpin(instance->item);
	mi_db_unpin(instance_tag);
	return(miFALSE);
    }

    /* get the field value. */
    status = it->get(field_id, out_value);

    mi_db_unpin(instance->item);
    mi_db_unpin(instance_tag);

    if (!status.is_ok()) {
	/* something is wrong. */

	return(miFALSE);
    }

    return(miTRUE);
}


/*
 * returns in 'out_value' the value of the map field. It's a template
 * function over the output value type, and it's called from all the
 * "mib_map_get_*" shaders that query array values:
 *
 * mib_map_get_transform (array of 16 scalars)
 * mib_map_get_scalar_array
 * mib_map_get_integer_array
 *
 */
template<typename Type>
static miBoolean get_array_value(
    Type	    *out_value,
    Map_field_type  field_type,
    miState	    *state,
    struct mib_map_picker_paras	*paras)
{
    int	    particle_type;

    /* return here if it's not a map object. */
    if (!mi_query(miQ_PRI_PARTICLE_TYPE, state, 0, &particle_type))
	return(miFALSE);

    if (particle_type < 0)
	return(miFALSE);

    /* it's a map object for sure, because */
    /* mi_query returned miTRUE.           */
    miTag	instance_tag = state->instance;

    miInstance  *instance = (miInstance *) mi_db_access(instance_tag);
    miObject    *object = (miObject *) mi_db_access(instance->item);

    miTag   map_tag = object->geo.map;

    /* access the map by tag. */
    Access_map  map(map_tag);

    Map_field_id    field_id;
    bool	    field_is_global;

    if (!get_field_id<Type>(map, field_type, state, paras,
	field_id, field_is_global)) {
	/* something is wrong. */

	mi_db_unpin(instance->item);
	mi_db_unpin(instance_tag);
	return(miFALSE);
    }

    Map_status	status;

    if (field_is_global) {
	/* the field is global. */

	status = map->get(field_id, out_value);

	mi_db_unpin(instance->item);
	mi_db_unpin(instance_tag);

	if (!status.is_ok()) {
	    /* something is wrong. */

	    return(miFALSE);
	}

	return(miTRUE);
    }

    /* here we know that the field is not global - get the */
    /* particle index from the intersection and get the    */
    /* field value from the given particle.                */
    miUint    particle_index = 0;

    if (!mi_query(miQ_PRI_PARTICLE_INDEX, state, map_tag, &particle_index)) {
	mi_db_unpin(instance->item);
	mi_db_unpin(instance_tag);
	return(miFALSE);
    }

    /* use an iterator to retrieve the value of the field of */
    /* the particle whose index is 'particle_index'.         */
    Map_iterator_access	    it(map);
    status = it->set_to(particle_index);

    if (!status.is_ok()) {
	/* something is wrong. */

	mi_db_unpin(instance->item);
	mi_db_unpin(instance_tag);
	return(miFALSE);
    }

    /* get the field value. */
    status = it->get(field_id, out_value);

    mi_db_unpin(instance->item);
    mi_db_unpin(instance_tag);

    if (!status.is_ok()) {
	/* something is wrong. */

	return(miFALSE);
    }

    return(miTRUE);
}


/*
 * the two functions for the "mib_map_get_scalar" shader.
 */
extern "C" DLLEXPORT int mib_map_get_scalar_version(void) {return(1);}


extern "C" DLLEXPORT miBoolean mib_map_get_scalar(
    void	*result,
    miState	*state,
    struct mib_map_picker_paras	*paras)
{
    miScalar	*scalar_result = (miScalar *) result;
    *scalar_result = 0.0f;

    return get_value(*scalar_result, Map_field_type::Scalar, state, paras);
}
    

/*
 * the two functions for the "mib_map_get_integer" shader.
 */
extern "C" DLLEXPORT int mib_map_get_integer_version(void) {return(1);}


extern "C" DLLEXPORT miBoolean mib_map_get_integer(
    void	*result,
    miState	*state,
    struct mib_map_picker_paras	*paras)
{
    int	    *int_result = (int *) result;
    *int_result = 0;

    return get_value(*int_result, Map_field_type::Integer, state, paras);
}


/*
 * the two functions for the "mib_map_get_vector" shader.
 */
extern "C" DLLEXPORT int mib_map_get_vector_version(void) {return(1);}


extern "C" DLLEXPORT miBoolean mib_map_get_vector(
    void	*result,
    miState	*state,
    struct mib_map_picker_paras	*paras)
{
    miVector	*vector_result = (miVector *) result;
    vector_result->x = vector_result->y = vector_result->z = 0.0f;

    return get_value(*vector_result, Map_field_type::Vector, state, paras);
}
    

/*
 * the two functions for the "mib_map_get_color" shader.
 */
extern "C" DLLEXPORT int mib_map_get_color_version(void) {return(1);}


extern "C" DLLEXPORT miBoolean mib_map_get_color(
    void	*result,
    miState	*state,
    struct mib_map_picker_paras	*paras)
{
    miColor	*color_result = (miColor *) result;
    color_result->r = color_result->g = color_result->b = 0.0f;
    color_result->a = 1.0f;

    return get_value(*color_result, Map_field_type::Color, state, paras);
}


/*
 * the two functions for the "mib_map_get_transform" shader.
 * The caller must take care of passing a pointer that is big
 * enough to hold the contents of the transform array.
 * The 'result' void pointer is cast to an miScalar pointer because
 * a transform (miMatrix) is actually an array of miScalar.
 */
extern "C" DLLEXPORT int mib_map_get_transform_version(void) {return(1);}


extern "C" DLLEXPORT miBoolean mib_map_get_transform(
    void	*result,
    miState	*state,
    struct mib_map_picker_paras	*paras)
{
    miScalar	*scalar_array = (miScalar *) result;

    return get_array_value(scalar_array,
	Map_field_type::Transform, state, paras);
}
    

/*
 * the two functions for the "mib_map_get_scalar_array" shader.
 * The caller must take care of passing a pointer that is big
 * enough to hold the contents of the array.
 */
extern "C" DLLEXPORT int mib_map_get_scalar_array_version(void) {return(1);}


extern "C" DLLEXPORT miBoolean mib_map_get_scalar_array(
    void	*result,
    miState	*state,
    struct mib_map_picker_paras	*paras)
{
    miScalar	*scalar_array = (miScalar *) result;

    return get_array_value(scalar_array,
	Map_field_type::Scalar_array, state, paras);
}


/*
 * the two functions for the "mib_map_get_integer_array" shader.
 * The caller must take care of passing a pointer that is big
 * enough to hold the contents of the array.
 */
extern "C" DLLEXPORT int mib_map_get_integer_array_version(void) {return(1);}


extern "C" DLLEXPORT miBoolean mib_map_get_integer_array(
    void	*result,
    miState	    *state,
    struct mib_map_picker_paras	*paras)
{
    int	    *integer_array = (int *) result;

    return get_array_value(integer_array,
	Map_field_type::Integer_array, state, paras);
}

