/******************************************************************************
 * Copyright 1986-2011 by mental images GmbH, Fasanenstr. 81, D-10623 Berlin,
 * Germany. All rights reserved.
 ******************************************************************************
 * Created:	30.10.97
 * Module:	baseshader
 * Purpose:	base shaders for Phenomenon writers
 *
 * Exports:
 *	mib_texture_lookup
 *	mib_texture_lookup2
 *	mib_texture_lookup_version
 *	mib_texture_filter_lookup
 *	mib_texture_filter_lookup_version
 *
 * History:
 *	19.11.97: filtered texture lookups
 *
 * Description:
 * texture image lookups.
 *****************************************************************************/

#include <string.h>
#include <math.h>
#include <shader.h>


/*------------------------------------------ mib_texture_lookup -------------*/

struct mib_texture_lookup {
	miTag		tex;
	miVector	coord;
};

extern "C" DLLEXPORT int mib_texture_lookup_version(void) {return(1);}

extern "C" DLLEXPORT miBoolean mib_texture_lookup(
	miColor		*result,
	miState		*state,
	struct mib_texture_lookup *paras)
{
	miTag		tex   = *mi_eval_tag(&paras->tex);
	miVector	*coord = mi_eval_vector(&paras->coord);

	if (tex && coord->x >= 0 && coord->x < 1
		&& coord->y >= 0 && coord->y < 1
		&& mi_lookup_color_texture(result, state, tex, coord))

		return(miTRUE);

	result->r = result->g = result->b = result->a = 0;
	return(miFALSE);
}

/*------------------------------------------ mib_texture_lookup2 -------------*/

struct mib_texture_lookup2 {
    miTag	tex;			/* The texture to lookup */
    miScalar	factor;			/* Repeation factor */
};

/*-----------------------------------------------------------------------------
 * This texture lookup takes the first texture coordinates on the object,
 * apply a multiplication 'factor' on those coordinates
 *---------------------------------------------------------------------------*/
extern "C" DLLEXPORT int mib_texture_lookup2_version(void) {return(1);}

extern "C" DLLEXPORT miBoolean mib_texture_lookup2(
    miColor		*result,
    miState		*state,
    struct mib_texture_lookup2 *paras)
{
    miTag	tex   = *mi_eval_tag(&paras->tex);
    miVector	coord;
    miScalar	factor = *mi_eval_scalar(&paras->factor);

    if (tex )
    {
	coord.x = state->tex_list[0].x * factor;
	coord.y = state->tex_list[0].y * factor;
	mi_lookup_color_texture(result, state, tex, &coord);
	return(miTRUE);
    }


    result->r = result->g = result->b = result->a = 0;
    return(miFALSE);
}

/*------------------------------------------ mib_texture_filter_lookup ------*/

struct mib_texture_filter_lookup {
	miTag		tex;
	miVector	coord;
	miScalar	eccmax;
	miScalar	maxminor;
	miScalar	disc_r;
	miBoolean	bilinear;
	miUint		space;
	miTag		remap;
};

extern "C" DLLEXPORT int mib_texture_filter_lookup_version(void) {return(3);}

#define DISC_R		0.3	/* projection matrix circle radius */
#define CIRCLE_R	0.8	/* projected screen-space circle */

extern "C" DLLEXPORT miBoolean mib_texture_filter_lookup(
	miColor		*result,
	miState		*state,
	struct mib_texture_filter_lookup *paras)
{
	miTag		tex = *mi_eval_tag(&paras->tex);
	miVector	*coord;
	miUint		space;
	miTag		remap;
	miVector	p[3], t[3];
	miMatrix	ST;
	miTexfilter	ell_opt;
	miScalar	disc_r;

	if (!tex) {
		result->r = result->g = result->b = result->a = 0;
		return(miFALSE);
	}
	coord  = mi_eval_vector(&paras->coord);
	space  = *mi_eval_integer(&paras->space);
	disc_r = *mi_eval_scalar(&paras->disc_r);
	if (disc_r <= 0)
		disc_r = DISC_R;
	if (state->reflection_level == 0 &&
	    mi_texture_filter_project(p, t, state, disc_r, space) &&
	    (remap = *mi_eval_tag(&paras->remap))) {
		mi_call_shader_x((miColor*)&t[0], miSHADER_TEXTURE,
						state, remap, &t[0]);
		mi_call_shader_x((miColor*)&t[1], miSHADER_TEXTURE,
						state, remap, &t[1]);
		mi_call_shader_x((miColor*)&t[2], miSHADER_TEXTURE,
						state, remap, &t[2]);
		if (mi_texture_filter_transform(ST, p, t)) {
			ell_opt.eccmax	  = *mi_eval_scalar(&paras->eccmax);
			ell_opt.max_minor = *mi_eval_scalar(&paras->maxminor);
			ell_opt.bilinear  = *mi_eval_boolean(&paras->bilinear);
			ell_opt.circle_radius = CIRCLE_R;
			/*
			 * when no bump-mapping is used, coord and ST[..]
			 * are identical. for bump mapping, the projection
			 * matrix is calculated for the current raster
			 * position, the ellipse is translated to the
			 * bump position
			 */
			ST[2*4+0] = coord->x;
			ST[2*4+1] = coord->y;
			if (mi_lookup_filter_color_texture(result, state,
							tex, &ell_opt, ST))
				return(miTRUE);
		}
	}
	/* fallback to standard pyramid or nonfiltered texture lookup */
	return(mi_lookup_color_texture(result, state, tex, coord));
}

/*------------------------------------------ mib_texture_filter_lookup_2 ----*/

class Base_remap : public mi::shader::Mip_remap
{
  public:
    // constructor
    Base_remap();

    void coord_transform(
	miVector2d &	tex_coord) const;

    void coord_remap(
	const miImg_image *image,
	int		image_coord[2]) const;

  public:
    miVector *		m_crop_min;
    miVector *		m_crop_max;
    miVector *		m_repeat;
    miScalar *		m_transform;

    bool		m_enable_torus[2];
    bool		m_enable_alt[2];
};

// constructor
Base_remap::Base_remap()
  : m_crop_min(0), m_crop_max(0), m_repeat(0), m_transform(0)
{
    m_enable_torus[0] = m_enable_torus[1] = false;
    m_enable_alt[0]   = m_enable_alt[1]   = false;
}

// use Texture to transform the lookup coordinates, used by lookup/store.
void Base_remap::coord_transform(
    miVector2d &	t) const			// coordinate to trafo, in/out
{
    if (m_transform) {
	miScalar x = t.u * m_transform[0] + t.v * m_transform[4] + m_transform[8];
	miScalar y = t.u * m_transform[1] + t.v * m_transform[5] + m_transform[9];
	miScalar w = t.u * m_transform[2] + t.v * m_transform[6] + m_transform[10];

	if (w != 0 && w != 1) {
	    x /= w;
	    y /= w;
	}

	t.u = x; t.v = y;
    }

    if (m_repeat) {
	// Repeat/Scale
	t.u *= m_repeat->x;
	t.v *= m_repeat->y;
    }
}

// output coordinates (scaled to miplevel resolution) are remapped depending on Texture.
void Base_remap::coord_remap(
    const miImg_image *	image,
    int			image_coord[2]) const		// coordinate for texel lookup, in/out
{
    int img_res[2];

    img_res[0] = mi_img_get_width(image);
    img_res[1] = mi_img_get_height(image);

    for (miUint d=0; d < 2; d++) {
	// Wrap or Clamp
	if (m_enable_torus[d]) {
	    if (image_coord[d] < 0)
		// Optionally alternate
		image_coord[d] = (m_enable_alt[d] && ((image_coord[d]/img_res[d]) & 1)) ?
			     - (image_coord[d] % img_res[d]) :
			     image_coord[d] % img_res[d] + img_res[d]-1;
	    else
		// Optionally alternate
		image_coord[d] = (m_enable_alt[d] && ((image_coord[d]/img_res[d]) & 1)) ?
			     img_res[d]-1 - image_coord[d] % img_res[d] :
			     image_coord[d] % img_res[d];
	}
	else
	    if (image_coord[d] < 0)
		image_coord[d] = 0;
	    else if (image_coord[d] > img_res[d]-1)
		image_coord[d] = img_res[d]-1;
    }

    // Crop
    if (m_crop_min) {
	image_coord[0] = (int)((miScalar)image_coord[0] * (m_crop_max->x - m_crop_min->x)
			+ m_crop_min->x * (miScalar)img_res[0]);
	image_coord[1] = (int)((miScalar)image_coord[1] * (m_crop_max->y - m_crop_min->y)
			+ m_crop_min->y * (miScalar)img_res[1]);
    }
}

struct mib_texture_filter_lookup_2 {
	miTag		texture;
	miVector	coord;

	miMatrix	transform;
	miVector	repeat;
	miBoolean	alt_x;
	miBoolean	alt_y;
	miBoolean	torus_x;
	miBoolean	torus_y;
	miVector	min;
	miVector	max;
};

extern "C" DLLEXPORT int mib_texture_filter_lookup_2_version(void) {return(1);}

extern "C" DLLEXPORT miBoolean mib_texture_filter_lookup_2(
	miColor		*result,
	miState		*state,
	struct mib_texture_filter_lookup_2 *paras)
{
	miTag tex = *mi_eval_tag(&paras->texture);
	if (!tex) {
	    result->r = result->g = result->b = result->a = 0;
	    return(miFALSE);
	}

	miVector * coord = mi_eval_vector(&paras->coord);

	Base_remap remap;
	remap.m_enable_alt[0] = bool(*mi_eval_boolean(&paras->alt_x));
	remap.m_enable_alt[1] = bool(*mi_eval_boolean(&paras->alt_y));
	remap.m_enable_torus[0] = bool(*mi_eval_boolean(&paras->torus_x));
	remap.m_enable_torus[1] = bool(*mi_eval_boolean(&paras->torus_y));

	if (paras->repeat.x > 0.f || paras->repeat.y > 0.f) {
	    remap.m_repeat = mi_eval_vector(&paras->repeat);

	    if (!remap.m_enable_torus[0] || !remap.m_enable_torus[1] ||
		!remap.m_enable_alt[0] || !remap.m_enable_alt[1]) {
		remap.m_enable_torus[0] =
		remap.m_enable_torus[1] = true;
	    }
	}

	miVector * min = mi_eval_vector(&paras->min);
	miVector * max = mi_eval_vector(&paras->max);
	miVector v;
	mi_vector_sub(&v, max, min);
	if (mi_vector_dot(&v, &v) > 0.f) {
	    remap.m_crop_min = min;
	    remap.m_crop_max = max;
	}

	miScalar * transform = mi_eval_transform(&paras->transform);
	if (!mi_matrix_isnull(transform))
	    remap.m_transform = transform;

	mi::shader::Interface * iface = mi_get_shader_interface();
	return iface->lookup_filter_color_texture(result, state, tex, remap, coord);
}
