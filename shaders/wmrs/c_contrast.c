#include "contour_structs.h"

struct c_contrast {
    miScalar dot_threshold;
};

DLLEXPORT
int c_contrast_version(void) { return 1; }

DLLEXPORT
miBoolean c_contrast (
    contour_info      *info1, 
    contour_info      *info2, 
    int               level, 
    miState           *state, 
    struct c_contrast *params  )
{
    if (info1 == NULL || 
        info2 == NULL ||
        info1->instance != info2->instance ||
        (mi_vector_dot(&info1->normal, &info2->normal) <
         *mi_eval_scalar(&params->dot_threshold)))
        return miTRUE;
    else
        return miFALSE;
}
