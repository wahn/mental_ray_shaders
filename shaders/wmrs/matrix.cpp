#include <iostream>
#include <math.h>
#include "matrix.h"

using namespace std;

matrix::matrix()
{
    i();
}
    
// Identity matrix

matrix& matrix::i() 
{
    for (int i = 0; i < 16; ++i)
	m[i] = i % 5 == 0 ? 1.0 : 0.0;
    return *this;
}

// Matrix multiply

const matrix operator*(matrix &lhs, matrix &rhs)
{
    matrix result;
    for (int n = 0; n < 4; ++n) {
	for (int p = 0; p < 4; ++p) {
	    float v = 0;
	    for (int m = 0; m < 4; ++m) {
		v = v + (lhs[n * 4 + m] * rhs[m * 4 + p]);
		result[n * 4 + p] = v;
	    }
	}
    }
    return result;
}

// Scaling

matrix& matrix::sx(float scale)
{
    matrix m;
    m[0] = scale;
    *this = *this * m;
    return *this;
}

matrix& matrix::sy(float scale)
{
    matrix m;
    m[5] = scale;
    *this = *this * m;
    return *this;
}

matrix& matrix::sz(float scale)
{
    matrix m;
    m[10] = scale;
    *this = *this * m;
    return *this;
}

matrix& matrix::s(float scale)
{
    matrix m;
    m[0] = m[5] = m[10] = scale;
    *this = *this * m;
    return *this;
}

matrix& matrix::s(float x_scale, float y_scale, float z_scale)
{
    matrix m;
    m[0] = x_scale;
    m[5] = y_scale;
    m[10] = z_scale;
    *this = *this * m;
    return *this;
}

// Rotation

matrix& matrix::rx(float degrees)
{
    matrix m;
    float r = radians(degrees);
    m[5] = cos(r);
    m[6] = sin(r);
    m[9] = -sin(r);
    m[10] = cos(r);
    *this = *this * m;
    return *this;
}

matrix& matrix::ry(float degrees)
{
    matrix m;
    float r = radians(degrees);
    m[0] = cos(r);
    m[2] = -sin(r);
    m[8] = sin(r);
    m[10] = cos(r);

    *this = *this * m;
    return *this;
}

matrix& matrix::rz(float degrees)
{
    matrix m;
    float r = radians(degrees);
    m[0] = cos(r);
    m[1] = sin(r);
    m[4] = -sin(r);
    m[5] = cos(r);
    *this = *this * m;
    return *this;
}

matrix& matrix::r(float degrees, float x, float y, float z)
{
    matrix m;
    float r = radians(degrees);
    float k = 1.0 - cos(r);
    float cosr = cos(r);
    float sinr = sin(r);
    /*
    m[0] = x * x * k + cosr;
    m[1] = x * y * k - z * sinr;
    m[2] = x * z * k + y * sinr;
    m[4] = x * y * k + z * sinr;
    m[5] = y * y * k + cosr;
    m[6] = y * z * k - x * sinr;
    m[8] = x * z * k - y * sinr;
    m[9] = y * z * k + x * sinr;
    m[10] = z * z * k + cosr;
    */
    m[0] = x * x * k + cosr;
    m[1] = x * y * k + z * sinr;
    m[2] = x * z * k - y * sinr;

    m[4] = x * y * k - z * sinr;
    m[5] = y * y * k + cosr;
    m[6] = y * z * k + x * sinr;

    m[8] = x * z * k + y * sinr;
    m[9] = y * z * k - x * sinr;
    m[10] = z * z * k + cosr;

    *this = *this * m;
    return *this;
}    

// Translation

matrix& matrix::tx(float d)
{
    matrix m;
    m[12] = d;
    *this = *this * m;
    return *this;
}

matrix& matrix::ty(float d)
{
    matrix m;
    m[13] = d;
    *this = *this * m;
    return *this;
}

matrix& matrix::tz(float d)
{
    matrix m;
    m[14] = d;
    *this = *this * m;
    return *this;
}

matrix& matrix::t(float dx, float dy, float dz)
{
    matrix m;
    m[12] = dx;
    m[13] = dy;
    m[14] = dz;
    *this = *this * m;
    return *this;
}

// 3D point transformation

matrix& matrix::mult(float& x, float& y, float& z)
{
    float v[] = {x, y, z, 1};
    float result[] = {0, 0, 0, 0};
    for (int i = 0; i < 4; ++i) {
	for (int j = 0; j < 4; ++j) {
	    result[i] += m[j * 4 + i] * v[j];
	}
    }
    x = result[0];
    y = result[1];
    z = result[2];
    return *this;
}

// Stream output

ostream& operator<< (ostream& os, const matrix& m) {
    for (int i = 0; i < 16; ++i) {
	os << m.index(i) << " ";
	if (i % 4 == 3 && i < 15)
	    os << endl;
    }
    return os;
}
