void miaux_define_hair_object(
    miTag name_tag, miaux_bbox_function bbox_function, void *params,
    miTag *geoshader_result, miApi_object_callback callback)
{
    miTag tag;
    miObject *obj;
    char *name = miaux_tag_to_string(name_tag, "::hair");
    obj = mi_api_object_begin(mi_mem_strdup(name));
    obj->visible = miTRUE;
    obj->shadow = obj->reflection = obj->refraction = 3;
    bbox_function(obj, params);
    if (geoshader_result != NULL && callback != NULL) {
	mi_api_object_callback(callback, params);
	tag = mi_api_object_end();
	mi_geoshader_add_result(geoshader_result, tag);
	obj = (miObject *)mi_scene_edit(tag);
	obj->geo.placeholder_list.type = miOBJECT_HAIR;
	mi_scene_edit_end(tag);
    }
}

char* miaux_tag_to_string(miTag tag, char *default_value)
{
    char *result = default_value;
    if (tag != 0) {
	result = (char*)mi_db_access(tag);
	mi_db_unpin(tag);
    }
    return result;
}

result->r += miaux_random_range(-red_variance, red_variance);
    result->r = miaux_clamp(result->r, 0.0, 1.0);
    result->g += miaux_random_range(-green_variance, green_variance);
    result->g = miaux_clamp(result->g, 0.0, 1.0);
    result->b += miaux_random_range(-blue_variance, blue_variance);
    result->b = miaux_clamp(result->b, 0.0, 1.0);
}

double miaux_fit(
    double v, double oldmin, double oldmax, double newmin, double newmax)    
{
    return newmin + ((v - oldmin) / (oldmax - oldmin)) * (newmax - newmin);
}