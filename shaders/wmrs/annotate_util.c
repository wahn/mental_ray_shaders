char* miaux_tag_to_string(miTag tag, char *default_value)
{
    char *result = default_value;
    if (tag != 0) {
	result = (char*)mi_db_access(tag);
	mi_db_unpin(tag);
    }
    return result;
}

void miaux_load_fontimage(fontimage *fimage, char* filename)
{ 
    int printable_ascii_size = 126 - 32 + 1;
    int i, image_data_size;
    char identifier[33];

    FILE* fp = fopen(filename, "r");
    if (fp == NULL)
	mi_fatal("Could not open font image file: %s", filename);
    fscanf(fp, "%s ", identifier);
    if (strcmp(identifier, "FONTIMAGE") != 0)
	mi_fatal("File '%s' does not look like a fontimage file", filename);

    fscanf(fp, "%d %d ", &fimage->width, &fimage->height);
    image_data_size = fimage->width * fimage->height;
    fimage->x_offsets = 
	(int*)mi_mem_allocate(sizeof(int) * printable_ascii_size);
    for (i = 0; i <= printable_ascii_size; i++)
	fscanf(fp, "%d ", &fimage->x_offsets[i]);
    fimage->image = (unsigned char*)mi_mem_allocate(image_data_size);
    fread(fimage->image, image_data_size, 1, fp);

    fclose(fp);
}

void miaux_text_image(
    float **text_image, int *width, int *height, 
    fontimage *fimage, char* text)
{
    char *c;
    int i, total_width, xpos, first_printable = 32, image_size, 
	index, start, end, font_index, fx, fy, tx, ty, text_index;

    for (i = 0, total_width = 0, c = text; i < strlen(text); i++, c++) {
	index = *c - first_printable;
	start = fimage->x_offsets[index];
	end = fimage->x_offsets[index + 1];
	total_width += end - start + 1;
    }
    *width = total_width;
    *height = fimage->height;
    image_size = *width * *height;
    (*text_image) = (float*)mi_mem_allocate(image_size * sizeof(float));

    for (i = 0, c = text, xpos = 0; i < strlen(text); i++, c++) {
	int index = *c - first_printable;
	start = fimage->x_offsets[index];
	end = fimage->x_offsets[index + 1];
	for (fy = fimage->height - 1, ty = 0; fy >= 0; fy--, ty++) {
	    for (fx = start, tx = xpos; fx < end-1; fx++, tx++) {
		text_index = ty * *width + tx;
		font_index = fy * fimage->width + fx;
		(*text_image)[text_index] = 
		    (float)fimage->image[font_index] / 255.0;
	    }
	}
	xpos += end - start + 1;
    }
}

void miaux_alpha_blend(miColor *x, miColor *y, miScalar alpha)
{
    x->r = miaux_blend(y->r, x->r, alpha);
    x->g = miaux_blend(y->g, x->g, alpha);
    x->b = miaux_blend(y->b, x->b, alpha);
}

double miaux_blend(miScalar a, miScalar b, miScalar factor)
{
    return a * factor + b * (1.0 - factor);
}

void miaux_release_fontimage(fontimage *fimage)
{
    mi_mem_release(fimage->x_offsets);
    mi_mem_release(fimage->image);
}