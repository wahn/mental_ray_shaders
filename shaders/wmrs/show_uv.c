#include "shader.h"

struct show_uv {
    miBoolean u;
    miBoolean v;
};

DLLEXPORT
int show_uv_version(void) {return 1;}

DLLEXPORT
miBoolean show_uv (
    miColor *result, miState *state, struct show_uv *params )
{
    result->r = result->g = result->b = 0;
    if (*mi_eval_boolean(&params->u))
        result->r = state->tex_list[0].x;
    if (*mi_eval_boolean(&params->v))
        result->g = state->tex_list[0].y;
    return miTRUE;
}
