char* miaux_tag_to_string(miTag tag, char *default_value)
{
    char *result = default_value;
    if (tag != 0) {
	result = (char*)mi_db_access(tag);
	mi_db_unpin(tag);
    }
    return result;
}

miTag miaux_object_from_file(
    const char* name, const char* filename, 
    miVector bbox_min, miVector bbox_max)
{
    miObject *obj = mi_api_object_begin(mi_mem_strdup(name));
    obj->visible = miTRUE;
    obj->shadow = 3;
    obj->bbox_min = bbox_min;
    obj->bbox_max = bbox_max;
    mi_api_object_file(mi_mem_strdup(filename));
    return mi_api_object_end();
}