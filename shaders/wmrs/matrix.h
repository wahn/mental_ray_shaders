#ifndef __MATRIX_H__
#define __MATRIX_H__

using namespace std;

class matrix {

public:
    matrix();
    float& operator[](int i) 
	{ return m[i]; }
    float index(int i) const 
	{ return m[i]; }
    // Identity matrix
    matrix& i();

    // Scaling
    matrix& sx(float scale);
    matrix& sy(float scale);
    matrix& sz(float scale);
    matrix& s(float scale);
    matrix& s(float x_scale, float y_scale, float z_scale=1.0);

    // Rotation
    static float radians(float degrees) 
	{ return degrees * 0.017453293; }
    matrix& rx(float degrees);
    matrix& ry(float degrees);
    matrix& rz(float degrees);
    matrix& r(float degrees, float vx, float vy, float vz);

    // Translation
    matrix& tx(float d);
    matrix& ty(float d);
    matrix& tz(float d);
    matrix& t(float dx, float dy, float dz);

    // 3D point transformation
    matrix& mult(float& x, float& y, float& z);

private:
    float m[16];
};

#endif
