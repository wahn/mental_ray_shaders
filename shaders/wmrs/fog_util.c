void miaux_blend_colors(miColor *result, 
			miColor *color1, miColor *color2, miScalar factor)
{
    result->r = miaux_blend(color1->r, color2->r, factor);
    result->g = miaux_blend(color1->g, color2->g, factor);
    result->b = miaux_blend(color1->b, color2->b, factor);
}

double miaux_blend(miScalar a, miScalar b, miScalar factor)
{
    return a * factor + b * (1.0 - factor);
}