#include "shader.h"
#include "miaux.h"

struct glossy_refraction_sample_varying {
    miScalar index_of_refraction;
    miScalar shiny;
    int i_samples;
    int n_samples;
    int samples[1];
};

DLLEXPORT
int glossy_refraction_sample_varying_version(void) { return 1; }

DLLEXPORT
miBoolean glossy_refraction_sample_varying (
    miColor *result, miState *state,
    struct glossy_refraction_sample_varying *params )
{
    miScalar ior   = *mi_eval_scalar(&params->index_of_refraction);
    miScalar shiny = *mi_eval_scalar(&params->shiny);
    int i_samples  = *mi_eval_integer(&params->i_samples);
    int n_samples  = *mi_eval_integer(&params->n_samples);
    int *samples   = mi_eval_integer(params->samples) + i_samples;
    miVector refraction_dir, reflect_dir;
    miColor refract_color, reflect_color;
    int sample_number = 0;
    double sample[2];
    miUint sample_count;

    sample_count = samples[state->refraction_level >= n_samples ?
                           n_samples - 1 : 
                           state->refraction_level];

    miaux_set_state_refraction_indices(state, ior);

    if (sample_count == 1) {
        if (mi_transmission_dir_glossy(&refraction_dir, state,
                                       state->ior_in, state->ior, shiny))
            mi_trace_refraction(result, state, &refraction_dir);
        else {
            mi_reflection_dir(&reflect_dir, state);
            if (!mi_trace_reflection(result, state, &reflect_dir))
                mi_trace_environment(result, state, &reflect_dir);
        }
    } else {
        result->r = result->g = result->b = 0.0;
        while (mi_sample(sample, &sample_number, state, 2, &sample_count)) {
            if (mi_transmission_dir_glossy_x(&refraction_dir, state,
                                             state->ior_in, state->ior,
                                             shiny, sample)) {
                if (mi_trace_refraction(&refract_color, state, &refraction_dir))
                    miaux_add_color(result, &refract_color);
            }
            else {
                mi_reflection_dir(&reflect_dir, state);
                if (!mi_trace_reflection(&reflect_color, state, &reflect_dir))
                    mi_trace_environment(&reflect_color, state, &reflect_dir);
                miaux_add_color(result, &reflect_color);
            }
        }
        miaux_scale_color(result, 1.0 / sample_count);
    }
    return miTRUE;
}
