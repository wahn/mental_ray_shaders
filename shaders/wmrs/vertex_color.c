#include "shader.h"

struct vertex_color {
    miInteger uv_index;
};

DLLEXPORT
int vertex_color_version(void) { return 1; }

DLLEXPORT
miBoolean vertex_color ( 
    miColor *result, miState *state, struct vertex_color *params )
{
    miInteger uv_index = *mi_eval_integer(&params->uv_index);

    result->r = state->tex_list[uv_index].x;
    result->g = state->tex_list[uv_index].y;
    result->b = state->tex_list[uv_index].z;

    return miTRUE;
}
