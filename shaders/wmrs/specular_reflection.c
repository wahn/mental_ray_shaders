#include "shader.h"

DLLEXPORT
int specular_reflection_version(void) { return 1; }

DLLEXPORT
miBoolean specular_reflection (
    miColor *result, miState *state, void *params  )
{
    miVector reflection_direction;
    mi_reflection_dir(&reflection_direction, state);

    if (!mi_trace_reflection(result, state, &reflection_direction))
        mi_trace_environment(result, state, &reflection_direction);

    return miTRUE;
}
