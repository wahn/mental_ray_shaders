void miaux_set_state_refraction_indices(miState *state,
					miScalar material_ior)
{
    miState *previous_transmission = NULL;
    miScalar incoming_ior, outgoing_ior;
       
    if (miaux_ray_is_entering(state, previous_transmission)) {
	outgoing_ior = material_ior;
	incoming_ior = miaux_state_outgoing_ior(state->parent);
    } else {	
	incoming_ior = material_ior;
	outgoing_ior = miaux_state_incoming_ior(previous_transmission);
    }
    state->ior_in = incoming_ior;
    state->ior = outgoing_ior;
}

miBoolean miaux_ray_is_entering_material(miState *state)
{
    miState *s;
    miBoolean entering = miTRUE;
    for (s = state; s != NULL; s = s->parent)
	if (s->material == state->material)
	    entering = !entering;
    return entering;
}

miBoolean miaux_ray_is_transmissive(miState *state)
{
    return state->type == miRAY_TRANSPARENT ||
	state->type == miRAY_REFRACT;
}

miBoolean miaux_parent_exists(miState *state)
{ 
    return state->parent != NULL;
}

miBoolean miaux_shaders_equal(miState *s1, miState *s2)
{
    return s1->shader == s2->shader;
}

miScalar miaux_state_outgoing_ior(miState *state)
{
    miScalar unassigned_ior = 0.0, default_ior = 1.0;
    if (state != NULL && state->ior != unassigned_ior)
	return state->ior;
    else
	return default_ior;
}

miScalar miaux_state_incoming_ior(miState *state)
{
    miScalar unassigned_ior = 0.0, default_ior = 1.0;
    if (state != NULL && state->ior_in != unassigned_ior)
	return state->ior_in;
    else
	return default_ior;
}