#include "shader.h"
#include "miaux.h"

DLLEXPORT
int op_mix_ccc_version(void) { return 1; }

struct op_mix_ccc {
    miColor A;
    miColor B;
    miColor F;
};

DLLEXPORT
miBoolean op_mix_ccc (miColor *result, miState *state, struct op_mix_ccc *params )
{
    miColor *A = mi_eval_color(&params->A);
    miColor *B = mi_eval_color(&params->B);
    miColor *F = mi_eval_color(&params->F);
    result->r = miaux_blend(A->r, B->r, F->r);
    result->g = miaux_blend(A->g, B->g, F->g);
    result->b = miaux_blend(A->b, B->b, F->b);
    return miTRUE;
}
