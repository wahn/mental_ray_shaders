#include "shader.h"
extern "C" {
#include "miaux.h"
}

struct phong_framebuffer {
    miColor ambient; 
    miColor diffuse;
    miColor specular;
    miScalar exponent;
    int      i_light;
    int      n_light;
    miTag    light[1];
};

extern "C" DLLEXPORT
int phong_framebuffer_version(void) { return 1; }

extern "C" DLLEXPORT
miBoolean phong_framebuffer (
    miColor *result, miState *state, struct phong_framebuffer *params )
{
    miColor light_color, diffuse_from_light, specular_from_light,
        diffuse_component, specular_component;
    int i, light_count, light_sample_count;
    miVector direction_toward_light;
    miScalar dot_nl;
    miTag *light;

    miColor *diffuse = mi_eval_color(&params->diffuse);
    miColor *specular = mi_eval_color(&params->specular);
    miScalar exponent = *mi_eval_scalar(&params->exponent);
    miaux_light_array(&light, &light_count, state,
                      &params->i_light, &params->n_light, params->light);

    *result = *mi_eval_color(&params->ambient);
    miaux_set_channels(&diffuse_component, 0.0);
    miaux_set_channels(&specular_component, 0.0);

    for (i = 0; i < light_count; i++, light++) {
        miaux_set_channels(&diffuse_from_light, 0.0);
        miaux_set_channels(&specular_from_light, 0.0);
        light_sample_count = 0;

        while (mi_sample_light(&light_color, &direction_toward_light,
                               &dot_nl, state, *light, &light_sample_count)) {
            miaux_add_diffuse_component(
                &diffuse_from_light, dot_nl, diffuse, &light_color);
            miaux_add_phong_specular_component(
                &specular_from_light, state, exponent,
                &direction_toward_light, specular, &light_color);
        }

        if (light_sample_count > 0) {
            miScalar scale_factor = 1.0 / light_sample_count;
            miaux_add_scaled_color(
                &diffuse_component, &diffuse_from_light, scale_factor);
            miaux_add_scaled_color(
                &specular_component, &specular_from_light, scale_factor);
        }
    }
    int diffuse_index = 0;
    int specular_index = 0;
    mi::shader::Access_fb framebuffers(state->camera->buffertag);
    size_t count = 0;
    framebuffers->get_buffercount(count);
    for (unsigned int i = 0; i < count; i++) {
	const char *name = NULL;
	if (framebuffers->get_buffername(i, name)) {
	    size_t index;
	    if (framebuffers->get_index(name, index)) {
	      if (strcmp(name, "diffuse") == 0) {
		diffuse_index = index;
	      }
	      if (strcmp(name, "specular") == 0) {
		specular_index = index;
	      }
	    }
	}
    }
    mi_fb_put(state, diffuse_index, &diffuse_component);
    mi_fb_put(state, specular_index, &specular_component);

    miaux_add_color(result, &diffuse_component);
    miaux_add_color(result, &specular_component);

    return miTRUE;
}
