#include "shader.h"

struct texture_uv_simple {
    miTag tex;
};

DLLEXPORT
int texture_uv_simple_version(void) { return 1; }

DLLEXPORT
miBoolean texture_uv_simple ( 
    miColor *result, miState *state, struct texture_uv_simple *paras )
{
    mi_lookup_color_texture(
        result, state, *mi_eval_tag(&paras->tex), &state->tex_list[0]);

    return miTRUE;
}
