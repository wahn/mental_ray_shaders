#include "shader.h"
#include "contour_structs.h"

DLLEXPORT
int c_distance_store_version(void) { return 1; }

DLLEXPORT
miBoolean c_distance_store (
    void     *info_void,
    int      *info_size,
    miState  *state,
    miColor  *color )
{
    distance_contour_info *info = (distance_contour_info*)info_void;

    info->instance = state->instance;
    info->point    = state->point;
    info->normal   = state->normal;

    *info_size = sizeof(distance_contour_info);   /* For mental ray 2.x */
    return miTRUE;
}
