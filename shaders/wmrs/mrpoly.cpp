// Polygon construction toolkit for mental ray
//
// These C++ classes are used in "newblocks.cpp" for the simple
// objects developed as part of the book "Writing mental ray shaders"
// This is only an initial sketch of a possible direction for
// simplifying mental ray geometry shaders.  See the book's website for
// further developments:
//
//     http://www.writingshaders.com/
//
// No promises are made about the efficiency of this approach!


#include "mrpoly.h"
#include "matrix.h"

using namespace std;

// Utility functions

const char* get_string(miState *state, miTag *name_param, const char* default_value)
{
  miTag name_tag = *mi_eval_tag(name_param);
  return name_tag ? ((char*)mi_db_access(name_tag)) : default_value;
}

float fit(float v, float oldmin, float oldmax, float newmin, float newmax)
{
  return newmin + (newmax - newmin) * (v - oldmin) / (oldmax - oldmin);
}

void range(vector<float>& result, int count, float start, float end)
{
  for (int i= 0; i < count; i++)
    result.push_back(fit(i, 0, count-1, start, end));
}

string stringify(float f)
{
  // To use a string representation of the float values of a triple,
  // a "negative zero" needs to be converted to an unsigned value.
  // http://en.wikipedia.org/wiki/%E2%88%920_(number)
  ostringstream s;
  s.precision(6);
  s.setf(ios::fixed);
  s << f;
  string zero("0.000000");
  string negative_zero("-0.000000");
  string fs = s.str();
  if (fs.compare(negative_zero) == 0)
    fs = zero;
  return fs;
}

// ----------------------------------------------------------------------
// Class triple

triple triple::nil(-HUGE_VAL,-HUGE_VAL,-HUGE_VAL);
triple triple::x_axis(1,0,0);    
triple triple::y_axis(0,1,0);

ostream& operator<< (ostream& os, const triple& v) {
  return os << '<' << v.x << ',' << v.y << ',' << v.z << '>' << endl;
}

const triple operator+(const triple &lhs, const triple &rhs) {
  triple t(lhs.x + rhs.x, lhs.y + rhs.y, lhs.z + rhs.z); 
  return t;
}

const triple operator-(const triple &lhs, const triple &rhs) {
  triple t(lhs.x - rhs.x, lhs.y - rhs.y, lhs.z - rhs.z); 
  return t;
}

const triple operator*(const triple &lhs, float f) {
  triple t(lhs.x * f, lhs.y * f, lhs.z * f);
  return t;
}

triple& triple::min(triple& t) 
{
  x = std::min(t.x, x);
  y = std::min(t.y, y);
  z = std::min(t.z, z);
  return *this;
}

triple& triple::max(triple& t)
{
  x = std::max(t.x, x);
  y = std::max(t.y, y);
  z = std::max(t.z, z);
  return *this;
}

vector<float> triple::sort()
{
  vector<float> result;
  result.push_back(x);
  result.push_back(y);
  result.push_back(z);
  ::sort(result.begin(), result.end());
  return result;
}

triple triple::between(triple& t, float fraction) 
{
  triple result;
  result.x = ::fit(fraction, 0.0, 1.0, x, t.x);
  result.y = ::fit(fraction, 0.0, 1.0, y, t.y);
  result.z = ::fit(fraction, 0.0, 1.0, z, t.z);
  return result;
}

triple& triple::fit(triple& old_min, triple& old_max, triple& new_min, triple& new_max)
{
  x = ::fit(x, old_min.x, old_max.x, new_min.x, new_max.x);
  y = ::fit(y, old_min.y, old_max.y, new_min.y, new_max.y);
  z = ::fit(z, old_min.z, old_max.z, new_min.z, new_max.z);
  return *this;
}

triple& triple::normalize()
{
  float length = sqrt(x * x + y * y + z * z);
  x /= length;
  y /= length;
  z /= length;
  return *this;
}

triple triple::cross(triple& v)
{
  triple t(y * v.z - z * v.y,
           z * v.x - x * v.z,
           x * v.y - y * v.x);
  t.normalize();
  return t;
}

triple triple::toward(triple& v)
{
  triple t = v - *this;
  t.normalize();
  return t;
}

triple& triple::transform(matrix& m)
{
  m.mult(x, y, z);
  return *this;
}

// ----------------------------------------------------------------------
// Class hull

ostream& operator<< (ostream& os, const hull& h) {
  return os << "<hull [" << h.xmin() << "," << h.ymin() << "," << h.zmin() << "] ["
            << h.xmax() << "," << h.ymax() << "," << h.zmax() << "]>";
}

void hull::extend(triple& t)
{
  minimum.min(t);
  maximum.max(t);
}

float hull::max_extent()
{
  return extent().sort().back();
}

// ----------------------------------------------------------------------
// Class tlist

tlist tlist::nil;

tlist::tlist(int count, triple start, triple end)
{
  float max_index = (float)(count - 1);
  for (int i = 0; i < count; ++i)
    list.push_back(start.between(end, (float)i/max_index));
}

// An "arc" has <count>+1 points between the two angles to create <count> edges:
tlist tlist::arc(int count, float radius, float x, float y, float z, 
                 float start_angle, float end_angle)
{
  list.clear();
  float total_angle = end_angle - start_angle;
  triple t;
  for (int i = 0; i <= count; ++i) {
    float angle = start_angle + total_angle * ((float)i/(float)count);
    float radians = angle * M_PI / 180.0;
    float px = x + radius * cos(radians);
    float py = y + radius * sin(radians);
    t.set(px, py, z);
    list.push_back(t);
  }
  return *this;
}

// A "circle" has <count> points evenly distributed around a circle
// *without* repeating the first point at the end:

tlist tlist::circle(int count, float radius, float x, float y, float z, float start_angle)
{
  return arc(count-1, radius, x, y, z, start_angle, start_angle + (360.0 - 360.0 / (float)count));
}

tlist tlist::square(float x, float y, float z)
{
  arc(3, M_SQRT1_2, x, y, z, 45, 315);
  return *this;
}

tlist& tlist::transform(matrix& m)
{
  for (triple_iterator it = list.begin(); it != list.end(); ++it)
    m.mult(it->x, it->y, it->z);
  return *this;
}

hull tlist::bbox()
{
  hull bb;
  for (triple_iterator it = list.begin(); it != list.end(); ++it)
    bb.extend(*it);
  return bb;
}

tlist& tlist::fit(float max_extent)
{
  hull bb = bbox();
  triple center = bb.centroid();
  float scale = max_extent / bb.max_extent();
  matrix m;
  transform(m.t(-center.x, -center.y, -center.z).s(scale));
  return *this;
}


void tlist::range(int count, triple start, triple end)
{
  list.clear();
  for (int i = 0; i < count; ++i)
    add(start.between(end, (float)i/(float)(count - 10)));
}


tlist tlist::bounding_uvs(int u_index, int v_index)
{
  tlist result;
  hull extent = bbox();
  for (triple_iterator it = list.begin(); it != list.end(); ++it)
    result.add(triple(::fit((*it)[u_index], 
                            extent[u_index], extent[u_index+3], 0.0, 1.0),
                      ::fit((*it)[v_index], 
                            extent[v_index], extent[v_index+3], 0.0, 1.0)));
  return result;
}

void tlist::dump(const char* label)
{
  cout << "tlist "<< label << endl;
  for (unsigned int i = 0; i < list.size(); ++i)
    cout << i << ": " << list[i] << endl;
}



// ----------------------------------------------------------------------
// Class triple_cache

ostream& operator<< (ostream& os, const triple_cache& vc) {
  return os << "<triple_cache " << vc.size() << ">";
}

int triple_cache::index(const triple& v, const char* prefix)
{
  if (consolidate) {
    ostringstream vstr;
    vstr << prefix << ' '
         << stringify(v.x) << ' '
         << stringify(v.y) << ' '
         << stringify(v.z);
    string s = vstr.str();
    unsigned int next_index = vmap.size();
    vmap.insert(make_pair(s, next_index));
    if (next_index != vmap.size())
      triples.push_back(v);
    return vmap[s];
  } else {
    triples.push_back(v);
    return triples.size() - 1;
  }
}

void triple_cache::dump()
{
  cout << "Triple cache ---------------------------" << endl;
  vector<string> cache_contents(triples.size());
  for (map<string,int>::iterator it = vmap.begin(); it != vmap.end(); ++it) {
    cout << it->first << " => " << it->second << ": " << get(it->second) << endl;
    cache_contents[it->second] = it->first;
  }
  cout << endl;
  for (unsigned int i = 0; i < triples.size(); ++i) 
    cout << i << ": " << cache_contents[i] << ' ' << get(i) << endl;
  cout << "----------------------------------------" << endl;
}


// ----------------------------------------------------------------------
// Class vertex

ostream& operator<< (ostream& os, const vertex& v) 
{
  return os << "<" << v.p << "," << v.uv << "," << v.n << ">";
}

// ----------------------------------------------------------------------
// Class indexed_vertex

indexed_vertex::indexed_vertex(vertex v, triple_cache& tcache) 
{
  p = tcache.get_position(v.p);
  n = (v.n == triple::nil) ? -1 : tcache.get_normal(v.n);
  uv = (v.uv == triple::nil) ? -1 : tcache.get_uv(v.uv);
  bbu = (v.bbu == triple::nil) ? -1 : tcache.get_bump_basis(v.bbu);
  bbv = (v.bbv == triple::nil) ? -1 : tcache.get_bump_basis(v.bbv);
}    

// ----------------------------------------------------------------------
// Class polygon

ostream& operator<< (ostream& os, const polygon& p) 
{
  return os << "<poly-" << p.vertices.size() << "v>";
}

polygon& polygon::set_normal()
{
  triple a = vertices[2].p - vertices[1].p;
  triple b = vertices[0].p - vertices[1].p;
  a.normalize();
  b.normalize();
  normal = a.cross(b);
  return *this;
}

polygon::polygon(tlist tl)
{
  vertices.clear();
  vertices.reserve(tl.size());
  for (triple_iterator it = tl.list.begin(); it != tl.list.end(); ++it) {
    vertices.push_back(vertex(*it));
  }
  set_normal();
}

void polygon::bbox()
{
  for (vertex_iterator it = vertices.begin(); it != vertices.end(); ++it)
    extent.extend(it->p);
}

polygon& polygon::set_uvs(tlist t)
{
  triple_iterator uv_it;
  vertex_iterator v_it;
  for (uv_it = t.list.begin(), v_it = vertices.begin(); 
       uv_it != t.list.end(); ++uv_it, ++v_it) {
    v_it->uv = *uv_it;
  }
  return *this;
}

polygon& polygon::set_bump_basis(triple& basis_u, triple& basis_v)
{
  for (vertex_iterator it = vertices.begin(); it != vertices.end(); ++it) {
    it->bbu = basis_u;
    it->bbv = basis_v;
  }
  return *this;
}

polygon& polygon::set_bump_basis(tlist basis_u, tlist basis_v)
{
  if (vertices.size() == basis_u.size() && vertices.size() == basis_v.size()) {
    for (unsigned int i = 0; i < vertices.size(); ++i) {
      vertices[i].bbu = basis_u[i];
      vertices[i].bbv = basis_v[i];
    }
  }
  return *this;
}

polygon& polygon::set_bounding_uvs(int u_index, int v_index) 
{
  hull extent;
  for (vertex_iterator it = vertices.begin(); it != vertices.end(); ++it)
    extent.extend(it->p);
  for (vertex_iterator it = vertices.begin(); it != vertices.end(); ++it) {
    float u = fit(it->p[u_index], extent.xmin(), extent.xmax(), 0.0, 1.0);
    float v = fit(it->p[v_index], extent.ymin(), extent.ymax(), 0.0, 1.0);
    it->uv = triple(u,v);
  }
  return *this;
}

polygon& polygon::set_positions(tlist &t)
{
  vertices.clear();
  for (triple_iterator it = t.begin(); it != t.end(); ++it) {
    vertex v(*it);
    vertices.push_back(v);
  }
  set_normal();
  return *this;
}

void polygon::get_positions(tlist &result)
{
  for (vertex_iterator it = vertices.begin(); it != vertices.end(); ++it) {
    result.add(it->p);
  }
}    

polygon& polygon::set_normals(tlist &t)
{
  for (unsigned int i = 0; i < vertices.size(); i++)
    vertices[i].n = t[i];
  return *this;
}

polygon& polygon::triangle(triple& a, triple& b, triple& c)
{
  tlist tl;
  tl.add(a);
  tl.add(b);
  tl.add(c);
  set_positions(tl);
  return *this;
}

polygon& polygon::triangle(triple& a, triple& b, triple& c, 
                           triple& a_uv, triple& b_uv, triple& c_uv)
{
  tlist tl;
  tl.add(a);
  tl.add(b);
  tl.add(c);
  set_positions(tl);

  vertices[0].uv = a_uv;
  vertices[1].uv = b_uv;
  vertices[2].uv = c_uv;

  return *this;
}

polygon& polygon::triangle(triple& a, triple& b, triple& c, 
                           triple& a_uv, triple& b_uv, triple& c_uv,
                           triple& a_bu, triple& a_bv,
                           triple& b_bu, triple& b_bv,
                           triple& c_bu, triple& c_bv)
{
  tlist tl;
  tl.add(a);
  tl.add(b);
  tl.add(c);
  set_positions(tl);

  vertices[0].uv = a_uv;
  vertices[1].uv = b_uv;
  vertices[2].uv = c_uv;

  vertices[0].bbu = a_bu.normalize();
  vertices[0].bbv = a_bv.normalize();
  vertices[1].bbu = b_bu.normalize();
  vertices[1].bbv = b_bv.normalize();
  vertices[2].bbu = c_bu.normalize();
  vertices[2].bbv = c_bv.normalize();
  return *this;
}

polygon& polygon::interpolate_uvs(triple& start_uv, triple& end_uv)
{
  float max_index = (float)(vertices.size() - 1);
  int i = 0;
  for (vertex_iterator it = vertices.begin(); it != vertices.end(); ++it) {
    float f = (float)(i++) / (max_index);
    it->uv = start_uv.between(end_uv, f);
  }
  return *this;
}

polygon& polygon::transform(matrix& m)
{
  for (vertex_iterator it = vertices.begin(); it != vertices.end(); ++it)
    m.mult(it->p.x, it->p.y, it->p.z);
  set_normal();
  return *this;
}

// ----------------------------------------------------------------------
// Class indexed_polygon

indexed_polygon::indexed_polygon(polygon &p, triple_cache& tcache)
{
  for (vertex_iterator v = p.vertices.begin(); v != p.vertices.end(); ++v) {
    vertices.push_back(indexed_vertex(*v, tcache));
  }
  normal = tcache.get_normal(p.normal);
}


// ----------------------------------------------------------------------
// Class polygon_group

void polygon_group::add_polygon(polygon& p)
{
  indexed_polygon ip(p, tcache);
  indexed_polygons.push_back(ip);
}

void polygon_group::set_average_normals()
{
  map<int, vector<int> > vertex_normals;

  // For each polygon, loop through all the vertices:
  for (indexed_polygon_iterator p = indexed_polygons.begin(); p != indexed_polygons.end(); ++p) {
    
    // For each vertex, add the polygon's normal to the vertex_normals map:
    for (indexed_vertex_iterator v = p->vertices.begin(); v != p->vertices.end(); ++v) {
      vector<int> &vertices = vertex_normals[v->p];
      vector<int>::iterator current_vertex = find (vertices.begin(), vertices.end(), p->normal);
      if (current_vertex == vertices.end())
        vertices.push_back(p->normal);
    }
  }
  // For each vertex index in the vertex_normals map, average the normals in the list:
  for (map<int, vector<int> >::iterator vn = vertex_normals.begin(); vn != vertex_normals.end(); ++vn) {
    triple normal(0,0,0);
    for (vector<int>::iterator v = vn->second.begin(); v != vn->second.end(); ++v)
      normal += tcache.get(*v);
    normal /= vn->second.size();
    normal.normalize();
    indexed_average_normals[vn->first] = tcache.get_normal(normal);
  }
}


void polygon_group::cache_polygons()
{
  for (polygon_iterator p = polygons.begin(); p != polygons.end(); ++p) {
    indexed_polygon ip(*p, tcache);
    indexed_polygons.push_back(ip);
  }
}

void polygon_group::mi(bool average_normals)
{
  cache_polygons();

  if (average_normals)
    set_average_normals();

  int cache_size = tcache.size();
  mi_api_object_group_begin(0.0);

  // miVector data:
  for (int i = 0; i < cache_size; ++i) {
    miVector v;
    triple t = tcache.get(i);
    v.x = t.x;
    v.y = t.y;
    v.z = t.z;
    mi_api_vector_xyz_add(&v);
  }
  // Vertex data:
  for (indexed_polygon_iterator ip = indexed_polygons.begin(); ip != indexed_polygons.end(); ++ip) {
    for (indexed_vertex_iterator iv = ip->vertices.begin(); iv != ip->vertices.end(); ++iv) {
      // Position:
      mi_api_vertex_add(iv->p);

      // Normal:
      int normal_index;
      map<int,int>::iterator it = indexed_average_normals.find(iv->p);
      // First choice, averaged explicitly:
      if (it != indexed_average_normals.end())
        normal_index = indexed_average_normals[iv->p];
      // Second choice, normal assigned to the vertex:
      else if (iv->n != -1)
        normal_index = iv->n;
      // Third choice, normal of polygon:
      else
        normal_index = ip->normal;
      mi_api_vertex_normal_add(normal_index);

      // UV:
      if (iv->uv != -1)
        mi_api_vertex_tex_add(iv->uv, iv->bbu, iv->bbv);
    }
  }
  // Polygons:
  int vertex_index = 0;
  for (indexed_polygon_iterator ip = indexed_polygons.begin(); ip != indexed_polygons.end(); ip++) {
    mi_api_poly_begin_tag(1, 0);
    for (unsigned int iv = 0; iv < ip->vertices.size(); ++iv)
      mi_api_poly_index_add(vertex_index++);
    mi_api_poly_end();
  }
  // Displacement-mapping subdivision level:
  miApprox approx;
  miAPPROX_FINE_DEFAULT(approx);
  approx.subdiv[miMAX] = displacement_subdivisions;
  mi_api_poly_approx(&approx);

  mi_api_object_group_end();
}


/* Zipper 

   Create a row of polygons.  T-list c provides the direction for b's
   bump-basis vectors.  Without c, no bump basis vectors are constructed,
   and the last elements of a and b are included in the polygon set.

   c    .  .  .  .  .                c0   c1
   ^         
   b    .--.--.--.  .    |           b0---b1  b2
   |  |  |  |       v            | / |
   a    .--.--.--.  .      u -- >    a0---a1  a2   

*/

void polygon_group::zipper(tlist& a, tlist& b,
                           float u_min, float v_min, float u_max, float v_max,
                           triple& a_end, triple& b_end, tlist& c)
{
  bool use_c = c.size() != 0;
  int count = a.size() - 1;
  vector<float> u_range;
  range(u_range, count + 1, u_min, u_max);
  for (int i = 0; i < count; ++i) {
    bool last_polygon = i == (count - 1);
    triple &a0 = a[i];
    triple &a1 = a[i+1];
    triple &b0 = b[i];
    triple &b1 = b[i+1];
    triple& a2 = triple::nil;
    triple& b2 = triple::nil;
    triple& c0 = triple::nil;
    triple& c1 = triple::nil;
    if (last_polygon) {
      a2 = a_end;
      b2 = b_end;
    } else {
      a2 = a[i+2];
      b2 = b[i+2];
    }
    if (use_c) {
      c0 = c[i];
      c1 = c[i+1];
    }
    triple a0_uv(u_range[i], v_min);
    triple a1_uv(u_range[i+1], v_min);
    triple b0_uv(u_range[i], v_max);
    triple b1_uv(u_range[i+1], v_max);
    triple a0_bbu = a0.toward(a1);
    triple a0_bbv = a0.toward(b0);
    triple a1_bbv = a1.toward(b1);
    triple b0_bbu = b0.toward(b1);
    triple a1_bbu = a2 == triple::nil ? a0_bbu : a1.toward(a2);
    triple b0_bbv = use_c ? b0.toward(c0) : a0_bbv;
    triple b1_bbu = b2 == triple::nil ? b0_bbu : b1.toward(b2);
    triple b1_bbv = use_c ? b1.toward(c1) : a1_bbv;

    polygon triangle_0;
    triangle_0.triangle(a0, a1, b1, a0_uv, a1_uv, b1_uv, a0_bbu, a0_bbv, a1_bbu, a1_bbv, b1_bbu, b1_bbv); 
    add_polygon(triangle_0);

    polygon triangle_1;
    triangle_1.triangle(a0, b1, b0, a0_uv, b1_uv, b0_uv, a0_bbu, a0_bbv, b1_bbu, b1_bbv, b0_bbu, b0_bbv);
    add_polygon(triangle_1);
  }
}

/* Solid of revolution

   Rotate the profile around the axis count times.  From these, construct
   a set of levels and use zipper consecutively on them.
*/

void polygon_group::revolver(tlist profile, int count, triple& axis, 
                             float u_min, float v_min, float u_max, float v_max,
                             bool add_caps)
{
  unsigned int level_count = profile.size();
  vector<tlist> levels(level_count);
  matrix m;
  m.r(360.0 / (count - 1), axis.x, axis.y, axis.z);
  for (int i = 0; i < count; ++i) {
    profile.transform(m);
    for (unsigned int j = 0; j < level_count; ++j) {
      levels[j].add(profile[j]);
    }
  }
  for (unsigned int i = 0; i < level_count - 1; i++) {
    float v0 = fit(i, 0.0, (level_count-1), v_min, v_max);
    float v1 = fit((i+1), 0.0, (level_count-1), v_min, v_max);
    zipper(levels[i], levels[i+1], u_min, v0, u_max, v1);
  }
  if (add_caps) {
    tlist top, bottom;
    for (int i = 0; i < count; i++) {
      bottom.add(levels[0][i]);
      top.add(levels[level_count - 1][i]);
    }
    polygon t(top);
    tlist t_uvs;
    t_uvs.range(count, triple(u_min, v_max), triple(u_max, v_max));
    t.set_uvs(t_uvs);
    add_polygon(t); 

    polygon b(bottom);
    tlist b_uvs;
    b_uvs.range(count, triple(u_min, v_min), triple(u_max, v_min));
    t.set_uvs(b_uvs);
    add_polygon(b);
  }
}

// ----------------------------------------------------------------------
// Class polygon_object

map<string,int> polygon_object::names;

string polygon_object::object_name(string base_name)
{
  int serial_number = 0;
  if (names.find(base_name) == names.end())
    serial_number = names[base_name] = 1;
  else
    serial_number = ++names[base_name];
  ostringstream full_name; 
  full_name << base_name << "-" << serial_number;
  return full_name.str();
}

// (To be further parameterized for control of "options".)
void polygon_object::mi_begin(string name)
{
  obj = mi_api_object_begin(mi_mem_strdup(object_name(name).c_str()));
  obj->visible = miTRUE;
  obj->shadow = 3;
  obj->reflection = 3;
  obj->refraction = 3;
  obj->transparency = 3;
  obj->globillum = 3;
  obj->finalgather = 3;
  obj->caustic = 3;
  obj->fine = miTRUE;
}

polygon_group& polygon_object::new_group()
{
  polygon_group pgroup;
  groups.push_back(pgroup);
  return groups.front();
}

miBoolean polygon_object::mi_end(miTag *result)
{
  int fine_approx = 1;

  miApprox approx;
  if (fine_approx) {
    miAPPROX_FINE_DEFAULT(approx);
    approx.subdiv[miMAX] = 5;
  } else {
    miAPPROX_DEFAULT(approx);
  }
  mi_api_poly_approx(&approx);

  miTag object_tag = mi_api_object_end();
  return mi_geoshader_add_result(result, object_tag);
}
