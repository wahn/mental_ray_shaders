void miaux_multiply_colors(miColor *result, miColor *x, miColor *y)
{
    result->r = x->r * y->r;
    result->g = x->g * y->g;
    result->b = x->b * y->b;
}