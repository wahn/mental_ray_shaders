#include "shader.h"
#include "miaux.h"

struct show_barycentric {
    miColor a;
    miColor b;
    miColor c;
};

DLLEXPORT
int show_barycentric_version(void) {return 1;}

DLLEXPORT
miBoolean show_barycentric (  
    miColor *result, miState *state, struct show_barycentric *params  )
{
    miaux_add_scaled_color(result, mi_eval_color(&params->a), state->bary[0]);
    miaux_add_scaled_color(result, mi_eval_color(&params->b), state->bary[1]);
    miaux_add_scaled_color(result, mi_eval_color(&params->c), state->bary[2]);
    return miTRUE;
}
