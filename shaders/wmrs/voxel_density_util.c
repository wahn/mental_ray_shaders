char* miaux_tag_to_string(miTag tag, char *default_value)
{
    char *result = default_value;
    if (tag != 0) {
	result = (char*)mi_db_access(tag);
	mi_db_unpin(tag);
    }
    return result;
}

void* miaux_user_memory_pointer(miState *state, int allocation_size)
{
    void **user_pointer;
    mi_query(miQ_FUNC_USERPTR, state, 0, &user_pointer);
    if (allocation_size > 0) {
	*user_pointer = mi_mem_allocate(allocation_size);
    }
    return *user_pointer;
}

void miaux_read_volume_block(
    char* filename, 
    int *width, int *height, int *depth, float* block)
{
    int count;
    FILE* fp = fopen(filename, "r");
    if (fp == NULL) {
	mi_fatal("Error opening file \"%s\".", filename);
    }
    fscanf(fp, "%d %d %d ", width, height, depth);
    count = (*width) * (*height) * (*depth);
    mi_progress("Volume dataset: %dx%dx%d", *width, *height, *depth);
    fread(block, sizeof(float), count, fp);
}

miBoolean miaux_release_user_memory(char* shader_name, miState *state, void *params)
{
    if (params != NULL) {  /* Shader instance exit */
	void **user_pointer;                           
	if (!mi_query(miQ_FUNC_USERPTR, state, 0, &user_pointer))
	    mi_fatal("Could not get user pointer in shader exit function %s_exit", 
		     shader_name);
	mi_mem_release(*user_pointer);
    }
    return miTRUE;
}

miBoolean miaux_point_inside(miVector *p, miVector *min_p, miVector *max_p)
{
    return p->x >= min_p->x && p->y >= min_p->y && p->z >= min_p->z &&
	p->x <= max_p->x && p->y <= max_p->y && p->z <= max_p->z;
}

double miaux_fit(
    double v, double oldmin, double oldmax, double newmin, double newmax)    
{
    return newmin + ((v - oldmin) / (oldmax - oldmin)) * (newmax - newmin);
}