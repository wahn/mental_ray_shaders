#include "shader.h"
#include "contour_info.h"

struct c_contour {
    miColor color;
    miScalar width;
};

DLLEXPORT
int c_contour_version(void) { return 1; }

DLLEXPORT
miBoolean c_contour (
    miContour_endpoint *result,
    contour_info       *info_near,        
    contour_info       *info_far,
    miState            *state,
    struct c_contour   *params )
{
    result->color = *mi_eval_color(&params->color);
    result->width = *mi_eval_scalar(&params->width);
    return miTRUE;
}

