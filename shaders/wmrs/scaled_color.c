#include "shader.h"

struct scaled_color { 
    miColor base_color; 
    miColor scale_factor;
};

DLLEXPORT
int scaled_color_version(void) { return(1); }

DLLEXPORT
miBoolean scaled_color ( miColor *result, 
                         miState *state,
                         struct scaled_color *params  )
{
    miColor *base_color = mi_eval_color(&params->base_color);
    miColor *scale_factor = mi_eval_color(&params->scale_factor);

    result->r = base_color->r * scale_factor->r;
    result->g = base_color->g * scale_factor->g;
    result->b = base_color->b * scale_factor->b;

    return miTRUE;
}

