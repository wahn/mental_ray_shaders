#include "shader.h"
#include "miaux.h"

struct streak {
    miScalar max_distance;
};

DLLEXPORT
int streak_version(void) { return 1; }

DLLEXPORT
miBoolean streak (
    miColor *result, miState *state, struct streak *params  )
{
    miScalar max_distance = *mi_eval_scalar(&params->max_distance);
    state->org.z += miaux_random_range(-max_distance, max_distance);
    return mi_trace_eye(result, state, &state->org, &state->dir);
}
