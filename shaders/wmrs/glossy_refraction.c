#include "shader.h"
#include "miaux.h"

struct glossy_refraction {
    miScalar index_of_refraction;
    miScalar shiny;
};

DLLEXPORT
int glossy_refraction_version(void) { return 1; }

DLLEXPORT
miBoolean glossy_refraction (
    miColor *result, miState *state, struct glossy_refraction *params )
{
    miVector direction;
    miScalar ior = *mi_eval_scalar(&params->index_of_refraction);
    miScalar shiny = *mi_eval_scalar(&params->shiny);

    miaux_set_state_refraction_indices(state, ior);

    if (mi_transmission_dir_glossy(&direction, state,
                                   state->ior_in, state->ior, shiny))
        mi_trace_refraction(result, state, &direction);
    else {
        mi_reflection_dir(&direction, state);
        if (!mi_trace_reflection(result, state, &direction))
            mi_trace_environment(result, state, &direction);
    }
    return miTRUE;
}
