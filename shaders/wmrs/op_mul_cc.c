#include "shader.h"

struct op_mul_cc {
    miColor A;
    miColor B;
};

DLLEXPORT
int op_mul_cc_version(void) { return 1; }

DLLEXPORT
miBoolean op_mul_cc (miColor *result, miState *state, struct op_mul_cc *params )
{
    miColor *A = mi_eval_color(&params->A);
    miColor *B = mi_eval_color(&params->B);
    result->r = A->r * B->r;
    result->g = A->g * B->g;
    result->b = A->b * B->b;
    result->a = A->a * B->a;
    return miTRUE;
}
