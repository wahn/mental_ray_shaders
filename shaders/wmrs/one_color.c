#include "shader.h"

DLLEXPORT
int one_color_version(void) { return 1; }

struct one_color {
    miColor color;
};

DLLEXPORT
miBoolean one_color ( 
    miColor *result, miState *state, struct one_color *params  )
{
    *result = *mi_eval_color(&params->color);
    return miTRUE;
}
