#include "shader.h"

struct show_uv_steps {
    miInteger u_count;
    miInteger v_count;
};

DLLEXPORT
int show_uv_steps_version(void) {return 1;}

miScalar quantize(miScalar value, miInteger count)
{
    miScalar q = (miScalar)count;
    if (count < 2)
        return q;
    else
        return (miScalar)((int)(value * q) / (q - 1));
}

DLLEXPORT
miBoolean show_uv_steps (
    miColor *result, miState *state, struct show_uv_steps *params )
{
    result->r = quantize(state->tex_list[0].x, 
                         *mi_eval_integer(&params->u_count));
    result->g = quantize(state->tex_list[0].y, 
                         *mi_eval_integer(&params->v_count));
    result->b = 0;
    return miTRUE;
}
