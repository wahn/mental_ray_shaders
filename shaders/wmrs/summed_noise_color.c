#include "shader.h"
#include "miaux.h"

struct summed_noise_color { 
    miScalar point_scale;
    miScalar octave_scaling;
    miScalar summing_weight;
    miInteger number_of_octaves;
    miScalar red_exponent;
    miScalar green_exponent;
    miScalar blue_exponent;
};

DLLEXPORT
int summed_noise_color_version(void) { return(1); }

DLLEXPORT
miBoolean summed_noise_color ( 
    miColor *result, miState *state, struct summed_noise_color *params )
{
    miScalar noise_sum;
    miScalar red_exponent = *mi_eval_scalar(&params->red_exponent);
    miScalar green_exponent = *mi_eval_scalar(&params->green_exponent);
    miScalar blue_exponent = *mi_eval_scalar(&params->blue_exponent);
    miVector object_point;
    mi_point_to_object(state, &object_point, &state->point);
    mi_vector_mul(&object_point, *mi_eval_scalar(&params->point_scale));
    
    noise_sum = 
        miaux_summed_noise(&object_point,
                           *mi_eval_scalar(&params->summing_weight),
                           *mi_eval_scalar(&params->octave_scaling),
                           *mi_eval_integer(&params->number_of_octaves));
    result->r = 
        red_exponent == 1 ? noise_sum : pow(noise_sum, red_exponent);
    result->g = 
        green_exponent == 1 ? noise_sum : pow(noise_sum, green_exponent);
    result->b = 
        blue_exponent == 1 ? noise_sum : pow(noise_sum, blue_exponent);

    return miTRUE;
}

