#include "shader.h"

struct point_light {
    miColor light_color;
};

DLLEXPORT
int point_light_version(void) { return 1; }

DLLEXPORT
miBoolean point_light (
    miColor *result, miState *state, struct point_light *params )
{
    *result = *mi_eval_color(&params->light_color);
    return miTRUE;
}
