#include "shader.h"
#include "contour_info.h"

DLLEXPORT
int c_store_version(void) { return 1; }

DLLEXPORT
miBoolean c_store (
    void     *info_pointer,
    int      *info_size,
    miState  *state,
    miColor  *color )
{
    contour_info *info = (contour_info*)info_pointer;

    info->instance = state->instance;
    info->normal = state->normal;
    *info_size = sizeof(contour_info); 

    return miTRUE;
}
