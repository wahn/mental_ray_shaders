#include "shader.h"
#include "miaux.h"

DLLEXPORT
int shadow_breakpoint_scale_version(void) { return 1; }

struct shadow_breakpoint_scale {
    miColor color;
    miColor transparency;
    miScalar breakpoint;
    miScalar center;
    miScalar extent;
};

DLLEXPORT
miBoolean shadow_breakpoint_scale ( 
    miColor *result, miState *state, struct shadow_breakpoint_scale *params  )
{
    miColor *color = mi_eval_color(&params->color);
    miColor *transparency = mi_eval_color(&params->transparency);
    miScalar breakpoint = *mi_eval_scalar(&params->breakpoint);
    miScalar center = *mi_eval_scalar(&params->center);
    miScalar extent = *mi_eval_scalar(&params->extent);

    result->r *= miaux_shadow_breakpoint_scale (color->r, transparency->r, 
                                               breakpoint, center, extent );
    result->g *= miaux_shadow_breakpoint_scale (color->g, transparency->g,
                                               breakpoint, center, extent );
    result->b *= miaux_shadow_breakpoint_scale (color->b, transparency->b,
                                               breakpoint, center, extent );

    return miaux_all_channels_equal(result, 0.0) ? miFALSE : miTRUE;
}
