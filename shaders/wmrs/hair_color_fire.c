#include "shader.h"
#include "miaux.h"

struct hair_color_fire {
    miColor transparency;
};

DLLEXPORT
int hair_color_fire_version(void) { return 1; }

DLLEXPORT
miBoolean hair_color_fire (
    miColor *result, miState *state, struct hair_color_fire *params  )
{
    miScalar keys[6] = {0.0, .2, .4, .6, .9, 1.0};
    miColor colors[6] = {{1.2, 1.2, .7}, 
                         {1, 1, .5},
                         {1, .5, 0},
                         {1, .2, 0},
                         {.4, .1, 0},
                         {0, 0, 0}};
    miColor *transparency = mi_eval_color(&params->transparency);
    miScalar age = state->tex_list[0].x;
    int i;
    for (i = 4; i >=0; i--) {
        if (age >= keys[i]) {
            miaux_color_fit(result, age, 
                            keys[i], keys[i+1], &colors[i], &colors[i+1]);
            break;
        }
    }
    miaux_blend_transparency(result, state, transparency);

    return miTRUE;
}

