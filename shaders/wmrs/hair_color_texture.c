#include "shader.h"
#include "miaux.h"

DLLEXPORT
int hair_color_texture_version(void) { return 1; }

DLLEXPORT
miBoolean hair_color_texture (
    miColor *result, miState *state, void *params  )
{
    miColor hair_color, opacity, background;
    miaux_copy_color(&hair_color, (miColor*)&state->tex_list[0]);

    if (state->type == miRAY_SHADOW) {
        miScalar transparency = 1.0 - hair_color.a;
        result->r *= miaux_shadow_breakpoint(hair_color.r, transparency, 0.5);
        result->g *= miaux_shadow_breakpoint(hair_color.g, transparency, 0.5);
        result->b *= miaux_shadow_breakpoint(hair_color.b, transparency, 0.5);
        return miaux_all_channels_equal(result, 0.0) ? miFALSE : miTRUE;
    }
    miaux_copy_color(result, &hair_color);
    miaux_set_channels(&opacity, result->a);
    mi_opacity_set(state, &opacity);
    if (result->a < 1.0) {
        mi_trace_transparent(&background, state);
        miaux_blend_channels(result, &background, &opacity);
    }
    return miTRUE;
}
