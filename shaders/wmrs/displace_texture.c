#include "shader.h"
#include "miaux.h"

struct displace_texture {
    miTag texture;
    miScalar factor;
};

DLLEXPORT
int displace_texture_version(void) { return(1); }

DLLEXPORT
miBoolean displace_texture (
    miScalar *result, miState *state, struct displace_texture *params )
{
    miColor color;
    mi_lookup_color_texture(&color, state,
                            *mi_eval_tag(&params->texture),
                            &state->tex_list[0]);
    *result += *mi_eval_scalar(&params->factor) * color.r;
    return miTRUE;
}
