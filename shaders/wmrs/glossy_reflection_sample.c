#include "shader.h"
#include "miaux.h"

struct glossy_reflection_sample {
    miScalar shiny;
    miInteger samples;
};

DLLEXPORT
int glossy_reflection_sample_version(void) { return 1; }


DLLEXPORT
miBoolean glossy_reflection_sample (
    miColor *result, miState *state, struct glossy_reflection_sample *params  )
{
    miScalar shiny = *mi_eval_scalar(&params->shiny);
    miUint samples = *mi_eval_integer(&params->samples);
    miVector reflect_dir;
    miColor reflect_color;
    int sample_number = 0;
    double sampled_dir[2];

    result->r = result->g = result->b = 0.0;
    while (mi_sample(sampled_dir, &sample_number, state, 2, &samples)) {
        mi_reflection_dir_glossy_x(&reflect_dir, state, shiny, sampled_dir);
        if (!mi_trace_reflection(&reflect_color, state, &reflect_dir))
            mi_trace_environment(&reflect_color, state, &reflect_dir);
        miaux_add_color(result, &reflect_color);
    }
    miaux_scale_color(result, 1.0 / (double)samples);

    return miTRUE;
}
