#include "shader.h"
#include "miaux.h"

struct glossy_reflection_sample_varying {
    miScalar shiny;
    int i_samples;
    int n_samples;
    int samples[1];
};

DLLEXPORT
int glossy_reflection_sample_varying_version(void) { return 1; }

DLLEXPORT
miBoolean glossy_reflection_sample_varying (
    miColor *result, miState *state, 
    struct glossy_reflection_sample_varying *params )
{
    miScalar shiny = *mi_eval_scalar(&params->shiny);
    int i_samples  = *mi_eval_integer(&params->i_samples);
    int n_samples  = *mi_eval_integer(&params->n_samples);
    int *samples   = mi_eval_integer(params->samples) + i_samples;
    miVector reflect_dir;
    miColor reflect_color;
    double sampled_dir[2];
    int level = state->reflection_level, sample_number = 0;
    miUint sample_count = samples[level >= n_samples ? n_samples - 1 : level];

    if (sample_count == 1) {
        mi_reflection_dir_glossy(&reflect_dir, state, shiny);
        if (!mi_trace_reflection(result, state, &reflect_dir))
            mi_trace_environment(result, state, &reflect_dir);
    } else {
        result->r = result->g = result->b = 0.0;
        while (mi_sample(sampled_dir, &sample_number, 
                         state, 2, &sample_count)) {
            mi_reflection_dir_glossy_x(&reflect_dir, state, shiny, 
                                       sampled_dir);
            if (!mi_trace_reflection(&reflect_color, state, &reflect_dir))
                mi_trace_environment(&reflect_color, state, &reflect_dir);
            miaux_add_color(result, &reflect_color);
        }
        miaux_scale_color(result, 1.0 / (double)sample_count);
    }
    return miTRUE;
}
