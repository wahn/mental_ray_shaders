#include <math.h>
#include "shader.h"
#include "miaux.h"

struct point_light_falloff {
    miColor light_color;
};

DLLEXPORT
int point_light_falloff_version(void) {return(1);}

DLLEXPORT
miBoolean point_light_falloff (
    miColor *result, miState *state, struct point_light_falloff *params )
{
    miTag light;
    miScalar exponent;
    *result = *mi_eval_color(&params->light_color);

    if (state->type != miRAY_LIGHT) /* Visible area light */
        return miTRUE;

    mi_query(miQ_INST_ITEM, state, state->light_instance, &light);
    mi_query(miQ_LIGHT_EXPONENT, state, light, &exponent);

    miaux_scale_color(result, 1.0 / pow(state->dist, exponent));
    return mi_trace_shadow(result, state);
}
