char* miaux_tag_to_string(miTag tag, char *default_value)
{
    char *result = default_value;
    if (tag != 0) {
	result = (char*)mi_db_access(tag);
	mi_db_unpin(tag);
    }
    return result;
}

void miaux_hair_data_file_bounding_box(
    char* filename,
    float *xmin, float *ymin, float *zmin, 
    float *xmax, float *ymax, float *zmax)
{
    int hair_count, data_count;
    FILE* fp = fopen(filename, "r");
    fscanf(fp, "%d %d ", &hair_count, &data_count); /* Ignore. */
    fscanf(fp, "%f %f %f %f %f %f ", xmin, ymin, zmin, xmax, ymax, zmax);
    fclose(fp);
}

void miaux_define_hair_object(
    miTag name_tag, miaux_bbox_function bbox_function, void *params,
    miTag *geoshader_result, miApi_object_callback callback)
{
    miTag tag;
    miObject *obj;
    char *name = miaux_tag_to_string(name_tag, "::hair");
    obj = mi_api_object_begin(mi_mem_strdup(name));
    obj->visible = miTRUE;
    obj->shadow = obj->reflection = obj->refraction = 3;
    bbox_function(obj, params);
    if (geoshader_result != NULL && callback != NULL) {
	mi_api_object_callback(callback, params);
	tag = mi_api_object_end();
	mi_geoshader_add_result(geoshader_result, tag);
	obj = (miObject *)mi_scene_edit(tag);
	obj->geo.placeholder_list.type = miOBJECT_HAIR;
	mi_scene_edit_end(tag);
    }
}

void miaux_read_hair_data_file(char* filename, miScalar radius)
{
    int vertex_count, total_vertex_count, hair_scalar_size, vertex_total = 0,
	index_array_size, v, *hair_indices, *hi, hair_count, per_hair_scalars;
    float xmin, ymin, zmin, xmax, ymax, zmax, age;
    miScalar coord, *hair_scalars;
    miGeoIndex *harray;
    FILE *fp;

    fp = fopen(filename, "r");
    fscanf(fp, "%d %d ", &hair_count, &total_vertex_count);
    fscanf(fp, "%f %f %f %f %f %f ", 
	   &xmin, &ymin, &zmin, &xmax, &ymax, &zmax);
    mi_progress("particle bounding box: %f %f %f %f %f %f ",
		xmin, ymin, zmin, xmax, ymax, zmax);

    per_hair_scalars = 2;
    mi_api_hair_info(0, 'r', 1);
    mi_api_hair_info(0, 't', 1);

    hair_scalar_size = hair_count * per_hair_scalars + total_vertex_count * 3;
    hair_scalars = mi_api_hair_scalars_begin(hair_scalar_size);

    index_array_size = 1 + hair_count;
    hi = hair_indices = (int*)mi_mem_allocate(sizeof(int) * index_array_size);
    *hi++ = 0;
    vertex_total = 0;

    while (!feof(fp)) {
	*hair_scalars++ = radius;
	fscanf(fp, "%f ", &age);
	*hair_scalars++ = age; 
	fscanf(fp, "%d ", &vertex_count);
	for (v = 0; v < vertex_count * 3; v++) {
	    fscanf(fp, "%f ", &coord);
	    *hair_scalars++ = coord;
	}
	vertex_total += vertex_count * 3 + per_hair_scalars;
	*hi++ = vertex_total;
    }
    mi_api_hair_scalars_end(hair_scalar_size);
    harray = mi_api_hair_hairs_begin(index_array_size);
    memcpy(harray, hair_indices, index_array_size * sizeof(int));
    mi_api_hair_hairs_end();
}