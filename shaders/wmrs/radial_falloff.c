#include "shader.h"
#include "miaux.h"

DLLEXPORT
int radial_falloff_version(void) { return 1; }

struct radial_falloff {
    miVector center;
    miScalar radius;
    miScalar center_value;
    miScalar radius_value;
};

DLLEXPORT
miBoolean radial_falloff ( 
    miScalar *result, miState *state, struct radial_falloff *params  )
{
    miVector *center = mi_eval_vector(&params->center);
    miScalar radius = *mi_eval_scalar(&params->radius);
    miScalar center_value = *mi_eval_scalar(&params->center_value);
    miScalar radius_value = *mi_eval_scalar(&params->radius_value);
    miScalar distance;
    miVector point;

    mi_vector_to_world(state, &point, &state->point);
    distance = mi_vector_dist(center, &point);
    *result = miaux_sinusoid_fit_clamp(
        distance, 0.0, radius, center_value, radius_value);

    return miTRUE;
}
