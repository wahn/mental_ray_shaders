#include <string.h>
#include <math.h>
#include "shader.h"
#include "geoshader.h"
#include "miaux.h"

struct object_file {
    miTag name;
    miTag filename;
    miVector bbox_min;
    miVector bbox_max;
};

DLLEXPORT
int object_file_version(void) { return 1; }

DLLEXPORT
miBoolean object_file (
    miTag *result, miState *state, struct object_file *params  )
{
    char *name, *filename;

    if (!(name = miaux_tag_to_string(*mi_eval_tag(&params->name), NULL)) ||
        !(filename = miaux_tag_to_string(*mi_eval_tag(&params->filename), NULL)))
        return miFALSE;

    return mi_geoshader_add_result(
        result,
        miaux_object_from_file(name, filename,
                               *mi_eval_vector(&params->bbox_min),
                               *mi_eval_vector(&params->bbox_max)));
}

