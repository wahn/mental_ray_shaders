#include "shader.h"
#include "miaux.h"

struct store_diffuse_photon {
    miColor diffuse_color;
};

DLLEXPORT
int store_diffuse_photon_version(void) { return 1; }

DLLEXPORT
miBoolean store_diffuse_photon (
    miColor *result, miState *state, struct store_diffuse_photon *params  )
{
    miVector diffuse_direction;
    miColor *diffuse_color = mi_eval_color(&params->diffuse_color);

    mi_store_photon(result, state);

    miaux_multiply_color(result, diffuse_color);
    mi_reflection_dir_diffuse(&diffuse_direction, state);
    return mi_photon_reflection_diffuse(result, state, &diffuse_direction);
}
