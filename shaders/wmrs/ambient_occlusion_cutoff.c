#include "shader.h"
#include "miaux.h"

struct ambient_occlusion_cutoff {
    miUint samples;
    miScalar cutoff_distance;
};

DLLEXPORT
int ambient_occlusion_cutoff_version(void) {return(1);}

DLLEXPORT
miBoolean ambient_occlusion_cutoff (
    miColor *result, miState *state, struct ambient_occlusion_cutoff *params )
{
    miUint samples  = *mi_eval_integer(&params->samples);
    miScalar cutoff_distance = *mi_eval_scalar(&params->cutoff_distance);
    miVector trace_direction;
    int object_hit = 0, sample_number = 0;
    double sample[2], hit_fraction, ambient_exposure, 
        falloff_start, falloff_stop;

    falloff_start = falloff_stop = cutoff_distance;
    mi_ray_falloff(state, &falloff_start, &falloff_stop);

    while (mi_sample(sample, &sample_number, state, 2, &samples)) {
        mi_reflection_dir_diffuse_x(&trace_direction, state, sample);
        if (mi_trace_probe(state, &trace_direction, &state->point))
            object_hit++;
    }
    hit_fraction = ((double)object_hit / (double)samples);
    ambient_exposure = 1.0 - hit_fraction;
    result->r = result->g = result->b = ambient_exposure;
    result->a = 1.0;

    mi_ray_falloff(state, &falloff_start, &falloff_stop);
    return miTRUE;
}
