#include "shader.h"
#include "geoshader.h"
#include "miaux.h"

miBoolean hair_geo_2v_callback(miTag tag, void *ptr);

typedef struct {
    miTag name;
    miScalar radius;
    miVector start;
    miVector end;
} hair_geo_2v_t;

DLLEXPORT
int hair_geo_2v_version(void) { return 1; }

void hair_geo_2v_bbox(miObject *obj, void* params)
{
    hair_geo_2v_t *p = (hair_geo_2v_t*)params;
    miScalar bbox_increase = p->radius + .001;
    miaux_init_bbox(obj);
    miaux_adjust_bbox(obj, &p->start, bbox_increase);
    miaux_adjust_bbox(obj, &p->end, bbox_increase);
    miaux_describe_bbox(obj);
}    

DLLEXPORT
miBoolean hair_geo_2v (
    miTag *result, miState *state, hair_geo_2v_t *params  )
{
    hair_geo_2v_t *p = (hair_geo_2v_t*)mi_mem_allocate(sizeof(hair_geo_2v_t));
    p->name   = *mi_eval_tag(&params->name);
    p->radius = *mi_eval_scalar(&params->radius);
    p->start  = *mi_eval_vector(&params->start);
    p->end    = *mi_eval_vector(&params->end);

    miaux_define_hair_object(
        p->name, hair_geo_2v_bbox, p, result, hair_geo_2v_callback);

    return miTRUE;
}

miBoolean hair_geo_2v_callback(miTag tag, void *params)
{
    miHair_list *hair_list;
    miScalar    *hair_scalars;
    miGeoIndex  *hair_indices;
    hair_geo_2v_t *p = (hair_geo_2v_t*)params;
    int hair_scalar_count = 7;

    mi_api_incremental(miTRUE);
    miaux_define_hair_object(p->name, hair_geo_2v_bbox, p, NULL, NULL);

    hair_list = mi_api_hair_begin();
    hair_list->degree = 1;
    mi_api_hair_info(0, 'r', 1);

    hair_scalars = mi_api_hair_scalars_begin(hair_scalar_count);
    *hair_scalars++ = p->radius;
    *hair_scalars++ = p->start.x;
    *hair_scalars++ = p->start.y;
    *hair_scalars++ = p->start.z;
    *hair_scalars++ = p->end.x;
    *hair_scalars++ = p->end.y;
    *hair_scalars++ = p->end.z;
    mi_api_hair_scalars_end(hair_scalar_count);

    hair_indices = mi_api_hair_hairs_begin(2);
    hair_indices[0] = 0;
    hair_indices[1] = 7;
    mi_api_hair_hairs_end();

    mi_api_hair_end();
    mi_api_object_end();

    return miTRUE;
}
