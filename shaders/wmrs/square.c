#include "shader.h"
#include "geoshader.h"

DLLEXPORT
int square_version(void) { return 1; }

DLLEXPORT
miBoolean square (
    miTag *result, miState *state, void *params )
{
    int i;
    miVector v;
    miTag object_tag;

    /* 1. Begin the object definition. */
    miObject *obj = mi_api_object_begin(mi_mem_strdup("::square"));

    /* 2. Set the object flags. */
    obj->visible = miTRUE;
    obj->shadow = 3;

    /* 3. Begin the object group definition. */
    mi_api_object_group_begin(0.0);

    /* 4. Define the vectors to be used for vertices. */
    v.x = -.5; v.y = -.5; v.z = 0; mi_api_vector_xyz_add(&v);
    v.x = .5;  mi_api_vector_xyz_add(&v);
    v.y = .5;  mi_api_vector_xyz_add(&v);
    v.x = -.5; mi_api_vector_xyz_add(&v);

    /* 5. Define the vectors to be used for normals. */
    v.x = 0; v.y = 0; v.z = 1; mi_api_vector_xyz_add(&v);

    /* 6. Define the vectors to be used for texture coordinates. */
    v.x = 0; v.y = 0; v.z = 0; mi_api_vector_xyz_add(&v);
    v.x = 1; mi_api_vector_xyz_add(&v);    
    v.y = 1; mi_api_vector_xyz_add(&v);
    v.x = 0; mi_api_vector_xyz_add(&v);

    /* 7. Specify the sets of vectors to be used for the position, 
          normal and coordinates of each vertex. */
    for (i = 0; i < 4; i++) {
        mi_api_vertex_add(i);
        mi_api_vertex_normal_add(4);
        mi_api_vertex_tex_add(i + 5, -1, -1);
    }

    /* 8. Specify the vertices to be used for each polygon. */
    mi_api_poly_begin_tag(1, 0);
    for (i = 0; i < 4; i++)
        mi_api_poly_index_add(i);
    mi_api_poly_end();

    /* 9. End the object group definition. */
    mi_api_object_group_end();

    /* 10. End the object definition. */
    object_tag = mi_api_object_end();

    /* 11. Add the resulting object to the database. */
    return mi_geoshader_add_result(result, object_tag);
}
