double miaux_sinusoid_fit(
    double v, double oldmin, double oldmax, double newmin, double newmax)
{
    return miaux_fit(sin(miaux_fit(v, oldmin, oldmax, -M_PI_2, M_PI_2)), 
		     -1, 1, 
		     newmin, newmax);
}