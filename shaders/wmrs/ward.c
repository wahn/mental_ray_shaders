#include "shader.h"
#include "miaux.h"

struct ward {
    miColor  ambient; 
    miColor  diffuse; 
    miColor  glossy;  
    miScalar shiny_u_coeff;
    miScalar shiny_v_coeff;
    int      i_light;
    int      n_light; 
    miTag    light[1];
};

DLLEXPORT
int ward_version(void) { return 1; }

DLLEXPORT
miBoolean ward (
    miColor *result, miState *state, struct ward *params  )
{
    int i, light_count, light_sample_count;
    miColor sum, light_color;
    miVector direction_toward_light;
    miScalar dot_nl;
    miTag* light;

    miColor *diffuse       = mi_eval_color(&params->diffuse);
    miColor *glossy        = mi_eval_color(&params->glossy);
    miScalar shiny_u_coeff = *mi_eval_scalar(&params->shiny_u_coeff);
    miScalar shiny_v_coeff = *mi_eval_scalar(&params->shiny_v_coeff);
    miaux_light_array(&light, &light_count, state,
                      &params->i_light, &params->n_light, params->light);
    *result = *mi_eval_color(&params->ambient);

    for (i = 0; i < light_count; i++, light++) {
        miaux_set_channels(&sum, 0);
        light_sample_count = 0;
        while (mi_sample_light(&light_color, &direction_toward_light, &dot_nl,
                               state, *light, &light_sample_count)) {
            miaux_add_diffuse_component(&sum, dot_nl, diffuse, &light_color);
            miaux_add_ward_specular_component(
                &sum, state, shiny_u_coeff, shiny_v_coeff, glossy, dot_nl, 
                direction_toward_light, &light_color);
        }
        if (light_sample_count)
            miaux_add_scaled_color(result, &sum, 1.0/light_sample_count);
    }
    return miTRUE;
}
