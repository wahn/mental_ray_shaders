#include "shader.h"
extern "C" {
#include "miaux.h"
}

struct named_framebuffer_put { 
    miColor color;
    miTag name;
};

// Use shader's init function to report on framebuffer definitions in the camera:

extern "C" DLLEXPORT
miBoolean named_framebuffer_put_init(
    miState *state, void *params, miBoolean *instance_init_required)
{
    mi::shader::Access_fb framebuffers(state->camera->buffertag);

    size_t count = 0;
    framebuffers->get_buffercount(count);

    if (count > 0)
	mi_info("Checking the %i existing framebuffers:", count);
    else
	mi_warning("No framebuffers found.");

    for (unsigned int i = 0; i < count; i++) {
	const char *name = NULL;
	if (framebuffers->get_buffername(i, name)) {
	    size_t index;
	    if (framebuffers->get_index(name, index)) 
		mi_info("  framebuffer '%s' has index %i", name, (int)index);
	    else 
		mi_info("  framebuffer '%s' has no index", name);
	}
    }
    return miTRUE;
}

extern "C" DLLEXPORT
int named_framebuffer_put_version(void) { return(1); }

extern "C" DLLEXPORT
miBoolean named_framebuffer_put(
    miColor *result, miState *state, struct named_framebuffer_put *params)
{
    char *buffer_name = miaux_tag_to_string(*mi_eval_tag(&params->name), NULL);
    *result = *mi_eval_color(&params->color);

    mi::shader::Access_fb framebuffers(state->camera->buffertag);
    
    size_t buffer_index;
    if (buffer_name && framebuffers->get_index(buffer_name, buffer_index))
        mi_fb_put(state, buffer_index, result);

    return miTRUE;
}

