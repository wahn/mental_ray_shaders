/* 
   This is an incorrect calculation of the shadow color -- you can't
   just multiply the original light color by the transparency and
   surface colors.
*/

#include "shader.h"
#include "miaux.h"

DLLEXPORT
int shadow_color_version(void) { return 1; }

struct shadow_color {
    miColor color;
    miColor transparency;
};

DLLEXPORT
miBoolean shadow_color ( 
    miColor *result, miState *state, struct shadow_color *params  )
{
    miColor *color = mi_eval_color(&params->color);
    miColor *transparency = mi_eval_color(&params->transparency);

    result->r *= color->r * transparency->r;
    result->g *= color->g * transparency->g;
    result->b *= color->b * transparency->b;

    return miaux_all_channels_equal(result, 0.0) ? miFALSE : miTRUE;
}
