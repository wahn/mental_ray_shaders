void miaux_world_space_march_point(
    miVector *result, miState *state, miScalar distance)
{
    miaux_march_point(result, state, distance);
    mi_vector_to_world(state, result, result);
}

void miaux_march_point(
    miVector *result, miState *state, miScalar distance)
{
    miaux_point_along_vector(result, &state->org, &state->dir, distance);
}

void miaux_point_along_vector(
    miVector *result, miVector *point, miVector *direction, miScalar distance)
{
    result->x = point->x + distance * direction->x;
    result->y = point->y + distance * direction->y;
    result->z = point->z + distance * direction->z;
}

double miaux_fit_clamp(
    double v, double oldmin, double oldmax, double newmin, double newmax)
{
    if (oldmin > oldmax) {
	double temp = oldmin;
        oldmin = oldmax;
	oldmax = oldmin;
	temp = newmin;
        newmin = newmax;
	newmax = newmin;
    }
    if (v < oldmin)
	return newmin;
    else if (v > oldmax)
	return newmax;
    else 
	return miaux_fit(v, oldmin, oldmax, newmin, newmax);
}

double miaux_fit(
    double v, double oldmin, double oldmax, double newmin, double newmax)    
{
    return newmin + ((v - oldmin) / (oldmax - oldmin)) * (newmax - newmin);
}

void miaux_blend_colors(miColor *result, 
			miColor *color1, miColor *color2, miScalar factor)
{
    result->r = miaux_blend(color1->r, color2->r, factor);
    result->g = miaux_blend(color1->g, color2->g, factor);
    result->b = miaux_blend(color1->b, color2->b, factor);
}

double miaux_blend(miScalar a, miScalar b, miScalar factor)
{
    return a * factor + b * (1.0 - factor);
}