#include "shader.h"
#include "miaux.h"

struct illuminated_volume {
    miColor color;
    miVector center;
    miScalar radius;
    miScalar unit_density;
    miScalar march_increment;
    int     i_light;
    int     n_light; 
    miTag   light[1];
};

DLLEXPORT
int illuminated_volume_version(void) { return 1; }

DLLEXPORT
miBoolean illuminated_volume ( 
    miColor *result, miState *state, struct illuminated_volume *params  )
{
    miScalar radius, unit_density, march_increment, density, distance;
    miVector *center, internal_center, march_point;
    int light_count;
    miTag *light;

    if (state->type == miRAY_LIGHT)
        return miTRUE;

    center = mi_eval_vector(&params->center);
    radius = *mi_eval_scalar(&params->radius);
    unit_density = *mi_eval_scalar(&params->unit_density);
    march_increment = *mi_eval_scalar(&params->march_increment);
    mi_point_from_object(state, &internal_center, center);

    if (state->type == miRAY_SHADOW) {
        miScalar occlusion = miaux_fractional_occlusion_at_point (
            &state->org, &state->dir, state->dist,
            &internal_center, radius, unit_density, march_increment);
        miaux_scale_color(result, 1.0 - occlusion);
    } else {
        miColor *color = mi_eval_color(&params->color);
        miColor volume_color = {0,0,0,0}, light_color, point_color;
        void* original_state_pri = state->pri;
        state->pri = NULL;
        miaux_light_array(&light, &light_count, state,
                          &params->i_light, &params->n_light, params->light);

        for (distance = 0; distance <= state->dist; distance += march_increment) {
            miaux_march_point(&march_point, state, distance);
            density = miaux_threshold_density(
                &march_point, &internal_center, radius, 
                unit_density, march_increment);
            if (density > 0.0) {
                miaux_total_light_at_point(
                    &light_color, &march_point, state, light, light_count); 
                miaux_multiply_colors(&point_color, color, &light_color);
                miaux_add_transparent_color(&volume_color, &point_color, density);
            }
            if (volume_color.a == 1.0)
                break;
        }
        miaux_alpha_blend_colors(result, &volume_color, result);
        state->pri = original_state_pri;
    }
    return miTRUE;
}
