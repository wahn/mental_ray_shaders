#include <stdio.h>
#include <math.h>
#include "shader.h"
#include "miaux.h"

struct depth_of_field {
    miScalar focus_plane_distance;
    miScalar blur_radius;
    miInteger number_of_samples;
};

DLLEXPORT
int depth_of_field_version(void) { return 1; }

DLLEXPORT
miBoolean depth_of_field (
    miColor *result, miState *state, struct depth_of_field *params  )
{
    miScalar focus_plane_distance = 
        *mi_eval_scalar(&params->focus_plane_distance);
    miScalar blur_radius  = 
        *mi_eval_scalar(&params->blur_radius);
    miUint number_of_samples = 
        *mi_eval_integer(&params->number_of_samples);

    miVector camera_origin, camera_direction, origin, direction, focus_point;
    double samples[2], focus_plane_z;
    int sample_number = 0;
    miColor sum = {0,0,0,0}, single_trace;

    miaux_to_camera_space(state, &camera_origin, &camera_direction);

    focus_plane_z = state->org.z - focus_plane_distance;
    miaux_z_plane_intersect(
        &focus_point, &camera_origin, &camera_direction, focus_plane_z);

    while (mi_sample(samples, &sample_number, state, 2, &number_of_samples)) {
        miaux_sample_point_within_radius(
            &origin, &camera_origin, samples[0], samples[1], blur_radius);
        mi_vector_sub(&direction, &focus_point, &origin);
        mi_vector_normalize(&direction);
        miaux_from_camera_space(state, &origin, &direction);
        mi_trace_eye(&single_trace, state, &origin, &direction);
        miaux_add_color(&sum, &single_trace);
    }
    miaux_divide_color(result, &sum, number_of_samples);
    return miTRUE;
}
