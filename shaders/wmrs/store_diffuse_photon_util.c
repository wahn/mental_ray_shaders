void miaux_multiply_color(miColor *result, miColor *color)
{
    result->r *= color->r;
    result->g *= color->g;
    result->b *= color->b;
}