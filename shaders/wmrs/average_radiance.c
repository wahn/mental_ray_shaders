#include "shader.h"
#include "miaux.h"

DLLEXPORT
int average_radiance_version(void) { return 1; }

struct average_radiance {
    miColor color;
};

DLLEXPORT
miBoolean average_radiance ( 
    miColor *result, miState *state, struct average_radiance *params  )
{
    miColor *color = mi_eval_color(&params->color);
    mi_compute_avg_radiance(result, state, 'f', NULL);
    miaux_multiply_color(result, color);
    return miTRUE;
}
