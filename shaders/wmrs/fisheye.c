#include "shader.h"
#include "miaux.h"

struct fisheye {
    miColor outside_color;
};

DLLEXPORT
int fisheye_version(void) { return 1; }

DLLEXPORT
miBoolean fisheye (
    miColor *result, miState *state, struct fisheye *params  )
{
    miVector camera_direction;
    miScalar center_x = state->camera->x_resolution / 2.0;
    miScalar center_y = state->camera->y_resolution / 2.0;
    miScalar radius = center_x < center_y ? center_x : center_y;
    miScalar distance_from_center = 
        miaux_distance(center_x, center_y, state->raster_x, state->raster_y);

    if (distance_from_center < radius) {
        mi_vector_to_camera(state, &camera_direction, &state->dir);
        camera_direction.z *= miaux_fit(distance_from_center, 0, radius, 1, 0);
        mi_vector_normalize(&camera_direction);
        mi_vector_from_camera(state, &camera_direction, &camera_direction);
        return mi_trace_eye(result, state, &state->org, &camera_direction);
    } else {
        *result = *mi_eval_color(&params->outside_color);
        return miTRUE;
    }
}
