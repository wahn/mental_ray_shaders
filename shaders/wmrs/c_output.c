#include "shader.h"
#include "miaux.h"

struct c_output {
    miTag postscript_filename;
};

DLLEXPORT
int c_output_version(void) { return 1; }

DLLEXPORT
miBoolean c_output (
    miColor *result, miState *state, struct c_output *params )
{
    miContour_endpoint p1;
    miContour_endpoint p2;

    FILE* fp;
    char* postscript_filename =
        miaux_tag_to_string(*mi_eval_tag(&params->postscript_filename), NULL);
    if (postscript_filename == NULL)
        postscript_filename = "contour_output.ps";

    mi_progress("Writing contour PostScript data to file %s", 
                postscript_filename);
                                                                 
    fp = fopen(postscript_filename, "w");
    fprintf(fp, "%%!\n");
    fprintf(fp, "%%%%BoundingBox: 0 0 %d %d\n", 
           state->camera->x_resolution, state->camera->y_resolution);

    while (mi_get_contour_line(&p1, &p2))
        fprintf(fp, "%g %g moveto %g %g lineto stroke\n", 
               p1.point.x, p1.point.y,
               p2.point.x, p2.point.y);

    fprintf(fp, "showpage\n");
    fclose(fp);

    return miTRUE;
}
