#include "shader.h"
#include "geoshader.h"
#include "miaux.h"

struct triangles {
    miTag name;
    miInteger count;
    miVector bbox_min;
    miVector bbox_max;
    miScalar edge_length_max;
    miInteger random_seed;
};

DLLEXPORT
int triangles_version(void) { return 1; }

DLLEXPORT
miBoolean triangles (
    miTag *result, miState *state, struct triangles *params  )
{
    int vertex_index;
    miObject *obj;
    miInteger count = *mi_eval_integer(&params->count);
    miVector *bbox_min = mi_eval_vector(&params->bbox_min);
    miVector *bbox_max = mi_eval_vector(&params->bbox_max);
    miScalar edge_length_max = *mi_eval_scalar(&params->edge_length_max);
    char* name = 
        miaux_tag_to_string(*mi_eval_tag(&params->name), "::triangles");

    mi_srandom(*mi_eval_integer(&params->random_seed));

    obj = mi_api_object_begin(mi_mem_strdup(name));
    obj->visible = miTRUE;
    obj->shadow = 3;

    mi_api_object_group_begin(0.0);
    for (vertex_index = 0; vertex_index < count * 3; vertex_index += 3)
        miaux_add_random_triangle(
            vertex_index, edge_length_max, bbox_min, bbox_max);
    mi_api_object_group_end();

    return mi_geoshader_add_result(result, mi_api_object_end());
}
