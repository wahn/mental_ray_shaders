#include "shader.h"
#include "miaux.h"

struct ambient_occlusion {
    miUint samples;
};

DLLEXPORT
int ambient_occlusion_version(void) {return(1);}

DLLEXPORT
miBoolean ambient_occlusion (
    miColor *result, miState *state, struct ambient_occlusion *params )
{
    miUint samples  = *mi_eval_integer(&params->samples);
    miVector trace_direction;
    int object_hit = 0, sample_number = 0;
    double sample[2], hit_fraction, ambient_exposure;

    while (mi_sample(sample, &sample_number, state, 2, &samples)) {
        mi_reflection_dir_diffuse_x(&trace_direction, state, sample);
        if (mi_trace_probe(state, &trace_direction, &state->point))
            object_hit++;
    }
    hit_fraction = ((double)object_hit / (double)samples);
    ambient_exposure = 1.0 - hit_fraction;
    result->r = result->g = result->b = ambient_exposure;
    result->a = 1.0;

    return miTRUE;
}
