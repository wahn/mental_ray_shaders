#include "shader.h"

DLLEXPORT
int hair_color_bary_version(void) { return 1; }

DLLEXPORT
miBoolean hair_color_bary (
    miColor *result, miState *state, void *params  )
{
    result->r = state->bary[0];
    result->g = state->bary[1];
    result->b = 0;
    return miTRUE;
}
