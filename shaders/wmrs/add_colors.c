#include "shader.h"

struct add_colors { 
    miColor x;
    miColor y;
};

DLLEXPORT
int add_colors_version(void) { return 1; }

DLLEXPORT
miBoolean add_colors ( 
    miColor *result, miState *state, struct add_colors *params )
{
    miColor *x = mi_eval_color(&params->x);
    miColor *y = mi_eval_color(&params->y);

    result->r = x->r + y->r;
    result->g = x->g + y->g;
    result->b = x->b + y->b;

    return miTRUE;
}

