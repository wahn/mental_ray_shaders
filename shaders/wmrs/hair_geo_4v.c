#include "shader.h"
#include "geoshader.h"
#include "miaux.h"

miBoolean hair_geo_4v_callback(miTag tag, void *ptr);

typedef struct {
    miTag name;
    miScalar radius;
    miVector v1;
    miVector v2;
    miVector v3;
    miVector v4;
    miInteger approximation;
    miInteger degree;
} hair_geo_4v_t;

DLLEXPORT
int hair_geo_4v_version(void) { return 1; }

void hair_geo_4v_bbox(miObject *obj, void* params)
{
    hair_geo_4v_t *p = (hair_geo_4v_t*)params;
    miScalar bbox_increase = p->radius + .001;
    miaux_init_bbox(obj);
    miaux_adjust_bbox(obj, &p->v1, bbox_increase);
    miaux_adjust_bbox(obj, &p->v2, bbox_increase);
    miaux_adjust_bbox(obj, &p->v3, bbox_increase);
    miaux_adjust_bbox(obj, &p->v4, bbox_increase);
    miaux_describe_bbox(obj);
}

DLLEXPORT
miBoolean hair_geo_4v (
    miTag *result, miState *state, hair_geo_4v_t *params  )
{
    hair_geo_4v_t *p = (hair_geo_4v_t*)mi_mem_allocate(sizeof(hair_geo_4v_t));
    p->radius        = *mi_eval_scalar(&params->radius);
    p->v1            = *mi_eval_vector(&params->v1);
    p->v2            = *mi_eval_vector(&params->v2);
    p->v3            = *mi_eval_vector(&params->v3);
    p->v4            = *mi_eval_vector(&params->v4);
    p->approximation = *mi_eval_integer(&params->approximation);
    p->degree        = *mi_eval_integer(&params->degree);

    miaux_define_hair_object(
        p->name, hair_geo_4v_bbox, p, result, hair_geo_4v_callback);

    return miTRUE;
}

miBoolean hair_geo_4v_callback(miTag tag, void *params)
{
    miHair_list *hair_list;
    miScalar    *hair_scalars;
    miGeoIndex  *hair_indices;
    hair_geo_4v_t *p = (hair_geo_4v_t *)params;
    int hair_count = 1, hair_scalar_count = 4 * 3 + 1;

    mi_api_incremental(miTRUE);
    miaux_define_hair_object(p->name, hair_geo_4v_bbox, p, NULL, NULL);
    hair_list = mi_api_hair_begin();

    /* WAS: hair_list->approx = p->approximation; */
    miAPPROX_DEFAULT(hair_list->approx);
    hair_list->approx.cnst[miCNST_UPARAM] = (miScalar)p->approximation / (miScalar)p->degree;

    hair_list->degree = p->degree;
    mi_api_hair_info(0, 'r', 1);

    hair_scalars = mi_api_hair_scalars_begin(hair_scalar_count);
    *hair_scalars++ = p->radius;
    miaux_append_hair_vertex(&hair_scalars, &p->v1);
    miaux_append_hair_vertex(&hair_scalars, &p->v2);
    miaux_append_hair_vertex(&hair_scalars, &p->v3);
    miaux_append_hair_vertex(&hair_scalars, &p->v4);
    mi_api_hair_scalars_end(hair_scalar_count);

    hair_indices = mi_api_hair_hairs_begin(hair_count + 1);
    hair_indices[0] = 0;
    hair_indices[1] = hair_scalar_count;
    mi_api_hair_hairs_end();

    mi_api_hair_end();
    mi_api_object_end();

    return miTRUE;
}
