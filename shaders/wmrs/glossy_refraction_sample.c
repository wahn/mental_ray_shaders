#include "shader.h"
#include "miaux.h"

struct glossy_refraction_sample {
    miScalar index_of_refraction;
    miScalar shiny;
    miInteger samples;
};

DLLEXPORT
int glossy_refraction_sample_version(void) { return 1; }

DLLEXPORT
miBoolean glossy_refraction_sample (
    miColor *result, miState *state, struct glossy_refraction_sample *params )
{
    miScalar ior = *mi_eval_scalar(&params->index_of_refraction);
    miScalar shiny = *mi_eval_scalar(&params->shiny);
    miUint samples = *mi_eval_integer(&params->samples);
    miVector refract_dir, reflect_dir;
    miColor refract_color;
    int sample_number = 0;
    double sample[2];

    miaux_set_state_refraction_indices(state, ior);

    result->r = result->g = result->b = 0.0;
    while (mi_sample(sample, &sample_number, state, 2, &samples)) {
        if (mi_transmission_dir_glossy_x(&refract_dir, state,
                                         state->ior_in, state->ior,
                                         shiny, sample)) {
            if (mi_trace_refraction(&refract_color, state, &refract_dir))
                miaux_add_color(result, &refract_color);
        }
        else {
            mi_reflection_dir(&reflect_dir, state);
            if (!mi_trace_reflection(&refract_color, state, &reflect_dir))
                mi_trace_environment(&refract_color, state, &reflect_dir);
            miaux_add_color(result, &refract_color);
        }
    }
    miaux_scale_color(result, 1.0 / samples);
    return miTRUE;
}
