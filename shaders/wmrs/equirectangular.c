#include "shader.h"
#include "miaux.h"

DLLEXPORT
int equirectangular_version(void) { return 1; }

DLLEXPORT
miBoolean equirectangular (
    miColor *result, miState *state, void *params  )
{
    miMatrix matrix;
    miVector eye_ray_direction = {0, 0, -1};
    miScalar x_fractional_position = 
        state->raster_x / state->camera->x_resolution;
    miScalar y_fractional_position = 
        state->raster_y / state->camera->y_resolution;

    mi_matrix_rotate(
        matrix,
        miaux_fit(y_fractional_position, 0, 1, -M_PI/2, M_PI/2),
        miaux_fit(x_fractional_position, 0, 1, M_PI, -M_PI),
        0);
    mi_vector_transform(&eye_ray_direction, &eye_ray_direction, matrix);

    return mi_trace_eye(result, state, &state->org, &eye_ray_direction);
}
