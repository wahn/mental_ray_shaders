#include "shader.h"
#include "miaux.h"

struct cook_torrance {
    miColor  ambient;  
    miColor  diffuse;  
    miColor  specular; 
    miScalar avg_microfacet_slope;
    miColor  index_of_refraction;     
    int      i_light; 
    int      n_light; 
    miTag    light[1];
};

DLLEXPORT
int cook_torrance_version(void) { return 1; }

DLLEXPORT
miBoolean cook_torrance ( 
    miColor *result, miState *state, struct cook_torrance *params  )
{
    int i, light_count, light_sample_count;
    miColor sum, light_color;
    miVector direction_toward_light;
    miScalar dot_nl;
    miTag *light;

    miColor *diffuse              = mi_eval_color(&params->diffuse);
    miColor *specular             = mi_eval_color(&params->specular);
    miScalar avg_microfacet_slope = *mi_eval_scalar(&params->avg_microfacet_slope);
    miColor *index_of_refraction  = mi_eval_color(&params->index_of_refraction);
    miaux_light_array(&light, &light_count, state,
                      &params->i_light, &params->n_light, params->light);
    *result = *mi_eval_color(&params->ambient);

    for (i = 0; i < light_count; i++, light++) {
        miaux_set_channels(&sum, 0);
        light_sample_count = 0;
        while (mi_sample_light(&light_color, &direction_toward_light, &dot_nl, 
                               state, *light, &light_sample_count)) {
            miaux_add_diffuse_component(&sum, dot_nl, diffuse, &light_color);
            miaux_add_cook_torrance_specular_component(
                &sum, state, avg_microfacet_slope, index_of_refraction, 
                direction_toward_light, specular, &light_color);
        }
        if (light_sample_count)
            miaux_add_scaled_color(result, &sum, 1.0/light_sample_count);
    }
    return miTRUE;
}
