#include "shader.h"

struct point_light_shadow {
    miColor light_color;
};

DLLEXPORT
int point_light_shadow_version(void) { return 1; }

DLLEXPORT
miBoolean point_light_shadow (
    miColor *result, miState *state, struct point_light_shadow *params )
{
    *result = *mi_eval_color(&params->light_color);
    return mi_trace_shadow(result, state);
}
