void miaux_march_point(
    miVector *result, miState *state, miScalar distance)
{
    miaux_point_along_vector(result, &state->org, &state->dir, distance);
}

void miaux_point_along_vector(
    miVector *result, miVector *point, miVector *direction, miScalar distance)
{
    result->x = point->x + distance * direction->x;
    result->y = point->y + distance * direction->y;
    result->z = point->z + distance * direction->z;
}

miScalar miaux_threshold_density(
    miVector *point, miVector *center, miScalar radius, 
    miScalar unit_density, miScalar march_increment)
{
    miScalar distance = mi_vector_dist(center, point);
    if (distance <= radius)
	return unit_density * march_increment;
    else
	return 0.0;
}

void miaux_copy_color(miColor *result, miColor *color)
{
    result->r = color->r;
    result->g = color->g;
    result->b = color->b;
    result->a = color->a;
}