miBoolean miaux_ray_is_entering_material(miState *state)
{
    miState *s;
    miBoolean entering = miTRUE;
    for (s = state; s != NULL; s = s->parent)
	if (s->material == state->material)
	    entering = !entering;
    return entering;
}