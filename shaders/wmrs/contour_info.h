#ifndef _CONTOUR_INFO_H_
#define _CONTOUR_INFO_H_

#include "shader.h"

typedef struct {
    miTag    instance;
    miVector normal;
} contour_info;

#endif
