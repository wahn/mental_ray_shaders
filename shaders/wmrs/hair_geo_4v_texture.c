#include "shader.h"
#include "geoshader.h"
#include "miaux.h"

miBoolean hair_geo_4v_texture_callback(miTag tag, void *ptr);

typedef struct {
    miTag name;
    miVector p1;
    miVector p2;
    miVector p3;
    miVector p4;
    miScalar root_radius;
    miColor root_color;
    miScalar tip_radius;
    miColor tip_color;
    miInteger approximation;
    miInteger degree;
} hair_geo_4v_texture_t;

DLLEXPORT
int hair_geo_4v_texture_version(void) { return 1; }

void hair_geo_4v_texture_bbox(miObject *obj, void* params)
{
    hair_geo_4v_texture_t *p = (hair_geo_4v_texture_t*)params;
    double max_radius = miaux_max(p->root_radius, p->tip_radius) + .001;

    miaux_init_bbox(obj);
    miaux_adjust_bbox(obj, &p->p1, max_radius);
    miaux_adjust_bbox(obj, &p->p2, max_radius);
    miaux_adjust_bbox(obj, &p->p3, max_radius);
    miaux_adjust_bbox(obj, &p->p4, max_radius);
    miaux_describe_bbox(obj);
}

DLLEXPORT
miBoolean hair_geo_4v_texture (
    miTag *result, miState *state, hair_geo_4v_texture_t *params  )
{
    hair_geo_4v_texture_t *p = 
        (hair_geo_4v_texture_t*) mi_mem_allocate(sizeof(hair_geo_4v_texture_t));

    p->p1 = *mi_eval_vector(&params->p1);
    p->p2 = *mi_eval_vector(&params->p2);
    p->p3 = *mi_eval_vector(&params->p3);
    p->p4 = *mi_eval_vector(&params->p4);
    p->root_radius = *mi_eval_scalar(&params->root_radius);
    p->root_color = *mi_eval_color(&params->root_color);
    p->tip_radius = *mi_eval_scalar(&params->tip_radius);
    p->tip_color  = *mi_eval_color(&params->tip_color);
    p->approximation = *mi_eval_integer(&params->approximation);
    p->degree = *mi_eval_integer(&params->degree);

    miaux_define_hair_object(
        p->name, hair_geo_4v_texture_bbox, p, result, 
        hair_geo_4v_texture_callback);

    return miTRUE;
}

miBoolean hair_geo_4v_texture_callback(miTag tag, void *ptr)
{
    miHair_list *hair_list;
    miScalar    *hair_scalars;
    miGeoIndex  *hair_indices;
    hair_geo_4v_texture_t *p = (hair_geo_4v_texture_t *)ptr;
    int i;

    int hair_count = 1;
    int vertices_per_hair = 4;
    int scalars_per_vertex = 8;
    int hair_scalar_count = vertices_per_hair * scalars_per_vertex;

    miVector vertices[4];
    vertices[0] = p->p1;
    vertices[1] = p->p2;
    vertices[2] = p->p3;
    vertices[3] = p->p4;

    mi_api_incremental(miTRUE);

    miaux_define_hair_object(
        p->name, hair_geo_4v_texture_bbox, p, NULL, NULL);

    hair_list = mi_api_hair_begin();

    /* WAS: hair_list->approx = p->approximation; */
    miAPPROX_DEFAULT(hair_list->approx);
    hair_list->approx.cnst[miCNST_UPARAM] = (miScalar)p->approximation / (miScalar)p->degree;

    hair_list->degree = p->degree;
    mi_api_hair_info(1, 'r', 1);
    mi_api_hair_info(1, 't', 4);

    hair_scalars = mi_api_hair_scalars_begin(hair_scalar_count);
    for (i = 0; i < vertices_per_hair; i++)
        miaux_append_hair_data(
            &hair_scalars, &vertices[i], 
            miaux_fit(i, 0, vertices_per_hair, 0, 1),
            p->root_radius, &p->root_color, p->tip_radius, &p->tip_color);
    mi_api_hair_scalars_end(hair_scalar_count);

    hair_indices = mi_api_hair_hairs_begin(hair_count + 1);
    hair_indices[0] = 0;
    hair_indices[1] = hair_scalar_count;
    mi_api_hair_hairs_end();

    mi_api_hair_end();
    mi_api_object_end();

    return miTRUE;
}
