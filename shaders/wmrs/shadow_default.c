#include "shader.h"

DLLEXPORT
int shadow_default_version(void) { return 1; }

DLLEXPORT
miBoolean shadow_default ( 
    miColor *result, miState *state, void *params  )
{
    result->r = result->g = result->b = 0.0;
    return miFALSE;
}
