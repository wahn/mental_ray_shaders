#include "shader.h"
#include "miaux.h"
#include <stdlib.h>

struct median_filter {
    miInteger radius;
};

DLLEXPORT
int median_filter_version(void) { return 1; }

DLLEXPORT
miBoolean median_filter (
    void *result, miState *state, struct median_filter *params )
{
    miColor *neighbors;
    int radius = *mi_eval_integer(&params->radius), x, y,
        kernel_size = (radius * 2 + 1) * (radius * 2 + 1),
        middle = kernel_size / 2;

    miImg_image *fb_input = mi_output_image_open(state, miRC_IMAGE_RGBA);
    miImg_image *fb_output = mi_output_image_open(state, miRC_IMAGE_USER);

    neighbors = (miColor*) mi_mem_allocate(kernel_size * sizeof(miColor));
    for (y = 0; y < state->camera->y_resolution; y++) {
        if (mi_par_aborted())
            break;
        for (x = 0; x < state->camera->x_resolution; x++) {
            miaux_pixel_neighborhood(neighbors, fb_input, x, y, radius);
            qsort(neighbors, kernel_size, sizeof(miColor), 
                  miaux_color_compare);
            mi_img_put_color(fb_output, &neighbors[middle], x, y);
        }
    }
    mi_mem_release(neighbors);

    mi_output_image_close(state, miRC_IMAGE_RGBA);
    mi_output_image_close(state, miRC_IMAGE_USER);

    return miTRUE;
}
