void miaux_light_array(miTag **lights, int *light_count, miState *state,
		       int *offset_param, int *count_param, miTag *lights_param)
{
    int array_offset = *mi_eval_integer(offset_param);
    *light_count = *mi_eval_integer(count_param);
    *lights = mi_eval_tag(lights_param) + array_offset;
}

void miaux_set_channels(miColor *c, miScalar new_value)
{
    c->r = c->g = c->b = c->a = new_value;
}

void miaux_add_light_color(
    miColor *result, miState *state, miTag light, char shadow_state)
{
    int light_sample_count = 0;
    miScalar dot_nl;
    miVector direction_toward_light;
    miColor sample_color, single_light_color;

    const miOptions *original_options = state->options;
    miOptions options_copy = *original_options;
    options_copy.shadow = shadow_state;
    state->options = &options_copy;

    miaux_set_channels(&single_light_color, 0.0);
    while (mi_sample_light(&sample_color, &direction_toward_light,
			   &dot_nl, state, light, &light_sample_count))
	miaux_add_scaled_color(&single_light_color, &sample_color, dot_nl);
    if (light_sample_count)
	miaux_add_scaled_color(result, &single_light_color,
			       1.0/light_sample_count);

    state->options = (miOptions*)original_options;
}

void miaux_add_scaled_color(miColor *result, miColor *color, miScalar scale)
{
    result->r += color->r * scale;
    result->g += color->g * scale;
    result->b += color->b * scale;
}

void miaux_divide_colors(miColor *result, miColor *x, miColor *y)
{
    result->r = y->r == 0.0 ? 1.0 -: x->r / y->r;
    result->g = y->g == 0.0 ? 0.0 -: x->g / y->g;
    result->b = y->b == 0.0 ? 1.0 -: x->b / y->b;
}

void miaux_multiply_colors(miColor *result, miColor *x, miColor *y)
{
    result->r = x->r * y->r;
    result->g = x->g * y->g;
    result->b = x->b * y->b;
}