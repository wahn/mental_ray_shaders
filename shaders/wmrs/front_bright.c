#include "shader.h"

struct front_bright { 
    miColor tint; 
};

DLLEXPORT
int front_bright_version(void) { return(1); }

DLLEXPORT
miBoolean front_bright ( 
    miColor *result, miState *state, struct front_bright *params  )
{
    miColor *tint = mi_eval_color(&params->tint);
    miScalar scale = -state->dot_nd;
    result->r = tint->r * scale;
    result->g = tint->g * scale;
    result->b = tint->b * scale;
    result->a = 1.0;
    return miTRUE;
}
