#include "shader.h"
#include "miaux.h"

struct bump_texture { 
    miTag texture;
    miScalar factor;
    miScalar nearby;
};

DLLEXPORT
int bump_texture_version(void) { return(1); }

DLLEXPORT
miBoolean bump_texture ( 
    miColor *result, miState *state, struct bump_texture *params  )
{
    miColor color_here, color_over, color_up;
    miScalar value_here, value_over, value_up,
        change_going_over, change_going_up;
    miVector here, over, up;

    miTag texture = *mi_eval_tag(&params->texture);
    miScalar factor = *mi_eval_scalar(&params->factor);
    miScalar nearby = *mi_eval_scalar(&params->nearby);
    
    miVector bump_basis_u = state->bump_x_list[0];
    miVector bump_basis_v = state->bump_y_list[0];

    here = state->tex_list[0];
    miaux_set_vector(&over, here.x + nearby, here.y, here.z);
    miaux_set_vector(&up, here.x, here.y + nearby, here.z);

    if (!mi_lookup_color_texture(&color_here, state, texture, &here))
        return miFALSE;
    value_here = miaux_color_channel_average(&color_here);

    mi_flush_cache(state);
    value_over = mi_lookup_color_texture(&color_over, state, texture, &over) ?
        miaux_color_channel_average(&color_over) : value_here;

    mi_flush_cache(state);
    value_up = mi_lookup_color_texture(&color_up, state, texture, &up) ?
        miaux_color_channel_average(&color_up) : value_here;

    change_going_over = factor * (value_over - value_here);
    change_going_up = factor * (value_up - value_here);

    mi_vector_mul(&bump_basis_u, -change_going_over);
    mi_vector_mul(&bump_basis_v, -change_going_up);
    
    mi_vector_to_object(state, &state->normal, &state->normal);
    mi_vector_add(&state->normal, &state->normal, &bump_basis_u);
    mi_vector_add(&state->normal, &state->normal, &bump_basis_v);
    mi_vector_normalize(&state->normal);
    mi_vector_from_object(state, &state->normal, &state->normal);

    return miTRUE;
}

