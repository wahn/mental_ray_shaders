#include "shader.h"
extern "C" {
#include "miaux.h"
}

struct shadowpass {
    miColor base_color;
    int     i_light;
    int     n_light;
    miTag   light[1];
};

extern "C" DLLEXPORT
int shadowpass_version(void) { return 1; }

extern "C" DLLEXPORT
miBoolean shadowpass (
    miColor *result, miState *state, struct shadowpass *params  )
{
    int i, light_count;
    miTag *light;
    miColor without_shadow, with_shadow, shadow;
    miColor *base_color = mi_eval_color(&params->base_color);
    miaux_light_array(&light, &light_count, state,
                      &params->i_light, &params->n_light, params->light);

    miaux_set_channels(&without_shadow, 0.0);
    miaux_set_channels(&with_shadow, 0.0);

    for (i = 0; i < light_count; i++, light++) {
        miaux_add_light_color(&without_shadow, state, *light, miFALSE);
        miaux_add_light_color(&with_shadow, state, *light, miTRUE);
    }
    miaux_divide_colors(&shadow, &with_shadow, &without_shadow);

    miaux_multiply_colors(result, base_color, &shadow);
    int shadows_index = 0;
    int without_shadows_index = 0;
    mi::shader::Access_fb framebuffers(state->camera->buffertag);
    size_t count = 0;
    framebuffers->get_buffercount(count);
    for (unsigned int i = 0; i < count; i++) {
	const char *name = NULL;
	if (framebuffers->get_buffername(i, name)) {
	    size_t index;
	    if (framebuffers->get_index(name, index)) {
	      if (strcmp(name, "shadows") == 0) {
		shadows_index = index;
	      }
	      if (strcmp(name, "without_shadows") == 0) {
		without_shadows_index = index;
	      }
	    }
	}
    }
    mi_fb_put(state, shadows_index, &shadow);
    mi_fb_put(state, without_shadows_index, base_color);

    return miTRUE;
}

