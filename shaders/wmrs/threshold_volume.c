#include "shader.h"
#include "miaux.h"

struct threshold_volume {
    miColor color;
    miVector center;
    miScalar radius;
    miScalar density_threshold;
    miScalar unit_density;
    miScalar march_increment;
};

DLLEXPORT
int threshold_volume_version(void) { return 1; }

DLLEXPORT
miBoolean threshold_volume ( 
    miColor *result, miState *state, struct threshold_volume *params  )
{
    miScalar march_increment = *mi_eval_scalar(&params->march_increment);
    miColor *color = mi_eval_color(&params->color);
    miVector *center = mi_eval_vector(&params->center);
    miScalar radius = *mi_eval_scalar(&params->radius);
    miScalar unit_density = *mi_eval_scalar(&params->unit_density);
    miScalar density_threshold = *mi_eval_scalar(&params->density_threshold);
    miScalar distance, accumulated_density = 0.0;
    miVector march_point, internal_center;
    mi_point_from_object(state, &internal_center, center);

    for (distance = 0; distance < state->dist; distance += march_increment) {
        miaux_march_point(&march_point, state, distance);
        accumulated_density += 
            miaux_threshold_density(&march_point, &internal_center, radius, 
                                    unit_density, march_increment);
        if (accumulated_density > density_threshold) {
            miaux_copy_color(result, color);
            break;
        }
    }
    return miTRUE;
}
