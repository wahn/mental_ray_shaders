// C++

#include "shader.h"

extern "C"
DLLEXPORT
int negate_version(void) { return 1; }

extern "C"
DLLEXPORT
miBoolean negate (void *result, miState *state, void *params )
{
    miImg_image *fb = mi_output_image_open(state, miRC_IMAGE_RGBA);
    for (int y = 0; y < state->camera->y_resolution; y++) {
        for (int x = 0; x < state->camera->x_resolution; x++) {
            miColor pixel;
            mi_img_get_color(fb, &pixel, x, y);
            pixel.r = 1.0 - pixel.r;
            pixel.g = 1.0 - pixel.g;
            pixel.b = 1.0 - pixel.b;
            mi_img_put_color(fb, &pixel, x, y);
        }
    }
    mi_output_image_close(state, miRC_IMAGE_RGBA);
    return miTRUE;
}
