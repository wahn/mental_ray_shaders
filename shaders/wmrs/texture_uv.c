#include "shader.h"
#include <math.h>

struct texture_uv {
    miTag tex;
    miScalar u_scale;
    miScalar v_scale;
    miScalar u_offset;
    miScalar v_offset;
};

DLLEXPORT
int texture_uv_version(void) {return 1;}

DLLEXPORT
miBoolean texture_uv ( 
    miColor *result, miState *state, struct texture_uv *params )
{
    miVector uv_coord = {0.0, 0.0, 0.0};
    miScalar u_scale  = *mi_eval_scalar(&params->u_scale);
    miScalar v_scale  = *mi_eval_scalar(&params->v_scale);
    miScalar u_offset = *mi_eval_scalar(&params->u_offset);
    miScalar v_offset = *mi_eval_scalar(&params->v_offset);

    uv_coord.x = fmod((state->tex_list[0].x * u_scale) + u_offset, 1.0);
    uv_coord.y = fmod((state->tex_list[0].y * v_scale) + v_offset, 1.0);

    mi_lookup_color_texture(
        result, state, *mi_eval_tag(&params->tex), &uv_coord);

    return miTRUE;
}
