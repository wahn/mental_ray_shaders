void miaux_color_fit(miColor *result, 
		     miScalar f, miScalar start, miScalar end,
		     miColor *start_color, miColor *end_color)
{
    result->r = miaux_fit(f, start, end, start_color->r, end_color->r);
    result->g = miaux_fit(f, start, end, start_color->g, end_color->g);
    result->b = miaux_fit(f, start, end, start_color->b, end_color->b);
}

double miaux_fit(
    double v, double oldmin, double oldmax, double newmin, double newmax)    
{
    return newmin + ((v - oldmin) / (oldmax - oldmin)) * (newmax - newmin);
}

void miaux_blend_transparency(miColor *result, 
			      miState *state, miColor *transparency)
{
    miColor opacity, background;
    miaux_invert_channels(&opacity, transparency);
    mi_opacity_set(state, &opacity);
    if (!miaux_all_channels_equal(transparency, 1.0)) {
        mi_trace_transparent(&background, state);
        miaux_blend_channels(result, &background, &opacity);
    }
}

void miaux_invert_channels(miColor *result, miColor *color)
{
    result->r = 1.0 - color->r;
    result->g = 1.0 - color->g;
    result->b = 1.0 - color->b;
    result->a = 1.0 - color->a;
}

void miaux_blend_channels(miColor *result,
			  miColor *blend_color, miColor *blend_fraction)
{
    result->r = miaux_blend(result->r, blend_color->r, blend_fraction->r);
    result->g = miaux_blend(result->g, blend_color->g, blend_fraction->g);
    result->b = miaux_blend(result->b, blend_color->b, blend_fraction->b);
}

double miaux_blend(miScalar a, miScalar b, miScalar factor)
{
    return a * factor + b * (1.0 - factor);
}