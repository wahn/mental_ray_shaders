#include "shader.h"
#include "miaux.h"

struct sinusoid_soft_spotlight {
    miColor light_color;
    miScalar inner_spread;
};

DLLEXPORT
int sinusoid_soft_spotlight_version(void) { return 1; }

DLLEXPORT
miBoolean sinusoid_soft_spotlight (
    miColor *result, miState *state, struct sinusoid_soft_spotlight *params )
{
    miScalar inner_spread, attenuation;
    miTag light_tag = miaux_current_light_tag(state);
    miScalar offset_spread = miaux_offset_spread_from_light(state, light_tag);
    miScalar light_spread = miaux_light_spread(state, light_tag);

    if (offset_spread < light_spread)
        return miFALSE;

    *result = *mi_eval_color(&params->light_color);
    inner_spread = *mi_eval_scalar(&params->inner_spread);

    if (offset_spread < inner_spread) {
        attenuation = 
            miaux_sinusoid_fit(offset_spread, inner_spread, light_spread, 1, 0);
        miaux_scale_color(result, attenuation);
    }
    return mi_trace_shadow(result, state);
}
