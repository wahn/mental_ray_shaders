#include "shader.h"
#include "miaux.h"

struct bump_wave_uv { 
    miScalar frequency_u;
    miScalar amplitude_u;
    miScalar frequency_v;
    miScalar amplitude_v;
};

DLLEXPORT
int bump_wave_uv_version(void) { return(1); }

DLLEXPORT
miBoolean bump_wave_uv ( 
    miColor *result, miState *state, struct bump_wave_uv *params  )
{
    state->normal.x += miaux_sinusoid(state->tex_list[0].x,
                                      *mi_eval_scalar(&params->frequency_u),
                                      *mi_eval_scalar(&params->amplitude_u));
    state->normal.y += miaux_sinusoid(state->tex_list[0].y,
                                      *mi_eval_scalar(&params->frequency_v),
                                      *mi_eval_scalar(&params->amplitude_v));
    mi_vector_normalize(&state->normal);
    return miTRUE;
}

