#include "shader.h"
#include "miaux.h"
#include <stdlib.h>

struct letterbox {
    miScalar aspect_ratio;
    miColor outside_scale;
};

DLLEXPORT
int letterbox_version(void) { return 1; }

DLLEXPORT
miBoolean letterbox (
    void *result, miState *state, struct letterbox  *params )
{
    int x, y;
    miColor pixel;
    miScalar aspect_ratio = *mi_eval_scalar(&params->aspect_ratio);
    miColor *outside_scale = mi_eval_color(&params->outside_scale);
    miScalar image_width = state->camera->x_resolution;
    miScalar image_height = state->camera->y_resolution;
    miScalar letterbox_height = image_width / aspect_ratio;
    miScalar y_min = (image_height - letterbox_height) / 2.0;
    miScalar y_max = image_height - y_min;

    miImg_image *fb = mi_output_image_open(state, miRC_IMAGE_RGBA);

    for (y = 0; y < state->camera->y_resolution; y++) {
        if (mi_par_aborted())
            break;
        for (x = 0; x < state->camera->x_resolution; x++) {
            mi_img_get_color(fb, &pixel, x, y);
            if (y < y_min || y > y_max) {
                miaux_multiply_colors(&pixel, &pixel, outside_scale);
                mi_img_put_color(fb, &pixel, x, y);
            }
        }
    }
    mi_output_image_close(state, miRC_IMAGE_RGBA);

    return miTRUE;
}
