#include "shader.h"

struct depth_fade { 
    miScalar near;
    miScalar far;
};

DLLEXPORT
int depth_fade_version(void) { return(1); }

DLLEXPORT
miBoolean depth_fade ( 
    miColor *result, miState *state, struct depth_fade *params  )
{
    miScalar near = *mi_eval_scalar(&params->near);
    miScalar far = *mi_eval_scalar(&params->far);
    miScalar zpos = state->point.z, factor;
    if (zpos > near)
        factor = 1.0;
    else if (zpos < far)
        factor = 0.0;
    else
        factor =  (zpos - far) / (near - far);
    result->r = result->g = result->b = factor;
    return miTRUE;
}


