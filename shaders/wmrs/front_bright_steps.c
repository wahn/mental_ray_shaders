#include "shader.h"
#include "miaux.h"

struct front_bright_steps { 
    miColor tint; 
    miInteger steps;
};

DLLEXPORT
int front_bright_steps_version(void) { return(1); }

DLLEXPORT
miBoolean front_bright_steps ( 
    miColor *result, miState *state, struct front_bright_steps *params  )
{
    miColor *tint = mi_eval_color(&params->tint);
    miScalar scale = 
        miaux_quantize(-state->dot_nd, *mi_eval_integer(&params->steps));
    result->r = tint->r * scale;
    result->g = tint->g * scale;
    result->b = tint->b * scale;
    result->a = 1.0;
    return miTRUE;
}
