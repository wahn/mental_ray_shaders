#include "shader.h"
#include "geoshader.h"
#include "miaux.h"

miBoolean hair_geo_datafile_callback(miTag tag, void *ptr);

typedef struct {
    miTag name;
    miScalar radius;
    miTag particle_filename;
} hair_geo_datafile_t;

DLLEXPORT
int hair_geo_datafile_version(void) { return 1; }

void hair_geo_datafile_bbox(miObject *obj, void* params)
{
   hair_geo_datafile_t *hairdata = (hair_geo_datafile_t*)params;
   char* particle_filename; 
   particle_filename = 
       miaux_tag_to_string(hairdata->particle_filename, NULL);
   if (particle_filename == NULL)
       mi_fatal("Particle filename required for hair_geo_datafile.");
   miaux_hair_data_file_bounding_box(
       particle_filename,
       &obj->bbox_min.x, &obj->bbox_min.y, &obj->bbox_min.z,
       &obj->bbox_max.x, &obj->bbox_max.y, &obj->bbox_max.z);
}

DLLEXPORT
miBoolean hair_geo_datafile (
    miTag *result, miState *state, hair_geo_datafile_t *params  )
{
    hair_geo_datafile_t *hairdata = 
        (hair_geo_datafile_t*)mi_mem_allocate(sizeof(hair_geo_datafile_t));
    hairdata->radius = *mi_eval_scalar(&params->radius);
    hairdata->particle_filename = *mi_eval_tag(&params->particle_filename);

    miaux_define_hair_object(
        hairdata->name, hair_geo_datafile_bbox, hairdata, result, 
        hair_geo_datafile_callback);

    return miTRUE;
}

miBoolean hair_geo_datafile_callback(miTag tag, void *ptr)
{
    miHair_list *hair;
    hair_geo_datafile_t *hairdata = (hair_geo_datafile_t *)ptr;

    mi_api_incremental(miTRUE);
    miaux_define_hair_object(
        hairdata->name, hair_geo_datafile_bbox, hairdata, NULL, NULL);
    hair = mi_api_hair_begin();

    /* WAS: hair->approx = 1; */
    miAPPROX_DEFAULT(hair->approx);
    hair->approx.cnst[miCNST_UPARAM] = 1;
    hair->degree = 1;

    miaux_read_hair_data_file(
        miaux_tag_to_string(hairdata->particle_filename, NULL),
        hairdata->radius);
    mi_api_hair_end();
    mi_api_object_end();
    return miTRUE;
}
