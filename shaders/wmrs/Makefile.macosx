MR	           = /usr/local/mental_ray
CC	           = gcc
CPP	           = g++
STRICT         = -ansi -pedantic -Wall
CFLAGS	       = ${STRICT} -I/usr/include -I$(MR)/common/include \
-O3 -fPIC -dynamic -fno-common
COMMON_LDFLAGS = -dynamic -flat_namespace -undefined suppress
C_LDFLAGS      = -dynamiclib ${COMMON_LDFLAGS}
C++_LIBDIR     = /usr/lib
C++_LDFLAGS    = -L${C++_LIBDIR} ${COMMON_LDFLAGS} -lstdc++
NB             = newblocks
WMRS	         = wmrs

%.dylib : %.so
	ln -s $*.so $*.dylib

%.so : %.c miaux.o
	$(CC) $(CFLAGS) -c $*.c
	$(CPP) ${C_LDFLAGS} -o $*.so miaux.o $*.o

%.so : %.cpp miaux.o
	$(CPP) $(CFLAGS) -c $*.cpp
	libtool ${C++_LDFLAGS} -o $*.so miaux.o $*.o

%.o : %.c
	$(CC) $(CFLAGS) -c $*.c

%.o : %.cpp
	$(CPP) $(CFLAGS) -c $*.cpp

all: \
	add_colors.so \
	add_colors.dylib \
	ambient_occlusion.so \
	ambient_occlusion.dylib \
	ambient_occlusion_cutoff.so \
	ambient_occlusion_cutoff.dylib \
	annotate.so \
	annotate.dylib \
	average_radiance.so \
	average_radiance.dylib \
	average_radiance_options.so \
	average_radiance_options.dylib \
	blinn.so \
	blinn.dylib \
	bump_ripple.so \
	bump_ripple.dylib \
	bump_texture.so \
	bump_texture.dylib \
	bump_wave.so \
	bump_wave.dylib \
	bump_wave_uv.so \
	bump_wave_uv.dylib \
	c_contour.so \
	c_contour.dylib \
	c_contrast.so \
	c_contrast.dylib \
	c_distance_contrast.so \
	c_distance_contrast.dylib \
	c_distance_store.so \
	c_distance_store.dylib \
	c_output.so \
	c_output.dylib \
	c_store.so \
	c_store.dylib \
	c_tessellate.so \
	c_tessellate.dylib \
	c_toon.so \
	c_toon.dylib \
	channel_ramp.so \
	channel_ramp.dylib \
	chrome_ramp.so \
	chrome_ramp.dylib \
	color_ramp.so \
	color_ramp.dylib \
	cook_torrance.so \
	cook_torrance.dylib \
	density_volume.so \
	density_volume.dylib \
	depth_fade.so \
	depth_fade.dylib \
	depth_fade_tint.so \
	depth_fade_tint.dylib \
	depth_of_field.so \
	depth_of_field.dylib \
	displace_ripple.so \
	displace_ripple.dylib \
	displace_texture.so \
	displace_texture.dylib \
	displace_wave.so \
	displace_wave.dylib \
	displace_wave_uv.so \
	displace_wave_uv.dylib \
	equirectangular.so \
	equirectangular.dylib \
	fisheye.so \
	fisheye.dylib \
	fog.so \
	fog.dylib \
	framebuffer_put.so \
	framebuffer_put.dylib \
	front_bright.so \
	front_bright.dylib \
	front_bright_steps.so \
	front_bright_steps.dylib \
	global_lambert.so \
	global_lambert.dylib \
	glossy_reflection.so \
	glossy_reflection.dylib \
	glossy_reflection_sample.so \
	glossy_reflection_sample.dylib \
	glossy_reflection_sample_varying.so \
	glossy_reflection_sample_varying.dylib \
	glossy_refraction.so \
	glossy_refraction.dylib \
	glossy_refraction_sample.so \
	glossy_refraction_sample.dylib \
	glossy_refraction_sample_varying.so \
	glossy_refraction_sample_varying.dylib \
	ground_fog.so \
	ground_fog.dylib \
	ground_fog_layers.so \
	ground_fog_layers.dylib \
	hair_color_bary.so \
	hair_color_bary.dylib \
	hair_color_fire.so \
	hair_color_fire.dylib \
	hair_color_light.so \
	hair_color_light.dylib \
	hair_color_texture.so \
	hair_color_texture.dylib \
	hair_geo_2v.so \
	hair_geo_2v.dylib \
	hair_geo_4v.so \
	hair_geo_4v.dylib \
	hair_geo_4v_texture.so \
	hair_geo_4v_texture.dylib \
	hair_geo_curl.so \
	hair_geo_curl.dylib \
	hair_geo_datafile.so \
	hair_geo_datafile.dylib \
	hair_geo_row.so \
	hair_geo_row.dylib \
	illuminated_volume.so \
	illuminated_volume.dylib \
	instanced_object_file.so \
	instanced_object_file.dylib \
	lambert.so \
	lambert.dylib \
	lambert_steps.so \
	lambert_steps.dylib \
	letterbox.so \
	letterbox.dylib \
	make_indexed_framebuffers.so \
	make_indexed_framebuffers.dylib \
	miaux.o \
	median_filter.so \
	median_filter.dylib \
	mock_specular.so \
	mock_specular.dylib \
	named_framebuffer_put.so \
	named_framebuffer_put.dylib \
	negate.so \
	negate.dylib \
	newblocks.so \
	newblocks.dylib \
	normals_as_colors.so \
	normals_as_colors.dylib \
	object_file.so \
	object_file.dylib \
	one_color.so \
	one_color.dylib \
	op_mix_ccc.so \
	op_mix_ccc.dylib \
	op_mul_cc.so \
	op_mul_cc.dylib \
	op_pow_cs.so \
	op_pow_cs.dylib \
	parameter_volume.so \
	parameter_volume.dylib \
	phong.so \
	phong.dylib \
	phong_framebuffer.so \
	phong_framebuffer.dylib \
	point_light.so \
	point_light.dylib \
	point_light_falloff.so \
	point_light_falloff.dylib \
	point_light_shadow.so \
	point_light_shadow.dylib \
	radial_falloff.so \
	radial_falloff.dylib \
	rim_bright.so \
	rim_bright.dylib \
	scaled_color.so \
	scaled_color.dylib \
	shadow_breakpoint.so \
	shadow_breakpoint.dylib \
	shadow_breakpoint_scale.so \
	shadow_breakpoint_scale.dylib \
	shadow_color.so \
	shadow_color.dylib \
	shadow_continuous.so \
	shadow_continuous.dylib \
	shadow_default.so \
	shadow_default.dylib \
	shadowpass.so \
	shadowpass.dylib \
	show_barycentric.so \
	show_barycentric.dylib \
	show_uv.so \
	show_uv.dylib \
	show_uv_steps.so \
	show_uv_steps.dylib \
	sinusoid_soft_spotlight.so \
	sinusoid_soft_spotlight.dylib \
	soft_spotlight.so \
	soft_spotlight.dylib \
	soft_spotlight_falloff.so \
	soft_spotlight_falloff.dylib \
	specular_reflection.so \
	specular_reflection.dylib \
	specular_refraction.so \
	specular_refraction.dylib \
	specular_refraction_simple.so \
	specular_refraction_simple.dylib \
	spherical_density.so \
	spherical_density.dylib \
	spotlight.so \
	spotlight.dylib \
	square.so \
	square.dylib \
	store_diffuse_photon.so \
	store_diffuse_photon.dylib \
	streak.so \
	streak.dylib \
	summed_noise_color.so \
	summed_noise_color.dylib \
	summed_noise_scalar.so \
	summed_noise_scalar.dylib \
	transmit_specular_photon.so \
	transmit_specular_photon.dylib \
	transparent.so \
	transparent.dylib \
	transparent_shadow.so \
	transparent_shadow.dylib \
	triangles.so \
	triangles.dylib \
	texture_uv.so \
	texture_uv.dylib \
	texture_uv_simple.so \
	texture_uv_simple.dylib \
	threshold_volume.so \
	threshold_volume.dylib \
	vertex_color.so \
	vertex_color.dylib \
	voxel_density.so \
	voxel_density.dylib \
	ward.so \
	ward.dylib \
	$(WMRS).so \
	$(WMRS).dylib

newblocks.so: $(NB).cpp matrix.o mrpoly.o
	$(CPP) $(CFLAGS) -c $(NB).cpp
	libtool ${C++_LDFLAGS} -o $(NB).so matrix.o mrpoly.o $(NB).o

# update whenever you need another shader being supported in the library
$(WMRS).so: \
	ambient_occlusion.o \
	depth_fade.o \
	front_bright.o \
	normals_as_colors.o \
	one_color.o \
	point_light_falloff.o \
	shadow_default.o \
	soft_spotlight_falloff.o \
	texture_uv.o \
  transparent_shadow.o \
	show_uv.o
	libtool ${C++_LDFLAGS} -o $(WMRS).so \
ambient_occlusion.o \
depth_fade.o \
front_bright.o \
normals_as_colors.o \
one_color.o \
point_light_falloff.o \
shadow_default.o \
soft_spotlight_falloff.o \
texture_uv.o \
transparent_shadow.o \
show_uv.o
# don't forget to update the $(WMRS).mi file (e.g. cat new_shader.mi >> wmrs.mi)

clean:
	-rm -f *.o *~

clobber: clean
	-rm -f *.so *.dylib
