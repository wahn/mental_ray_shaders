#include "shader.h"

DLLEXPORT
int normals_as_colors_version(void) { return(1); }

DLLEXPORT
miBoolean normals_as_colors ( 
    miColor *result, miState *state, void *params  )
{
    miVector normal;
    mi_vector_to_object(state, &normal, &state->normal);
    mi_vector_normalize(&normal);

    result->r = normal.x * 0.5 + 0.5;
    result->g = normal.y * 0.5 + 0.5;
    result->b = normal.z * 0.5 + 0.5;
    result->a = 1.0;

    return miTRUE;
}
