#include "shader.h"
#include "miaux.h"

struct fog {
    miScalar full_fade_distance;
    miColor fog_color;
};

DLLEXPORT
int fog_version(void) { return 1; }

DLLEXPORT
miBoolean fog (
     miColor *result, miState *state, struct fog *params  )
{
    miScalar full_fade_distance = *mi_eval_scalar(&params->full_fade_distance);
    miColor *fog_color = mi_eval_color(&params->fog_color);

    if (state->dist > full_fade_distance || state->dist == 0.0)
        *result = *fog_color;
    else 
        miaux_blend_colors(
            result, fog_color, result, state->dist/full_fade_distance);

    return miTRUE;
}
