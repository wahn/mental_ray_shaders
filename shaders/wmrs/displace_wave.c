#include "shader.h"
#include "miaux.h"

struct displace_wave { 
    miScalar frequency;
    miScalar amplitude;
};

DLLEXPORT
int displace_wave_version(void) { return(1); }

DLLEXPORT
miBoolean displace_wave ( 
    miScalar *result, miState *state, struct displace_wave *params  )
{
    *result += miaux_sinusoid(state->point.x,
                              *mi_eval_scalar(&params->frequency),
                              *mi_eval_scalar(&params->amplitude));
    return miTRUE;
}

