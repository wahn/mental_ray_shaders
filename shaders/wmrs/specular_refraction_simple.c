#include "shader.h"
#include "miaux.h"

struct specular_refraction_simple {
    miScalar index_of_refraction;
};

DLLEXPORT
int specular_refraction_simple_version(void) { return 1; }

DLLEXPORT
miBoolean specular_refraction_simple (
    miColor *result, miState *state, struct specular_refraction_simple *params  )
{
    miScalar instance_ior = *mi_eval_scalar(&params->index_of_refraction);
    miScalar vacuum_ior = 1.0, incoming_ior, outgoing_ior;
    miVector direction;
    if (miaux_ray_is_entering_material(state)) {
        incoming_ior = vacuum_ior;
        outgoing_ior = instance_ior;
    } else {
        incoming_ior = instance_ior;
        outgoing_ior = vacuum_ior;
    }
    if (mi_refraction_dir(&direction, state, incoming_ior, outgoing_ior))
        mi_trace_refraction(result, state, &direction);
    else {
        mi_reflection_dir(&direction, state);
        if (!mi_trace_reflection(result, state, &direction))
            mi_trace_environment(result, state, &direction);
    }
    return miTRUE;
}
