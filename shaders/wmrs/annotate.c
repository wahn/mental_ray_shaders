#include "shader.h"
#include "miaux.h"

struct annotate {
    miTag text;
    miTag fontimage_filename;
    miInteger x;
    miInteger y;
    miColor color;
};

DLLEXPORT
int annotate_version(void) { return 1; }

DLLEXPORT
miBoolean annotate (
    void *result, miState *state, struct annotate *params )
{
    if (params->text != miNULLTAG) {
        int x, y, t_x, t_y, t_width, t_height;
        float *text_image;
        miImg_image *fb;
        fontimage fimage;
        miColor *text_color = mi_eval_color(&params->color), pixel_color;
        char* text = miaux_tag_to_string(
            *mi_eval_tag(&params->text), NULL);
        int p_x = *mi_eval_integer(&params->x), 
            p_y = *mi_eval_integer(&params->y);
        char* fontimage_filename = 
            miaux_tag_to_string(*mi_eval_tag(&params->fontimage_filename), 
                                "Courier-Bold_24.fontimage");

        miaux_load_fontimage(&fimage, fontimage_filename);
        miaux_text_image(&text_image, &t_width, &t_height, &fimage, text);

        fb = mi_output_image_open(state, miRC_IMAGE_RGBA);
        for (y = p_y, t_y = 0; t_y < t_height; y++, t_y++) {
            if (mi_par_aborted()) {
                mi_progress("Abort");
                break;
            }
            for (x = p_x, t_x = 0; t_x < t_width; x++, t_x++) {
                float alpha = text_image[t_y * t_width + t_x];
                mi_img_get_color(fb, &pixel_color, x, y);
                miaux_alpha_blend(&pixel_color, text_color, alpha);
                mi_img_put_color(fb, &pixel_color, x, y);
            }
        }
        miaux_release_fontimage(&fimage);
        mi_output_image_close(state, miRC_IMAGE_RGBA);
    }
    return miTRUE;
}
