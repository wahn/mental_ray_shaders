#include "shader.h"
#include "miaux.h"

struct specular_refraction {
    miScalar index_of_refraction;
};

DLLEXPORT
int specular_refraction_version(void) { return 1; }

DLLEXPORT
miBoolean specular_refraction (
    miColor *result, miState *state, struct specular_refraction *params  )
{
    miVector direction;
    miScalar ior = *mi_eval_scalar(&params->index_of_refraction);
    miaux_set_state_refraction_indices(state, ior);

    if (mi_refraction_dir(&direction, state, state->ior_in, state->ior))
        mi_trace_refraction(result, state, &direction);
    else {
        mi_reflection_dir(&direction, state);
        if (!mi_trace_reflection(result, state, &direction))
            mi_trace_environment(result, state, &direction);
    }
    return miTRUE;
}
