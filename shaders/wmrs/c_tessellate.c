#include "shader.h"

/* Info struct */

typedef struct {
    int primitive_index;
} c_tessellate_info;

/* Store shader */

DLLEXPORT
int c_tessellate_store_version(void) { return 1; }

DLLEXPORT
miBoolean c_tessellate_store (
    void *info_pointer, 
    int *info_size, 
    miState *state, 
    miColor *color)
{
    c_tessellate_info *info = (c_tessellate_info*)info_pointer;
    int primitive_index;
    mi_query(miQ_PRI_INDEX, state, 0, &primitive_index);
    info->primitive_index = primitive_index;
    *info_size = sizeof(c_tessellate_info); 
    return miTRUE;
}

/* Contrast shader */

DLLEXPORT
int c_tessellate_contrast_version(void) { return 1; }

DLLEXPORT
miBoolean c_tessellate_contrast(
    c_tessellate_info *info1, 
    c_tessellate_info *info2, 
    int level, 
    miState *state, 
    void *params)
{
    if (info1 == NULL ||
        info2 == NULL ||
        info1->primitive_index != info2->primitive_index)
        return miTRUE;
    else
        return miFALSE;
}

/* Contour shader */

struct c_tessellate_contour {
    miColor color;
    miScalar width;
};

DLLEXPORT
int c_tessellate_contour_version(void)  { return 1; }

DLLEXPORT
miBoolean c_tessellate_contour(
    miContour_endpoint *result, 
    c_tessellate_info *info_near, 
    c_tessellate_info *info_far,
    miState *state, 
    struct c_tessellate_contour *params)
{
    result->color = *mi_eval_color(&params->color);
    result->width = *mi_eval_scalar(&params->width);
    return miTRUE;
}

