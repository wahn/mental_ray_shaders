void miaux_set_vector(miVector *v, double x, double y, double z)
{
    v->x = x;
    v->y = y;
    v->z = z;
}

miScalar miaux_color_channel_average(miColor *c)
{
    return (c->r + c->g + c->b) / 3.0;
}