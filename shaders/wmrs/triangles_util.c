char* miaux_tag_to_string(miTag tag, char *default_value)
{
    char *result = default_value;
    if (tag != 0) {
	result = (char*)mi_db_access(tag);
	mi_db_unpin(tag);
    }
    return result;
}

void miaux_add_random_triangle(int index, float edge_length_max,
			       miVector *bbox_min, miVector *bbox_max)
{
    int vertex_index;
    float offset_max = edge_length_max / 2.0;
    float offset_min = -offset_max;
    float x, y, z;

    miaux_random_point_in_unit_sphere(&x, &y, &z);
    x = miaux_fit(x, -.5, .5, bbox_min->x, bbox_max->x);
    y = miaux_fit(y, -.5, .5, bbox_min->y, bbox_max->y);
    z = miaux_fit(z, -.5, .5, bbox_min->z, bbox_max->z);

    miaux_add_vertex(index, x, y, z);
    miaux_add_vertex(index + 1, 
		     x + miaux_random_range(offset_min, offset_max),
		     y + miaux_random_range(offset_min, offset_max),
		     z + miaux_random_range(offset_min, offset_max));
    miaux_add_vertex(index + 2, 
		     x + miaux_random_range(offset_min, offset_max),
		     y + miaux_random_range(offset_min, offset_max),
		     z + miaux_random_range(offset_min, offset_max));

    mi_api_poly_begin_tag(1, 0);
    for (vertex_index = 0; vertex_index < 3; vertex_index++)
	mi_api_poly_index_add(index + vertex_index);
    mi_api_poly_end();
}

void miaux_random_point_in_unit_sphere(float *x, float *y, float *z)
{
    do {
	*x = miaux_random_range(-.5, .5);
	*y = miaux_random_range(-.5, .5);
	*z = miaux_random_range(-.5, .5);
    } while (sqrt((*x * *x) +(*y * *y) + (*z * *z)) >= 0.5);
}

result->r += miaux_random_range(-red_variance, red_variance);
    result->r = miaux_clamp(result->r, 0.0, 1.0);
    result->g += miaux_random_range(-green_variance, green_variance);
    result->g = miaux_clamp(result->g, 0.0, 1.0);
    result->b += miaux_random_range(-blue_variance, blue_variance);
    result->b = miaux_clamp(result->b, 0.0, 1.0);
}

double miaux_fit(
    double v, double oldmin, double oldmax, double newmin, double newmax)    
{
    return newmin + ((v - oldmin) / (oldmax - oldmin)) * (newmax - newmin);
}

void miaux_add_vertex(int index, miScalar x, miScalar y, miScalar z)
{
    miVector v;
    v.x = x; v.y = y; v.z = z;
    mi_api_vector_xyz_add(&v);
    mi_api_vertex_add(index);
}