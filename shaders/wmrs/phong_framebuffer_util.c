void miaux_light_array(miTag **lights, int *light_count, miState *state,
		       int *offset_param, int *count_param, miTag *lights_param)
{
    int array_offset = *mi_eval_integer(offset_param);
    *light_count = *mi_eval_integer(count_param);
    *lights = mi_eval_tag(lights_param) + array_offset;
}

void miaux_set_channels(miColor *c, miScalar new_value)
{
    c->r = c->g = c->b = c->a = new_value;
}

void miaux_add_diffuse_component(
    miColor *result, 
    miScalar light_and_surface_cosine, 
    miColor *diffuse, miColor *light_color)
{
    result->r += light_and_surface_cosine * diffuse->r * light_color->r;
    result->g += light_and_surface_cosine * diffuse->g * light_color->g;
    result->b += light_and_surface_cosine * diffuse->b * light_color->b;
}

void miaux_add_phong_specular_component(
    miColor *result, miState *state, miScalar exponent,
    miVector *direction_toward_light, 
    miColor *specular, miColor *light_color)
{
    miScalar specular_amount =
	mi_phong_specular(exponent, state, direction_toward_light);
    if (specular_amount > 0.0) {
	result->r += specular_amount * specular->r * light_color->r;
	result->g += specular_amount * specular->g * light_color->g;
	result->b += specular_amount * specular->b * light_color->b;
    }
}

void miaux_add_scaled_color(miColor *result, miColor *color, miScalar scale)
{
    result->r += color->r * scale;
    result->g += color->g * scale;
    result->b += color->b * scale;
}

void miaux_add_color(miColor *result, miColor *c)
{
    result->r += c->r;
    result->g += c->g;
    result->b += c->b;
    result->a += c->a;
}