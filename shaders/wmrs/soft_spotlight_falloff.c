#include "shader.h"
#include "miaux.h"

struct soft_spotlight_falloff {
    miColor light_color;
    miScalar inner_spread;
};

DLLEXPORT
int soft_spotlight_falloff_version(void) { return 1; }

DLLEXPORT
miBoolean soft_spotlight_falloff (
    miColor *result, miState *state, struct soft_spotlight_falloff *params )
{
    miScalar inner_spread, attenuation;
    miTag light_tag = miaux_current_light_tag(state);
    miScalar exponent;
    miScalar offset_spread = miaux_offset_spread_from_light(state, light_tag);
    miScalar light_spread = miaux_light_spread(state, light_tag);

    if (offset_spread < light_spread)
        return miFALSE;

    *result = *mi_eval_color(&params->light_color);
    mi_query(miQ_INST_ITEM, state, state->light_instance, &light_tag);
    mi_query(miQ_LIGHT_EXPONENT, state, light_tag, &exponent);
    miaux_scale_color(result, 1.0 / pow(state->dist, exponent));
    inner_spread = *mi_eval_scalar(&params->inner_spread);

    if (offset_spread < inner_spread) {
        attenuation = miaux_fit(offset_spread, inner_spread, light_spread, 1, 0);
        miaux_scale_color(result, attenuation);
    }
    return mi_trace_shadow(result, state);
}
