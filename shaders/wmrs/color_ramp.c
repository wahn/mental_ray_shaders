#include "shader.h"
#include "miaux.h"

#define RAMPSIZE 1024

struct color_ramp {
    int      i_colors; 
    int      n_colors; 
    miColor colors[1];
};

DLLEXPORT
int color_ramp_version(void) { return 1; }

DLLEXPORT
miBoolean color_ramp_init(
    miState *state, struct color_ramp *params,
    miBoolean *instance_init_required)
{
    if (params == NULL) {  /* Main shader init (not an instance) */
        *instance_init_required = miTRUE;
    } else {               /* Instance initialization */
        int n_colors = *mi_eval_integer(&params->n_colors);
        miColor *colors = 
            mi_eval_color(params->colors) + *mi_eval_integer(&params->i_colors);
        miColor *ramp = 
            miaux_user_memory_pointer(state, sizeof(miColor) * RAMPSIZE);

        miaux_piecewise_color_sinusoid(ramp, RAMPSIZE, n_colors, colors);
    }
    return miTRUE;
}

DLLEXPORT
miBoolean color_ramp_exit(miState *state, void *params)
{
    return miaux_release_user_memory("color_ramp", state, params);
}

DLLEXPORT
miBoolean color_ramp (
    miColor *result, miState *state, struct color_ramp *params  )
{
    miColor *ramp = miaux_user_memory_pointer(state, 0);
    miaux_interpolated_color_lookup(
        result, ramp, RAMPSIZE, miaux_altitude(state));
    return miTRUE;
}
