void miaux_to_camera_space(
    miState *state, miVector *origin, miVector *direction)
{
    mi_point_to_camera(state, origin, &state->org);
    mi_vector_to_camera(state, direction, &state->dir);
}

void miaux_z_plane_intersect(
    miVector *result, miVector *origin, miVector *direction, miScalar z_plane)
{
    miScalar z_delta = (z_plane - origin->z) / direction->z;
    result->x = origin->x + z_delta * direction->x;
    result->y = origin->y + z_delta * direction->y;
    result->z = z_plane;
}

void miaux_sample_point_within_radius(
    miVector *result, miVector *center, float x, float y, float max_radius)
{
    float x_offset, y_offset;
    miaux_square_to_circle(&x_offset, &y_offset, x, y, max_radius);
    result->x = center->x + x_offset;
    result->y = center->y + y_offset;
    result->z = center->z;
}

void miaux_square_to_circle(
    float *result_x, float *result_y, float x, float y, float max_radius)
{
    float angle = M_PI * 2 * x;
    float radius = max_radius * sqrt(y);
    *result_x = radius * cos(angle);
    *result_y = radius * sin(angle);
}

void miaux_from_camera_space(
        miState *state, miVector *origin, miVector *direction)
{
    mi_point_from_camera(state, origin, origin);
    mi_vector_from_camera(state, direction, direction);
}

void miaux_add_color(miColor *result, miColor *c)
{
    result->r += c->r;
    result->g += c->g;
    result->b += c->b;
    result->a += c->a;
}

void miaux_divide_colors(miColor *result, miColor *x, miColor *y)
{
    result->r = y->r == 0.0 ? 1.0 -: x->r / y->r;
    result->g = y->g == 0.0 ? 0.0 -: x->g / y->g;
    result->b = y->b == 0.0 ? 1.0 -: x->b / y->b;
}