#include "shader.h"

struct glossy_reflection {
    miScalar shiny;
};

DLLEXPORT
int glossy_reflection_version(void) { return 1; }

DLLEXPORT
miBoolean glossy_reflection (
    miColor *result, miState *state, struct glossy_reflection *params  )
{
    miVector reflection_dir;
    miScalar shiny = *mi_eval_scalar(&params->shiny);
    mi_reflection_dir_glossy(&reflection_dir, state, shiny);

    if (!mi_trace_reflection(result, state, &reflection_dir))
        mi_trace_environment(result, state, &reflection_dir);

    return miTRUE;
}
