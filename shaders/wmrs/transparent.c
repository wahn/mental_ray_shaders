#include "shader.h"
#include "miaux.h"

struct transparent { 
    miColor color; 
    miColor transparency;
};

DLLEXPORT
int transparent_version(void) { return(1); }

DLLEXPORT
miBoolean transparent (
    miColor *result, miState *state, struct transparent *params  )
{
    miColor *transparency = mi_eval_color(&params->transparency);
    if (transparency->r == 0.0 && transparency->g == 0.0 &&
        transparency->b == 0.0 && transparency->a == 0.0)
        *result = *mi_eval_color(&params->color);
    else {
        mi_trace_transparent (result, state );
        if (!(transparency->r == 1.0 && transparency->g == 1.0 && 
              transparency->b == 1.0 && transparency->a == 1.0)) {
            miColor *color = mi_eval_color(&params->color);
            miColor opacity;
            opacity.r = 1.0 - transparency->r;
            opacity.g = 1.0 - transparency->g;
            opacity.b = 1.0 - transparency->b;
            opacity.a = 1.0 - transparency->a;
            mi_opacity_set(state, &opacity);
            result->r = result->r * transparency->r + color->r * opacity.r;
            result->g = result->g * transparency->g + color->g * opacity.g;
            result->b = result->b * transparency->b + color->b * opacity.b;
        }
    }
    return miTRUE;
}
