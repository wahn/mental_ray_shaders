#include "shader.h"
#include "miaux.h"

struct ground_fog {
    miColor fog_color;
    miColor fog_density;
    miScalar fade_start;
    miScalar fade_end;
    miScalar unit_density;
    miScalar march_increment;
};

DLLEXPORT
int ground_fog_version(void) { return 1; }

DLLEXPORT
miBoolean ground_fog (
    miColor *result, miState *state, struct ground_fog *params  )
{
    miScalar fade_start, fade_end, fog_density, distance, density_factor,
        march_increment, unit_density, accumulated_density;
    miVector march_point;
    
    if (state->dist == 0)
        return miTRUE;

    fade_start = *mi_eval_scalar(&params->fade_start);
    fade_end = *mi_eval_scalar(&params->fade_end);
    fog_density = *mi_eval_scalar(&params->fog_density); 
    march_increment = *mi_eval_scalar(&params->march_increment);
    unit_density = *mi_eval_scalar(&params->unit_density);
    accumulated_density = 0.0;

    for (distance = 0; distance < state->dist; distance += march_increment) {
        miaux_world_space_march_point(&march_point, state, distance);
        density_factor = miaux_fit_clamp(
            march_point.y, fade_start, fade_end, fog_density, 0.0);
        accumulated_density += density_factor * march_increment * unit_density;
        if (accumulated_density > 1.0) {
            *result = *mi_eval_color(&params->fog_color);
            return miTRUE;
        }
    }
    if (accumulated_density > 0.0)
        miaux_blend_colors(result, result, mi_eval_color(&params->fog_color),
                           1.0 - accumulated_density);
    return miTRUE;
}
