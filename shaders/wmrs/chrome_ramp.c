#include "shader.h"
#include "miaux.h"

#define RAMPSIZE 1024

typedef struct {
    int r_size, g_size, b_size;
    miScalar r[RAMPSIZE];
    miScalar g[RAMPSIZE];
    miScalar b[RAMPSIZE];
} channel_ramp_table;

static channel_ramp_table *ramp;

DLLEXPORT
miBoolean chrome_ramp_init(
    miState *state, void *params, miBoolean *instance_init_required)
{
    int key_count = 4;

    miScalar key_positions[] = {0,    0.49, 0.51, 1.0};
    miScalar red[]           = {0.95, 0.68, 0.95, 0.5};
    miScalar green[]         = {0.85, 0.6,  0.95, 0.4};
    miScalar blue[]          = {0.75, 0.48, 1.0,  0.9};

    ramp = (channel_ramp_table*)mi_mem_allocate(sizeof(channel_ramp_table));

    miaux_piecewise_sinusoid(ramp->r, RAMPSIZE, key_count, key_positions, red);
    miaux_piecewise_sinusoid(ramp->g, RAMPSIZE, key_count, key_positions, green);
    miaux_piecewise_sinusoid(ramp->b, RAMPSIZE, key_count, key_positions, blue);

    return miTRUE;
}

DLLEXPORT
miBoolean chrome_ramp_exit(miState *state, void *params)
{
    mi_mem_release(ramp);
    return miTRUE;
}

DLLEXPORT
int chrome_ramp_version(void) { return 1; }

DLLEXPORT
miBoolean chrome_ramp (
    miColor *result, miState *state, void *params  )
{
    miScalar altitude = miaux_altitude(state);
    result->r = miaux_interpolated_lookup(ramp->r, RAMPSIZE, altitude);
    result->g = miaux_interpolated_lookup(ramp->g, RAMPSIZE, altitude);
    result->b = miaux_interpolated_lookup(ramp->b, RAMPSIZE, altitude);
    return miTRUE;
}
