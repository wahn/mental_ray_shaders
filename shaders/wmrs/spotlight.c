#include "shader.h"
#include "miaux.h"

struct spotlight {
    miColor light_color;
};

DLLEXPORT
int spotlight_version(void) { return 1; }

DLLEXPORT
miBoolean spotlight (
    miColor *result, miState *state, struct spotlight *params  )
{
    miTag light_tag = miaux_current_light_tag(state);

    if (miaux_offset_spread_from_light(state, light_tag)
        > miaux_light_spread(state, light_tag)) {
        *result = *mi_eval_color(&params->light_color);
        return mi_trace_shadow(result, state);
    }
    else
        return miFALSE;
}
