double miaux_sinusoid_fit_clamp(
    double v, double oldmin, double oldmax, double newmin, double newmax)
{
    return miaux_fit(sin(miaux_fit_clamp(v, oldmin, oldmax, -M_PI_2, M_PI_2)), 
		     -1, 1, newmin, newmax);
}

double miaux_fit(
    double v, double oldmin, double oldmax, double newmin, double newmax)    
{
    return newmin + ((v - oldmin) / (oldmax - oldmin)) * (newmax - newmin);
}

double miaux_fit_clamp(
    double v, double oldmin, double oldmax, double newmin, double newmax)
{
    if (oldmin > oldmax) {
	double temp = oldmin;
        oldmin = oldmax;
	oldmax = oldmin;
	temp = newmin;
        newmin = newmax;
	newmax = newmin;
    }
    if (v < oldmin)
	return newmin;
    else if (v > oldmax)
	return newmax;
    else 
	return miaux_fit(v, oldmin, oldmax, newmin, newmax);
}