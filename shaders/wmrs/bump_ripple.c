#include "shader.h"
#include "miaux.h"

struct bump_ripple { 
    miVector center;
    miScalar frequency;
    miScalar amplitude;
    miScalar nearby;
};

DLLEXPORT
int bump_ripple_version(void) { return(1); }

DLLEXPORT
miBoolean bump_ripple ( 
    miColor *result, miState *state, struct bump_ripple *params  )
{
    double ripple_here, ripple_over, ripple_up,
        change_going_over, change_going_up;
    miVector here, over, up;

    miVector center = *mi_eval_vector(&params->center);
    miScalar frequency = *mi_eval_scalar(&params->frequency);
    miScalar amplitude = *mi_eval_scalar(&params->amplitude);
    miScalar nearby    = *mi_eval_scalar(&params->nearby);
    
    miVector bump_basis_u = state->bump_x_list[0];
    miVector bump_basis_v = state->bump_y_list[0];

    here = state->tex_list[0];
    miaux_set_vector(&over, here.x + nearby, here.y, here.z);
    miaux_set_vector(&up, here.x, here.y + nearby, here.z);

    ripple_here = miaux_sinusoid(mi_vector_dist(&center, &here),
                                 frequency, amplitude);
    ripple_over =  miaux_sinusoid(mi_vector_dist(&center, &over),
                                  frequency, amplitude);
    ripple_up = miaux_sinusoid(mi_vector_dist(&center, &up),
                               frequency, amplitude);

    change_going_over = ripple_over - ripple_here;
    change_going_up = ripple_over - ripple_up;

    mi_vector_mul(&bump_basis_u, -change_going_over);
    mi_vector_mul(&bump_basis_v, -change_going_up);
    
    mi_vector_to_object(state, &state->normal, &state->normal);
    mi_vector_add(&state->normal, &state->normal, &bump_basis_u);
    mi_vector_add(&state->normal, &state->normal, &bump_basis_v);
    mi_vector_normalize(&state->normal);
    mi_vector_from_object(state, &state->normal, &state->normal);

    return miTRUE;
}

