void miaux_copy_color(miColor *result, miColor *color)
{
    result->r = color->r;
    result->g = color->g;
    result->b = color->b;
    result->a = color->a;
}

double miaux_shadow_breakpoint (
    double color, double transparency, double breakpoint )
{
    if (transparency < breakpoint)
	return miaux_fit(transparency, 0, breakpoint, 0, color);
    else
	return miaux_fit(transparency, breakpoint, 1, color, 1);
}

double miaux_fit(
    double v, double oldmin, double oldmax, double newmin, double newmax)    
{
    return newmin + ((v - oldmin) / (oldmax - oldmin)) * (newmax - newmin);
}

miBoolean miaux_all_channels_equal(miColor *c, miScalar v)
{
    if (c->r == v && c->g == v && c->b == v && c->a == v)
	return miTRUE;
    else
	return miFALSE;
}

void miaux_set_channels(miColor *c, miScalar new_value)
{
    c->r = c->g = c->b = c->a = new_value;
}

void miaux_blend_channels(miColor *result,
			  miColor *blend_color, miColor *blend_fraction)
{
    result->r = miaux_blend(result->r, blend_color->r, blend_fraction->r);
    result->g = miaux_blend(result->g, blend_color->g, blend_fraction->g);
    result->b = miaux_blend(result->b, blend_color->b, blend_fraction->b);
}

double miaux_blend(miScalar a, miScalar b, miScalar factor)
{
    return a * factor + b * (1.0 - factor);
}