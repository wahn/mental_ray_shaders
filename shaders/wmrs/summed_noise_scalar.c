#include "shader.h"
#include "miaux.h"

struct summed_noise_scalar { 
    miScalar point_scale;
    miScalar magnitude;
    miScalar octave_scaling;
    miScalar summing_weight;
    miInteger number_of_octaves;
};

DLLEXPORT
int summed_noise_scalar_version(void) { return(1); }

DLLEXPORT
miBoolean summed_noise_scalar ( 
    miScalar *result, miState *state, struct summed_noise_scalar *params  )
{
    miVector object_point;
    miScalar magnitude = *mi_eval_scalar(&params->magnitude), noise_sum;

    mi_point_to_object(state, &object_point, &state->point);
    mi_vector_mul(&object_point, *mi_eval_scalar(&params->point_scale));
    
    noise_sum = miaux_summed_noise(&object_point, 
                                   *mi_eval_scalar(&params->summing_weight),
                                   *mi_eval_scalar(&params->octave_scaling),
                                   *mi_eval_integer(&params->number_of_octaves));

    *result += miaux_fit(noise_sum, 0.0, 1.0, -magnitude, magnitude);

    return miTRUE;
}

