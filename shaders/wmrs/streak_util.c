result->r += miaux_random_range(-red_variance, red_variance);
    result->r = miaux_clamp(result->r, 0.0, 1.0);
    result->g += miaux_random_range(-green_variance, green_variance);
    result->g = miaux_clamp(result->g, 0.0, 1.0);
    result->b += miaux_random_range(-blue_variance, blue_variance);
    result->b = miaux_clamp(result->b, 0.0, 1.0);
}

double miaux_fit(
    double v, double oldmin, double oldmax, double newmin, double newmax)    
{
    return newmin + ((v - oldmin) / (oldmax - oldmin)) * (newmax - newmin);
}