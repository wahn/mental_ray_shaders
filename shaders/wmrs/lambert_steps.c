#include "shader.h"
#include "miaux.h"

struct lambert_steps {
    miColor ambient;
    miColor diffuse;
    miInteger steps;
    int     i_light;
    int     n_light;
    miTag   light[1];
};

DLLEXPORT
int lambert_steps_version(void) { return 1; }

DLLEXPORT
miBoolean lambert_steps (
    miColor *result, miState *state, struct lambert_steps *params  )
{
    int i, light_count, light_sample_count;
    miColor sum, light_color;
    miScalar dot_nl;
    miTag *light;

    miColor *diffuse = mi_eval_color(&params->diffuse);
    miInteger steps = *mi_eval_integer(&params->steps);
    miaux_light_array(&light, &light_count, state,
                      &params->i_light, &params->n_light, params->light);
    *result = *mi_eval_color(&params->ambient);

    for (i = 0; i < light_count; i++, light++) {
        miaux_set_channels(&sum, 0);
        light_sample_count = 0;
        while (mi_sample_light(&light_color, NULL, &dot_nl,
                               state, *light, &light_sample_count)) {
            dot_nl = miaux_quantize(dot_nl, steps);
            miaux_add_diffuse_component(&sum, dot_nl, diffuse, &light_color);
        }
        if (light_sample_count)
            miaux_add_scaled_color(result, &sum, 1.0/light_sample_count);
    }
    return miTRUE;
}
