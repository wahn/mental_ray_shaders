#include "shader.h"
#include "miaux.h"

typedef struct {
    miTag instance;
    miVector normal;
    miColor color;
} c_toon_info;

int c_toon_store_version(void) { return 1; }

miBoolean c_toon_store (
    void *info_pointer, int *info_size, miState *state, miColor *color)
{
    c_toon_info *info = (c_toon_info*)info_pointer;

    info->instance = state->instance;
    info->normal = state->normal;
    info->color = *color;
    *info_size = sizeof(c_toon_info);

    return miTRUE;
}

struct c_toon_contrast {
    miScalar dot_threshold;
};

int c_toon_contrast_version(void) { return 1; }

miBoolean c_toon_contrast (
    c_toon_info *info1, c_toon_info *info2, int level, miState *state, 
    struct c_toon_contrast *params )
{
    if (info1 == NULL ||
        info2 == NULL ||
        info1->instance != info2->instance ||
        (mi_vector_dot(&info1->normal, &info2->normal) <
         *mi_eval_scalar(&params->dot_threshold)) ||
        info1->color.r != info2->color.r ||
        info1->color.g != info2->color.g ||
        info1->color.b != info2->color.b)
        return miTRUE;
    else
        return miFALSE;
}

struct c_toon_contour {
    miColor color;
    miScalar width;
};

int c_toon_contour_version(void)  { return 1; }

miBoolean c_toon_contour (
    miContour_endpoint *result, c_toon_info *info_near, c_toon_info *info_far,
    miState *state, struct c_toon_contour *params )
{
    result->color = *mi_eval_color(&params->color);
    result->width = *mi_eval_scalar(&params->width);
    return miTRUE;
}

