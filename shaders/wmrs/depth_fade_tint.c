#include "shader.h"

struct depth_fade_tint { 
    miScalar near;
    miColor near_color;
    miScalar far;
    miColor far_color;
};

DLLEXPORT
int depth_fade_tint_version(void) { return(1); }

DLLEXPORT
miBoolean depth_fade_tint ( 
    miColor *result, miState *state, struct depth_fade_tint *params  )
{
    miScalar near = *mi_eval_scalar(&params->near);
    miColor *near_color = mi_eval_color(&params->near_color);
    miScalar far = *mi_eval_scalar(&params->far);
    miColor *far_color = mi_eval_color(&params->far_color);
    miScalar zpos = state->point.z, factor;

    if (zpos > near)
        factor = 1.0;
    else if (zpos < far)
        factor = 0.0;
    else
        factor =  (zpos - far) / (near - far);

    result->r = near_color->r * factor + far_color->r * (1.0 - factor);
    result->g = near_color->g * factor + far_color->g * (1.0 - factor);
    result->b = near_color->b * factor + far_color->b * (1.0 - factor);

    return miTRUE;
}


