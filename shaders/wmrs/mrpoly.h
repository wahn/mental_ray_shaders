// Polygon construction toolkit for mental ray
//
// These C++ classes are used in "newblocks.cpp" For the simple
// objects developed as part of the book "Writing mental ray shaders"
// This is only an initial sketch of a possible direction for
// simplifying mental ray geometry shaders.  See the book's website for
// further developments:
//
//     http://www.writingshaders.com/
//
// No promises are made about the efficiency of this approach!


#ifndef __MRPOLY_H__
#define __MRPOLY_H__

#include <cmath>
#include <map>
#include <vector>
#include <set>
#include <iostream>
#include <sstream>
#include <fstream>
#include <algorithm>
#include <string>
#include <stdexcept>
#include <assert.h>
#include "shader.h"
#include "geoshader.h"
#include "matrix.h"

using namespace std;


// ----------------------------------------------------------------------
// Utilities

const char* get_string(miState *state, miTag *name_param, const char* default_value);
float fit(float v, float oldmin, float oldmax, float newmin, float newmax);


// ----------------------------------------------------------------------
// triple

class triple {
 public:
  static triple nil;
  static triple x_axis;
  static triple y_axis;
  float x, y, z;
 triple() : x(0), y(0), z(0) {}
 triple(float x, float y, float z=0) : x(x), y(y), z(z) {} 
 triple(const triple& t) :  x(t.x), y(t.y), z(t.z)  {}
  friend ostream& operator<< (ostream& os, const triple& v);
  triple& set(float px, float py, float pz=0.0) 
    { x = px; y = py; z = pz; return *this; }

  friend const bool operator<(const triple &lhs, const triple &rhs);

  bool operator==(const triple &v) const {
    return x == v.x && y == v.y && z == v.z;
  }

  bool operator!=(const triple &v) const {
    return !(*this == v);
  }

  triple& operator=(const triple &rhs) {
    if (this != &rhs) {
      x = rhs.x; y = rhs.y; z = rhs.z; 
    }
    return *this;
  }

  triple& operator+=(const triple &rhs) {
    x += rhs.x; y += rhs.y; z += rhs.z;
    return *this;
  }

  triple& operator/=(const float &f) {
    x /= f; y /= f; z /= f;
    return *this;
  }

  float &operator[](int i) {
    switch (i) {
    case 0 : return x;
    case 1 : return y;
    case 2 : return z;
    default : return x;
    }
  }

  void mi_vector(miVector *v) { 
    v->x = x; v->y = y; v->z = z; }

  triple& min(triple& t);
  triple& max(triple& t);
  triple between(triple& t, float fraction=0.5);
  vector<float> sort();
  triple& fit(triple& old_min, triple& old_max, triple& new_min, triple& new_max);
  triple& normalize();
  triple cross(triple& v);
  triple toward(triple& v);
  triple& transform(matrix& m);
};
const triple operator+(const triple &lhs, const triple &rhs);
const triple operator-(const triple &lhs, const triple &rhs);
const triple operator*(const triple &lhs, float f);
ostream& operator<< (ostream& os, const triple& v);

typedef vector<triple>::iterator triple_iterator;


// ----------------------------------------------------------------------
// hull

class hull {
  friend ostream& operator<< (ostream& os, const hull& h);
  triple minimum;
  triple maximum;
 public:
 hull() : minimum(triple(HUGE_VAL, HUGE_VAL, HUGE_VAL)),
    maximum(triple(-HUGE_VAL, -HUGE_VAL, -HUGE_VAL))
      {}
  triple min() const { return minimum; }
  triple max() const { return maximum; }
  float xmin() const { return minimum.x; }
  float ymin() const { return minimum.y; }
  float zmin() const { return minimum.z; }
  float xmax() const { return maximum.x; }
  float ymax() const { return maximum.y; }
  float zmax() const { return maximum.z; }
  triple centroid() { return minimum.between(maximum); }
  triple extent() { return maximum - minimum; }
  void extend(triple &t);
  float max_extent();
  float &operator[](int i) {
    switch (i) {
    case 0 : return minimum.x;
    case 1 : return minimum.y;
    case 2 : return minimum.z;
    case 3 : return maximum.x;
    case 4 : return maximum.y;
    case 5 : return maximum.z;
    default : return minimum.x;
    }
  }
};


// ----------------------------------------------------------------------
// tlist

class tlist {
 public:
  static tlist nil;
  vector<triple> list;
  tlist() {};
  tlist(const tlist& tl) {
    list = tl.list;
  }
  tlist(int count, triple start, triple end);
  triple& operator[](int i) {
    try { return list[i]; }
    catch (out_of_range &error) {
      cerr << "tlist of size " << size() << " indexed at " << i << endl;
      exit(1);
    }
  }
  unsigned int size() { return list.size(); }
  triple_iterator begin() { return list.begin(); }
  triple_iterator end() { return list.end(); }
  tlist& add(const triple &t) { list.push_back(t); return *this; }
  tlist& add(float x, float y, float z=0) {
    triple t(x,y,z); list.push_back(t); return *this; }
  tlist arc(int count, 
            float radius=1, float x=0, float y=0, float z=0,
            float start_angle=0, float end_angle=360);
  tlist circle(int count, float radius=0.5, float x=0, float y=0, float z=0,
               float start_angle=0);
  tlist square(float x=0, float y=0, float z=0);
  tlist& transform(matrix& m);
  hull bbox();
  tlist& fit(float scale=1.0);
  void range(int count, triple start, triple end);
  tlist bounding_uvs(int u_index=0, int v_index=1);
  void dump(const char* label = NULL);
};



// ----------------------------------------------------------------------
// triple_cache

class triple_cache {
  map<string,int> vmap;
  vector<triple> triples;
  bool consolidate;
 public:
 triple_cache() : consolidate(true) {}
  void set_cache_off() { consolidate = false; }
  void set_cache_on() { consolidate = true; }
  const int size() const { return triples.size(); }
  int index(const triple& v, const char* prefix = "");
  triple get(int i) { return triples[i]; }
  int get_position(triple v) { return index(v, "p"); }
  int get_normal(triple v) { return index(v, "n"); }
  int get_uv(triple v) { return index(v, "u"); }
  int get_bump_basis(triple v) { return index(v, "b"); }
  void dump();
};


// ----------------------------------------------------------------------
// vertex

class vertex {
 public:
  triple p;
  triple n;
  triple uv;
  triple bbu;
  triple bbv;
  tlist normals;
 vertex() : 
  p(triple::nil), n(triple::nil), uv(triple::nil),
    bbu(triple::nil), bbv(triple::nil) {}
 vertex(const vertex& v) :
  p(v.p), n(v.n), uv(v.uv), bbu(v.bbu), bbv(v.bbv) {}
 vertex(triple &pos) : 
  p(pos), n(triple::nil), uv(triple::nil),
    bbu(triple::nil), bbv(triple::nil) {}
 vertex(float x, float y, float z) :
  p(triple(x,y,z)), n(triple::nil), uv(triple::nil),
    bbu(triple::nil), bbv(triple::nil) {}
};

typedef vector<vertex>::iterator vertex_iterator;


// ----------------------------------------------------------------------
// indexed vertex

class indexed_vertex {
 public:
  int p;
  int n;
  int uv;
  int bbu;
  int bbv;
  indexed_vertex(vertex v, triple_cache& tcache);
};

typedef vector<indexed_vertex>::iterator indexed_vertex_iterator;


// ----------------------------------------------------------------------
// polygon

class polygon {
 public:
  vector<vertex> vertices;
  hull extent;
  triple normal;
 polygon() : normal(triple::nil) {}
  polygon(tlist tl);

  void init();
  void bbox();
  polygon& set_bounding_uvs(int u_index=0, int v_index=1);
  polygon& set_normal();

  polygon& set_positions(tlist &t);
  void get_positions(tlist &result);
  polygon& set_normals(tlist &t);
  polygon& set_uvs(tlist t);
  polygon& set_bump_basis(triple& basis_u = triple::x_axis,
                          triple& basis_v = triple::y_axis);
  polygon& set_bump_basis(tlist basis_u, tlist basis_v);

  polygon& triangle(triple& a, triple& b, triple& c);
  polygon& triangle(triple& a, triple& b, triple& c, 
                    triple& a_uv, triple& b_uv, triple& c_uv);
  polygon& triangle(triple& a, triple& b, triple& c, 
                    triple& a_uv, triple& b_uv, triple& c_uv,
                    triple& a_bu, triple& a_bv,
                    triple& b_bu, triple& b_bv,
                    triple& c_bu, triple& c_bv);
  polygon& interpolate_uvs(triple& start_uv, triple& end_uv);
  polygon& transform(matrix& m);
};

typedef vector<polygon>::iterator polygon_iterator;


// ----------------------------------------------------------------------
// indexed_polygon
class indexed_polygon {
 public:
  vector<indexed_vertex> vertices;
  int normal;
  indexed_polygon(polygon &p, triple_cache& tcache);
};

typedef vector<indexed_polygon>::iterator indexed_polygon_iterator;


// ----------------------------------------------------------------------
// polygon_group

class polygon_group {
  map<int, int> indexed_average_normals;
  map<triple, triple> average_normals;
  map<string, triple> str_average_normals;
  triple_cache tcache;
  int displacement_subdivisions;
  vector<indexed_polygon> indexed_polygons;
  void cache_polygons();
 public:
  vector<polygon> polygons;
 polygon_group() : displacement_subdivisions(5) {}
  void set_cache_on() { tcache.set_cache_on(); }
  void set_cache_off() { tcache.set_cache_off(); }
  void add_polygon(polygon& p);
  void set_average_normals();
  void set_displacment_subdivisions(int subdivisions) 
  { displacement_subdivisions = subdivisions; }
  void zipper(tlist& a, tlist& b,
              float u_min = 0.0, float v_min = 0.0, float u_max = 1.0, float v_max = 1.0,
              triple& a_end = triple::nil, triple& b_end = triple::nil,
              tlist& c = tlist::nil);
  void revolver(tlist profile, int count, triple &axis,
                float u_min = 0.0, float v_min = 0.0, float u_max = 1.0, float v_max = 1.0,
                bool add_caps=false);
  void mi(bool average_normals=true);
};


// ----------------------------------------------------------------------
// polygon_object

class polygon_object {
  miObject *obj;
  static map<string,int> names;
 public:
 polygon_object() : obj(NULL) {};
  string object_name(string name);
  vector<polygon_group> groups;
  void mi_begin(string name);
  polygon_group& new_group();
  miBoolean mi_end(miTag *result);
};


#endif
