#include "shader.h"
#include "miaux.h"

DLLEXPORT
int shadow_breakpoint_version(void) { return 1; }

struct shadow_breakpoint {
    miColor color;
    miColor transparency;
    miScalar breakpoint;
};

DLLEXPORT
miBoolean shadow_breakpoint ( 
    miColor *result, miState *state, struct shadow_breakpoint *params  )
{
    miColor *color = mi_eval_color(&params->color);
    miColor *transparency = mi_eval_color(&params->transparency);
    miScalar breakpoint = *mi_eval_scalar(&params->breakpoint);

    result->r *= miaux_shadow_breakpoint (color->r, transparency->r, breakpoint );
    result->g *= miaux_shadow_breakpoint (color->g, transparency->g, breakpoint );
    result->b *= miaux_shadow_breakpoint (color->b, transparency->b, breakpoint );

    return miaux_all_channels_equal(result, 0.0) ? miFALSE : miTRUE;
}
