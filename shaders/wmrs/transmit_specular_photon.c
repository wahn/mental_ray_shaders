#include "shader.h"
#include "miaux.h"

struct transmit_specular_photon {
    miColor transparency;
    miScalar index_of_refraction;
};

DLLEXPORT
int transmit_specular_photon_version(void) { return 1; }

DLLEXPORT
miBoolean transmit_specular_photon (
    miColor *result, miState *state, struct transmit_specular_photon *params  )
{
    miVector photon_direction;
    miColor new_energy; 

    miaux_multiply_colors(&new_energy, result, 
                          mi_eval_color(&params->transparency));
    mi_refraction_dir(&photon_direction, state, 1.0, 
                      *mi_eval_scalar(&params->index_of_refraction));
    mi_photon_transmission_specular(&new_energy, state, &photon_direction);

    return miTRUE;
}


