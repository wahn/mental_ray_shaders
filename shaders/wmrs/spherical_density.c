#include "shader.h"
#include "miaux.h"

DLLEXPORT
int spherical_density_version(void) { return 1; }

struct spherical_density {
    miVector center;
    miScalar radius;
};

DLLEXPORT
miBoolean spherical_density ( 
    miScalar *result, miState *state, struct spherical_density *params  )
{
    miVector *center = mi_eval_vector(&params->center);
    miScalar radius = *mi_eval_scalar(&params->radius);
    miVector point;
    mi_vector_to_world(state, &point, &state->point);

    if (mi_vector_dist(center, &point) <= radius)
        *result = 1.0;
    else
        *result = 0.0;

    return miTRUE;
}
