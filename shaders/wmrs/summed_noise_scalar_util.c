double miaux_summed_noise ( 
    miVector *point,
    double summing_weight, double octave_scaling, int octave_count)
{
    int i;
    double noise_value,
	noise_sum = 0.0, noise_scale = 1.0, maximum_noise_sum = 0.0;
    miVector scaled_point;
    miaux_set_vector(&scaled_point, point->x, point->y, point->z);

    for (i = 0; i < octave_count; i++) {
	noise_value = mi_unoise_3d(&scaled_point);
	noise_sum += noise_value / noise_scale;
	maximum_noise_sum += 1.0 / noise_scale;
	noise_scale *= summing_weight;
	miaux_scale_vector(&scaled_point, octave_scaling);
    }
    return noise_sum/maximum_noise_sum;
}

void miaux_set_vector(miVector *v, double x, double y, double z)
{
    v->x = x;
    v->y = y;
    v->z = z;
}

void miaux_scale_vector(miVector *result, miScalar scale)
{
    result->x *= scale;
    result->y *= scale;
    result->z *= scale;
}

double miaux_fit(
    double v, double oldmin, double oldmax, double newmin, double newmax)    
{
    return newmin + ((v - oldmin) / (oldmax - oldmin)) * (newmax - newmin);
}