#include "shader.h"
#include "miaux.h"

DLLEXPORT
int average_radiance_options_version(void) { return 1; }

struct average_radiance_options {
    miColor    color;
    miInteger  gi_accuracy;
    miScalar   gi_radius;
    miInteger  fg_rays;
    miScalar   fg_maxradius;
    miScalar   fg_minradius;
    miInteger  fg_view;
    miInteger  fg_filter;
    miInteger  fg_points;
    miScalar   importance;
    miInteger  caustic_accuracy;
    miScalar   caustic_radius;
};

DLLEXPORT
miBoolean average_radiance_options ( 
    miColor *result, miState *state, struct average_radiance_options *params  )
{
    miColor *color = mi_eval_color(&params->color);
    miIrrad_options options;
    miIRRAD_DEFAULT(&options, state);

    miaux_set_integer_if_not_default(
        &options.globillum_accuracy, state, &params->gi_accuracy);
    miaux_set_scalar_if_not_default(
        &options.globillum_radius, state, &params->gi_radius);
    miaux_set_integer_if_not_default(
        &options.finalgather_rays, state, &params->fg_rays);
    miaux_set_scalar_if_not_default(
        &options.finalgather_maxradius, state, &params->fg_maxradius);
    miaux_set_scalar_if_not_default(
        &options.finalgather_minradius, state, &params->fg_minradius);
    miaux_set_integer_if_not_default(
        (miInteger*)&options.finalgather_view, state, &params->fg_view);
    miaux_set_integer_if_not_default(
        (miInteger*)&options.finalgather_filter, state, &params->fg_filter);
    miaux_set_integer_if_not_default(
        (miInteger*)&options.finalgather_points, state, &params->fg_points);
    miaux_set_scalar_if_not_default(
        &options.importance, state, &params->importance);
    miaux_set_integer_if_not_default(
        &options.caustic_accuracy, state, &params->caustic_accuracy);
    miaux_set_scalar_if_not_default(
        &options.caustic_radius, state, &params->caustic_radius);

    mi_compute_avg_radiance(result, state, 'f', &options); 
    miaux_multiply_color(result, color);

    return miTRUE;
}
