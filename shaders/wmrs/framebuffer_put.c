#include "shader.h"

struct framebuffer_put { 
    miColor color;
    miInteger index;
};

DLLEXPORT
int framebuffer_put_version(void) { return(1); }

DLLEXPORT
miBoolean framebuffer_put (
    miColor *result, miState *state, struct framebuffer_put *params )
{ 
    *result = *mi_eval_color(&params->color);

    if (state->type == miRAY_EYE)
        mi_fb_put(state, *mi_eval_integer(&params->index), result);

    return miTRUE;
}

