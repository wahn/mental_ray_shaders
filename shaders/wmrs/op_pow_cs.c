#include "shader.h"
#include <math.h>

DLLEXPORT
int op_pow_cs_version(void) { return 1; }

struct op_pow_cs {
    miColor A;
    miScalar B;
};

DLLEXPORT
miBoolean op_pow_cs (miColor *result, miState *state, struct op_pow_cs *params )
{
    miColor *A = mi_eval_color(&params->A);
    miScalar B = *mi_eval_scalar(&params->B);
    result->r = powf(A->r, B);
    result->g = powf(A->g, B);
    result->b = powf(A->b, B);
    return miTRUE;
}
