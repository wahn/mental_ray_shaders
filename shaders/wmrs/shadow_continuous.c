#include "shader.h"
#include "miaux.h"

DLLEXPORT
int shadow_continuous_version(void) { return 1; }

struct shadow_continuous {
    miColor color;
    miColor transparency;
    miScalar expansion;
};

DLLEXPORT
miBoolean shadow_continuous ( 
    miColor *result,
    miState *state,
    struct shadow_continuous *params  )
{
    miColor *color = mi_eval_color(&params->color);
    miColor *transparency = mi_eval_color(&params->transparency);
    miScalar expansion = *mi_eval_scalar(&params->expansion);

    result->r *= miaux_shadow_continuous (color->r, transparency->r, expansion );
    result->g *= miaux_shadow_continuous (color->g, transparency->g, expansion );
    result->b *= miaux_shadow_continuous (color->b, transparency->b, expansion );

    return miaux_all_channels_equal(result, 0.0) ? miFALSE : miTRUE;
}
