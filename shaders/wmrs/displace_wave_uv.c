#include "shader.h"
#include "miaux.h"

struct displace_wave_uv { 
    miScalar frequency_u;
    miScalar amplitude_u;
    miScalar frequency_v;
    miScalar amplitude_v;
};

DLLEXPORT
int displace_wave_uv_version(void) { return(1); }

DLLEXPORT
miBoolean displace_wave_uv ( 
    miScalar *result, miState *state, struct displace_wave_uv *params  )
{
    *result += 
        miaux_sinusoid(state->tex_list[0].x,
                       *mi_eval_scalar(&params->frequency_u),
                       *mi_eval_scalar(&params->amplitude_u)) +
        miaux_sinusoid(state->tex_list[0].y,
                       *mi_eval_scalar(&params->frequency_v),
                       *mi_eval_scalar(&params->amplitude_v));
    return miTRUE;
}

