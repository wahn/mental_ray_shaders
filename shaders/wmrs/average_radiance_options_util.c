void miaux_set_integer_if_not_default(
    miInteger *result, miState *state, miInteger *param) 
{
    miInteger use_default_flag = -1;
    miInteger param_value = *mi_eval_integer(param);
    if (param_value != use_default_flag)
	*result = param_value;
}

void miaux_set_scalar_if_not_default(
    miScalar *result, miState *state, miScalar *param)
{
    miScalar use_default_flag = -1.0;
    miScalar param_value = *mi_eval_scalar(param);
    if (param_value != use_default_flag)
	*result = param_value;
}

void miaux_multiply_color(miColor *result, miColor *color)
{
    result->r *= color->r;
    result->g *= color->g;
    result->b *= color->b;
}