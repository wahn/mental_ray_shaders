#include "shader.h"
#include "miaux.h"

struct rim_bright {
    miColor ambient; 
    miColor diffuse; 
    miColor rim;     
    int     i_light; 
    int     n_light; 
    miTag   light[1];
};

DLLEXPORT
int rim_bright_version(void) { return 1; }

DLLEXPORT
miBoolean rim_bright (
    miColor *result, miState *state, struct rim_bright *params  )
{
    int i, light_count, light_sample_count;
    miColor sum, light_color;
    miVector direction_toward_light;
    miScalar dot_nl;
    miTag *light;

    miColor *diffuse = mi_eval_color(&params->diffuse);
    miColor *rim = mi_eval_color(&params->rim);
    miaux_light_array(&light, &light_count, state,
                      &params->i_light, &params->n_light, params->light);
    *result = *mi_eval_color(&params->ambient);

    for (i = 0; i < light_count; i++, light++) {
        miaux_set_channels(&sum, 0);
        light_sample_count = 0;
        while (mi_sample_light(&light_color, &direction_toward_light,
                               &dot_nl, state, *light, &light_sample_count))
            miaux_add_diffuse_component(&sum, dot_nl, diffuse, &light_color);
        if (light_sample_count) {
            miaux_add_scaled_color(result, &sum, 1/light_sample_count);
        }
    }
    miaux_brighten_rim(result, state, rim);
    return miTRUE;
}
