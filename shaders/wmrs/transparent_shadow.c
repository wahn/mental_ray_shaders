#include "shader.h"
#include "miaux.h"

struct transparent_shadow { 
    miColor color; 
    miColor transparency;
    miScalar breakpoint;
    miScalar center;
    miScalar extent;
};

DLLEXPORT
int transparent_shadow_version(void) { return(1); }

DLLEXPORT
miBoolean transparent_shadow (
    miColor *result, miState *state, struct transparent_shadow *params  )
{
    miColor *transparency = mi_eval_color(&params->transparency);

    if (state->type == miRAY_SHADOW) {
        miColor *color = mi_eval_color(&params->color);
        miScalar breakpoint = *mi_eval_scalar(&params->breakpoint);
        miScalar center = *mi_eval_scalar(&params->center);
        miScalar extent = *mi_eval_scalar(&params->extent);

        result->r *= miaux_shadow_breakpoint_scale(color->r, transparency->r, 
                                                   breakpoint, center, extent);
        result->g *= miaux_shadow_breakpoint_scale(color->g, transparency->g,
                                                   breakpoint, center, extent);
        result->b *= miaux_shadow_breakpoint_scale(color->b, transparency->b,
                                                   breakpoint, center, extent);
        return miaux_all_channels_equal(result, 0.0) ? miFALSE : miTRUE;
    }
    if (miaux_all_channels_equal(transparency, 0.0))
        *result = *mi_eval_color(&params->color);
    else {
        mi_trace_transparent(result, state);
        if (!miaux_all_channels_equal(transparency, 1.0)) {
            miColor *color = mi_eval_color(&params->color);
            miColor opacity;
            miaux_invert_channels(&opacity, transparency);
            mi_opacity_set(state, &opacity);
            miaux_blend_channels(result, color, transparency);
        }
    }
    return miTRUE;
}
