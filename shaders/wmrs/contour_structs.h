#ifndef _CONTOUR_STRUCTS_H_
#define _CONTOUR_STRUCTS_H_
#include "shader.h"

typedef struct {
    miTag    instance;   /* Instance hit by ray */
    miVector normal;     /* Ray intersection normal */
} contour_info;

typedef struct {
    miTag    instance; 
    miVector point;
    miVector normal;
} distance_contour_info;

#endif
