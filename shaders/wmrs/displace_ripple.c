#include "shader.h"
#include "miaux.h"

struct displace_ripple { 
    miVector center;
    miScalar frequency;
    miScalar amplitude;
};

DLLEXPORT
int displace_ripple_version(void) { return(1); }

DLLEXPORT
miBoolean displace_ripple ( 
    miScalar *result, miState *state, struct displace_ripple *params  )
{
    *result += miaux_sinusoid(mi_vector_dist(mi_eval_vector(&params->center),
                                             &state->tex_list[0]),
                              *mi_eval_scalar(&params->frequency),
                              *mi_eval_scalar(&params->amplitude));
    return miTRUE;
}

