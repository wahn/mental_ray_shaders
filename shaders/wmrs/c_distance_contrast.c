#include "contour_structs.h"

struct c_distance_contrast {
    miScalar normal_change;
    miScalar distance;
};

DLLEXPORT
int c_distance_contrast_version(void) { return 1; }

DLLEXPORT
miBoolean c_distance_contrast (
    distance_contour_info      *info1,
    distance_contour_info      *info2,
    int               level,
    miState           *state,
    struct c_distance_contrast *params  )
{
    /* Surface orientation is different, but in the same object: */

    /*
    if (mi_vector_dist(&info1->point, &info2->point) < params->distance)
        return miFALSE;
    */
    /*
    if (info1->instance != info2->instance)
    return miTRUE;
    */

    if        (info1->instance == info2->instance &&
         (mi_vector_dot(&info1->normal, &info2->normal) < 
          cos(params->normal_change * M_PI/180.0)))
        return miTRUE;

    /* Otherwise, no contour here. */
    return miFALSE;
}
