#include "shader.h"
#include "miaux.h"

struct bump_wave { 
    miScalar frequency;
    miScalar amplitude;
};

DLLEXPORT
int bump_wave_version(void) { return(1); }

DLLEXPORT
miBoolean bump_wave ( 
    miColor *result, miState *state, struct bump_wave *params  )
{
    state->normal.x += miaux_sinusoid(state->point.x,
                                      *mi_eval_scalar(&params->frequency),
                                      *mi_eval_scalar(&params->amplitude));
    mi_vector_normalize(&state->normal);
    return miTRUE;
}

