A place to collect and develop mental ray shaders.

The first shaders going into this repository will come from Andy Kopra's book
"Writing mental ray shaders" (see http://writingshaders.com/) and might have
to be modified to work with the latest mental ray version. This way we will 
keep track of the changes necessary before adding more shaders.

